import logging
from pathlib import Path
import platform
from pyjob import StopWatch
import sys
import time
import traceback

from slicendice.command_line.dice_em import DiceEm
from slicendice.command_line.dice_mx import DiceMx
from slicendice.command_line.slice import Slice
from slicendice.util.argparse_util import parse_command_line
from slicendice.util.logging_util import setup_logging
from slicendice.util.em_util import em_check, em_fitting_scores
from slicendice.version import __snd_version__


def make_workdir(dir_name_stem="slicendice"):
    """Make a work directory rooted at run_dir and return its path.

    Args:
        dir_name_stem: Name to use as stem of working directory name.

    Returns:
        The path to the working directory.

    Raises:
        RuntimeError: If there are too many work directories.
        RuntimeError: If there is an existing work directory.
    """
    max_work_dirs = 100
    run_dir = Path.cwd()
    run_inc = 0
    while True:
        dname = f"{dir_name_stem}_{run_inc}"
        work_dir = run_dir.joinpath(dname)
        if not work_dir.exists():
            break
        run_inc += 1
        if run_inc > max_work_dirs:
            raise RuntimeError(f"Too many work directories! {work_dir}")
    if work_dir.exists():
        raise RuntimeError(
            f"There is an existing work directory: {work_dir}\n"
            f"Please delete/move it aside."
        )
    work_dir.mkdir()
    return work_dir


def main():
    args = parse_command_line()
    if args.mapin is not None:
        if not em_check(
            args.fitting_scores_python_excecutable, args.fitting_scores_site_packages
        ):
            raise RuntimeError(
                "\nSlice'N'Dice EM is not possible, please "
                "use the CCPEM version of Slice'N'Dice, "
                "accessible via CCPEM-Pipeliner / Doppio \n"
            )
        if args.fitting_scores_python_excecutable is None:
            (
                args.fitting_scores_python_excecutable,
                args.fitting_scores_site_packages,
            ) = em_fitting_scores()

    working_dir = make_workdir()
    log_file = Path(working_dir, "slicendice.log")
    debug_file = Path(working_dir, "debug.log")
    output_json = str(Path(working_dir, "slicendice_results.json"))

    global logger
    logger = setup_logging(level="INFO", logfile=log_file, debugfile=debug_file)

    print_header(dev=args.dev)

    stopwatch = StopWatch()
    stopwatch.start()
    logger.info(f"Running in directory: {working_dir}\n")

    try:
        slice = Slice(logger, working_dir, output_json)
        slice.run_slice(args)

        if args.hklin is not None:
            dice = DiceMx(logger, working_dir, output_json)
            dice.run_dice_mx(args, slice.results)

        elif args.mapin is not None or (
            args.half_map_1 is not None and args.half_map_2 is not None
        ):
            dice = DiceEm(logger, working_dir, output_json)

            dice.run_dice_em(args, slice.results)

        else:
            logger.info("\nNo hklin/mapin file provided, MR skipped\n")

    except KeyboardInterrupt:
        sys.stderr.write("Interrupted by keyboard!")
        return 0
    except Exception as e:
        sys.stderr.write("\nError running Slice'N'Dice: {}\n".format(e))
        traceback.print_exc(file=sys.stderr)
        return 2

    stopwatch.stop()
    logger.info(
        "All processing completed in %d days, %d hours, %d minutes, and %d seconds",
        *stopwatch.time_pretty,
    )


def print_header(dev=False):
    """Print the header information at the top of each script"""
    logger = logging.getLogger(__name__)
    nhashes = 120
    name = "Slice'N'Dice" if not dev else "Slice'N'Dev"
    logger.info(
        "%(sep)s%(hashish)s%(sep)s%(hashish)s%(sep)s%(hashish)"
        "s%(sep)s#%(line)s#%(sep)s%(hashish)s%(sep)s",
        {
            "hashish": "#" * nhashes,
            "sep": "\n",
            "line": f"{name} - Model segmenting and placing Pipeline".center(
                nhashes - 2, " "
            ),
        },
    )
    logger.info(f"Slice'N'Dice version: {__snd_version__}")
    logger.info(f"Running on host: {platform.node()}")
    logger.info(f"Running on platform: {platform.platform()}")
    logger.info(
        "Job started at: %s", time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())
    )
    script_name = Path(sys.argv[0]).stem
    logger.info(
        "Invoked with command-line:\n%s\n",
        " ".join(map(str, [script_name] + sys.argv[1:])),
    )


if __name__ == "__main__":
    sys.exit(main())
