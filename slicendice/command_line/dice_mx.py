from pathlib import Path

from slicendice.dice import MrSubmit, MrSubmitHybrid
from struvolpy import Structure


class DiceMx(object):
    def __init__(self, logger, working_dir, output_json):
        self.logger = logger
        self.working_dir = working_dir
        self.output_json = output_json

    def run_dice_mx(self, args, results):
        if args.xyz_list:
            assert (
                args.seqin and Path(args.seqin).exists()
            ), "A sequence file must be provided when using xyz_list argument"

        dice_kwargs = {}
        dice_options = [
            "pack_cutoff",
            "peak_rot_cutoff",
            "sgalternative",
            "ncyc_refmac",
            "no_mols",
            "mr_program",
            "molrep_tf",
            "tmp_dir",
            "llg_cutoff",
            "tfz_cutoff",
        ]
        for arg in args._get_kwargs():
            if arg[0] in dice_options and arg[1] is not None:
                dice_kwargs[arg[0]] = arg[1]

        if args.seqin is not None:
            seqin = args.seqin
        else:
            seqin = Path(self.working_dir, "input.fasta")
            structure = Structure.from_file(args.xyzin)
            structure.to_fasta(str(seqin))

        if args.mr_program == 'hybrid':
            mr = MrSubmitHybrid(
                args.hklin, str(seqin), working_dir=self.working_dir, **dice_kwargs
            )
            mr.submit_jobs(
                results,
                nproc=args.nproc,
                submit_qtype=args.submit_qtype,
                submit_queue=args.submit_queue,
            )
            mr.report_results(json_file=self.output_json)
        else:
            mr = MrSubmit(
                args.hklin, str(seqin), working_dir=self.working_dir, **dice_kwargs
            )
            mr.submit_jobs(
                results,
                nproc=args.nproc,
                submit_qtype=args.submit_qtype,
                submit_queue=args.submit_queue,
            )
            mr.report_results(json_file=self.output_json)

        output_dir = self.working_dir.joinpath("output")
        self.logger.info(f"Results output to: {output_dir}\n")
        output_dir.mkdir()
        for result in mr.results:
            xyz = Path(result.xyzout)
            xyzout = output_dir.joinpath(xyz.name)
            xyzout.write_text(xyz.read_text())
            hkl = Path(result.hklout)
            hklout = output_dir.joinpath(hkl.name)
            hklout.write_bytes(hkl.read_bytes())
