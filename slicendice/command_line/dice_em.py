__author__ = "Luc Elliott"
__date__ = "21 Apr 2023"
__version__ = "1.0"


import json
import os
from pandas import DataFrame, errors, read_csv
from pathlib import Path
from slicendice.dice import EmSubmit
from slicendice.dice.scoresclassifier import Classifier
from slicendice.post_processing import PostProcessGrouping
from slicendice.util import find_best_non_overlapping_models, combine_structures
from slicendice.util.file_utils import (
    FileTracker,
    ReportOutput,
    OUTPUT_DIR,
    GROUPED_DIR,
    RESULTS_DIR,
)
from struvolpy import Structure
from typing import Optional, List, Dict
import warnings
import pandas as pd

DICE_OPTIONS = [
    "mapin",
    "gpu",
    "nproc",
    "map_fitting_program",
    "xyz_fixed",
    "resolution",
    "xyz_list",
    "no_mols",
    "contour",
    "tmp_dir",
    "working_dir",
    "classifier_threshold",
    "dev",
    "fitting_scores_python_excecutable",
    "fitting_scores_site_packages",
    "half_map_1",
    "half_map_2",
    "point_group_symmetry",
    "seqin",
    "starting_model_vrms",
    "defragger",
]

FITTING_METHODS = ["molrep", "powerfit", "em_placement"]


class DiceEm(object):
    """
    A class for running Dice EM.

    Args:
        logger (logging.Logger): The logger object for logging messages.
        working_dir (pathlib.Path): The path to the working directory.
        output_json (pathlib.Path): The path to the output JSON file.


    Attributes:
        logger (logging.Logger): The logger object for logging messages.
        working_dir (pathlib.Path): The path to the working directory.
        output_json (pathlib.Path): The path to the output JSON file.


    """

    def __init__(self, logger, parent_dir, output_json):
        self.logger = logger
        self.parent_dir = parent_dir
        self.output_json = output_json

    def run_dice_em(self, args, results):
        """
        Runs the Dice EM algorithm.

        Args:
            args (argparse.Namespace): The parsed command-line arguments.
            results (list): A list of input models.

        Returns:
            None.

        Raises:
            None.
        """
        if args.xyz_list and args.map_fitting_program == "em_placement":
            assert (
                args.seqin and Path(args.seqin).exists()
            ), "A sequence file must be provided when using xyz_list argument"
        if args.map_fitting_program == "em_placement":
            if args.seqin is None:
                args.seqin = Path(self.parent_dir, "input.fasta")
                structure = Structure.from_file(args.xyzin)
                structure.to_fasta(str(args.seqin))
        dice_kwargs = self.generate_kwargs(args)
        self.pre_procesing_errors(dice_kwargs)
        self.pre_processing_warnings(dice_kwargs)

        for split in results:
            split_dir = self.parent_dir.joinpath(f"split_{len(split)}")
            if not split_dir.exists():
                split_dir.mkdir()
            run_track = self.initialise_run_track(split)
            # File tracker class helps to keep track of Dice outputs in one place
            file_tracker = FileTracker(split_dir)
            ems = EmSubmit(
                parent_dir=split_dir,
                models=run_track["XYZ_inputs"],
                logger=self.logger,
                **dice_kwargs,
            )
            # Check if a fixed model has been specified
            run_track = self.xyz_fixed_check(dice_kwargs, file_tracker, run_track)

            if dice_kwargs.get("map_fitting_program") == "molrep":
                self.molrep_em(
                    ems, args, split_dir, run_track, file_tracker, dice_kwargs, split
                )
                return

            elif dice_kwargs.get("map_fitting_program") == "powerfit":
                self.powerfit_em(
                    ems, args, split_dir, run_track, file_tracker, dice_kwargs, split
                )
                return

            elif dice_kwargs.get("map_fitting_program") == "em_placement":
                self.em_placement(
                    ems, args, split_dir, run_track, file_tracker, dice_kwargs, split
                )
                return

            self.logger.info("\nNo fitting method specified, Exiting Dice")
            exit()

    def molrep_em(
        self,
        ems: EmSubmit,
        args: dict,
        split_dir: Path,
        run_track: dict,
        file_tracker: FileTracker,
        dice_kwargs: dict,
        split: list,
    ):

        self.logger.info("\nRunning Molrep")
        # cycles to take all input models and uses the outputs as fixed
        if len(run_track["XYZ_inputs"]) == 0:
            self.logger.info("\nThere are no more clusters to be placed. Exiting Dice")
            return

        ems.working_dir = split_dir.joinpath("molrep")

        # Run Molrep
        smr = SubmitMolrep(ems, run_track, self.logger, file_tracker)
        smr.run_molrep(args)
        self.logger.info(f"\nMolrep run complete for {split_dir}")
        # Get the Molrep output
        df = smr.molrep_scoring()
        if df is None:
            return

        # Get the best non-overlapping models
        smr.non_overlapping(df)
        smr.get_fixed_structure()
        # Update the run track and file tracker
        run_track = smr.run_track
        ems.models = run_track["XYZ_inputs"]
        ems.xyz_fixed = run_track["XYZ_fixed_track"]

        self.process_dice_run(split_dir, run_track, file_tracker, dice_kwargs, split)

    def powerfit_em(
        self,
        ems: EmSubmit,
        args: dict,
        split_dir: Path,
        run_track: dict,
        file_tracker: FileTracker,
        dice_kwargs: dict,
        split: list,
    ):
        for i, xyz in enumerate(run_track["XYZ_inputs"], start=1):
            # Powerfit runs one model at a time
            xyz_name = Path(xyz).name
            self.logger.info(f"\nRunning PowerFit on {xyz_name}")
            ems.working_dir = split_dir.joinpath(f"powerfit_{i}")

            # Run PowerFit
            sp = SubmitPowerfit(ems, run_track, self.logger, file_tracker)
            sp.run_powerfit(args, xyz)
            self.logger.info("\nPowerFit run complete for %s", xyz_name)

            # Get the Powerfit output, generate scores and run classifier
            df = sp.powerfit_scoring(xyz)
            if df is None:
                continue
            # Get the best non-overlapping models
            sp.non_overlapping(df)
            sp.get_fixed_structure()
            # Update the run track and file tracker
            run_track = sp.run_track
            ems.xyz_fixed = run_track["XYZ_fixed_track"]
        self.process_dice_run(split_dir, run_track, file_tracker, dice_kwargs, split)

    def em_placement(
        self,
        ems: EmSubmit,
        args: dict,
        split_dir: Path,
        run_track: dict,
        file_tracker: FileTracker,
        dice_kwargs: dict,
        split: list,
    ):
        self.logger.info("\nRunning EM Placement")
        # cycles to take all input models and uses the outputs as fixed
        if len(run_track["XYZ_inputs"]) == 0:
            self.logger.info("\nThere are no more clusters to be placed. Exiting Dice")
            return

        ems.working_dir = split_dir.joinpath("em_placement")

        # Run em_placement
        sep = SubmitEMPlacement(ems, run_track, self.logger, file_tracker)
        sep.run_em_placement(args)
        self.logger.info(f"\nem_placement run complete for {split_dir}")

        # same as a scoring funciton
        df = sep.submithelper.combine_csvs(
            [x for x in ems.working_dir.rglob("output_table_docking.csv")],
            f"output_table_docking_{split_dir.name}.csv",
        )

        # set the pass condtion
        if df is None:
            msg = "The output csv file is empty, exiting Dice"
            self.logger.info(msg)
            return
        df = df[df["mapLLG"] > 60]
        # set the pass condtion
        if df is None or df.empty:
            warnings.warn(
                "Either no models were placed successfully or there was no \
output from the em_placement run"
            )
            return

        # set df index column to the first column
        df = df.set_index(df.columns[0])
        # Get the best non-overlapping models
        sep.non_overlapping(df)

        sep.get_fixed_structure()
        # Update the run track and file tracker
        run_track = sep.run_track
        ems.models = run_track["XYZ_inputs"]
        ems.xyz_fixed = run_track["XYZ_fixed_track"]

        self.process_dice_run(
            split_dir, run_track, file_tracker, dice_kwargs, split, "mapLLG"
        )

    def generate_kwargs(self, args):
        """Generate keyword arguments for the DiceEM class.

        Args:
            args: An argparse.Namespace object containing command-line arguments.

        Returns:
            A dictionary containing keyword arguments for the DiceEM class.
        """
        dice_kwargs = {}

        for arg in args._get_kwargs():
            if arg[0] in DICE_OPTIONS and arg[1] is not None:
                dice_kwargs[arg[0]] = arg[1]

        return dice_kwargs

    def pre_procesing_errors(self, kwargs: dict) -> None:
        """
        A function for performing pre-processing errors on input parameters
        for the Dice-EM algorithm.

        Args:
            kwargs (dict): A dictionary of input parameters.




        Raises:
            FileNotFoundError: If the mapin file does not exist.
            ValueError: If the resolution is not specified.
            ValueError: If the half maps are not specified when using em_placement.
            ValueError: If the sequence file is not specified when using xyz_list.

        """
        mapin: Optional[str] = kwargs.get("mapin")
        if mapin is not None:
            if not Path(mapin).exists():
                raise FileNotFoundError("Mapin file does not exist")

        if kwargs.get("resolution") is None:
            msg = "Must specify a resolution"
            raise ValueError(msg)

        half_map_1 = kwargs.get("half_map_1")
        half_map_2 = kwargs.get("half_map_2")

        if kwargs.get("em_placement", False) and (not half_map_1 and not half_map_2):
            if not (half_map_1 and Path(half_map_1).exists()) or not (
                half_map_2 and Path(half_map_2).exists()
            ):
                msg = "Half maps do not exist"
                raise FileNotFoundError(msg)

            if kwargs.get("xyz_list", False) and kwargs.get("seqin") is None:
                msg = "Must specify a sequence file when using xyz_list"
                raise ValueError(msg)

    def pre_processing_warnings(self, kwargs):
        """
        A function for performing pre-processing checks on input parameters
        for the Dice-EM algorithm.

        args:
            kwargs (dict): A dictionary of input parameters.

        Raises:
            Warning: If 'contour' is not specified.
        """
        if kwargs.get("contour") is None:
            msg = "No contour level specified, will preform an automatic contour \
calculation. This tends to be less accurate than a user specified contour level."
            warnings.warn(msg)

        if kwargs.get("em_placement", False) and kwargs.get("xyz_fixed", False):
            msg = "em_placment does not require a fixed model, the fixed model \
will be ignored"
            warnings.warn(msg)

    def intialise_em_submit(self, dice_kwargs, split_dir, run_track):
        return EmSubmit(
            parent_dir=split_dir,
            models=run_track["XYZ_inputs"],
            logger=self.logger,
            **dice_kwargs,
        )

    def xyz_fixed_check(self, kwargs, file_tracker, run_track):
        """
        Runs a check to see if a fixed model has been specified.

        Args:
            kwargs (dict): A dictionary of input parameters.
            file_tracker (FileTracker): An instance of the FileTracker class.
            run_track (dict): A dictionary containing the run track information.

        Returns:
            run_track (dict): A dictionary containing the updated run track information.
        """
        fixed_model_path = kwargs.get("xyz_fixed")
        if fixed_model_path is not None:
            fixed_file = file_tracker.results_directory.joinpath("xyz_fixed.pdb")
            Path(fixed_model_path).copy(fixed_file)
            run_track["XYZ_fixed_track"] = fixed_file

        return run_track

    def post_processing(self, file_tracker, split, kwargs) -> dict:
        """
        Runs post processing on the output files to group them into close partners.

        Args:
            file_tracker (FileTracker): An instance of the FileTracker class.
            split (list): A list of paths to the input XYZ files.
            kwargs (dict): A dictionary of input parameters.


        """
        with open(self.output_json, "r") as data:
            slice_data = json.load(data)["slice"]

        if not kwargs.get("xyz_list", False):
            split_key = "split_{}".format(len(split))
            if not len(slice_data[split_key]["models"]) == 1:
                ppg = PostProcessGrouping(
                    *[str(x) for x in file_tracker.return_all_file_pathways()],
                    split_key="split_{}".format(len(split)),
                    input_json=self.output_json,
                    no_mols=kwargs.get("no_mols", 1),
                )
                file_tracker.move_files_to_grouped_directory(ppg.groups)
        else:
            for split_key in slice_data:
                file_prefix = split_key.split("_split_")[0]
                if not len(slice_data[split_key]["models"]) == 1:
                    ppg = PostProcessGrouping(
                        *[
                            x
                            for x in file_tracker.return_all_file_pathways()
                            if file_prefix in Path(x).stem
                        ],
                        split_key=split_key,
                        input_json=self.output_json,
                        no_mols=kwargs.get("no_mols", 1),
                    )

                    file_tracker.move_files_to_grouped_directory(ppg.groups)
        file_tracker.move_remaining_files(copy=False)
        # check to see if ppg has been defined
        if "ppg" in locals():
            return ppg.groups
        return {}

    def results_reporter(
        self,
        proba_jsons: List[Path],
        groups: Dict[str, List[str]],
        no_mols: int,
        xyz_list: bool = False,
    ) -> None:
        """
        Generates a report of the results of the Dice EM algorithm.

        Args:
            proba_jsons (list): A list of paths to the probability JSON files.
            groups (dict): A dictionary containing the grouped models.

        Returns:
            None.
        """
        passed_models = False
        ro = ReportOutput(self.logger)

        ro.report_non_passed_models(proba_jsons)
        ro.report_probabilities(proba_jsons)
        passed_models = passed_models or ro.report_groupings(groups)
        passed_models = passed_models or ro.report_no_mols_passed(
            proba_jsons, no_mols, xyz_list=xyz_list
        )
        if not passed_models:
            ro.boxed_title("No models passed")
        self.logger.info("\n")

    def add_proba_json_to_directories(self, split_dir, file_tracker):
        """
        Adds the probability JSON file to the results directory.

        Args:
            results_dir (pathlib.Path): The path to the results directory.

        """
        prob_a_jsons = []

        for results_dir in split_dir.joinpath(RESULTS_DIR).iterdir():
            if results_dir.is_dir():
                if not any(results_dir.iterdir()):
                    continue
                prob_a_jsons.append(
                    file_tracker.add_proba_json_to_directory(results_dir)
                )

        return prob_a_jsons

    def update_results_json(self, split_dir: Path, result: Path):
        """
        Updates the results JSON file with the grouped models.

        Args:
            split_dir (pathlib.Path): The path to the split directory.
            groups (dict): A dictionary containing the grouped models.

        """
        with open(self.output_json, "r") as data:
            results = json.load(data)

        if "dice" not in results:
            results["dice"] = {}

        if split_dir.name not in results["dice"]:
            results["dice"][split_dir.name] = {}

        if result is not None:
            results["dice"][split_dir.name]["result"] = str(result)

        passed_models = [
            str(x)
            for x in split_dir.joinpath(RESULTS_DIR).joinpath(OUTPUT_DIR).iterdir()
            if x.suffix != ".json"
        ]

        if split_dir.joinpath(RESULTS_DIR).joinpath(GROUPED_DIR).exists():
            results["dice"][split_dir.name]["passed_models"] = [
                str(x)
                for x in split_dir.joinpath(RESULTS_DIR).joinpath(GROUPED_DIR).iterdir()
                if x.suffix != ".json"
            ]
            results["dice"][split_dir.name]["passed_models"].extend(passed_models)
        else:
            results["dice"][split_dir.name]["passed_models"] = passed_models

        with open(self.output_json, "w") as data:
            json.dump(results, data, indent=4)

    def process_dice_run(
        self,
        split_dir,
        run_track,
        file_tracker,
        dice_kwargs,
        split,
        col_name="proba",
    ):
        """
        Processes the output of a Dice run.

        Args:
            split_dir (pathlib.Path): The path to the split directory.
            run_track (dict): A dictionary containing the run track information.
            file_tracker (FileTracker): An instance of the FileTracker class.
            dice_kwargs (dict): A dictionary of input parameters.
            split (list): A list of paths to the input XYZ files.

        """
        self.logger.info(f"\nDice run complete for {split_dir}")
        result = run_track["XYZ_fixed_track"]

        if result is not None:
            result = Path(result).rename(split_dir.joinpath("result.pdb"))

        file_tracker.top_n_non_passed(
            dice_kwargs.get("no_mols", 1), column_name=col_name
        )

        groups = self.post_processing(file_tracker, split, dice_kwargs)

        prob_a_jsons = self.add_proba_json_to_directories(split_dir, file_tracker)

        xyz_list = True if dice_kwargs.get("xyz_list", False) else False

        self.results_reporter(
            prob_a_jsons, groups, dice_kwargs.get("no_mols", 1), xyz_list=xyz_list
        )

        self.update_results_json(split_dir, result)

    @staticmethod
    def initialise_run_track(split):
        """
        Initializes the run track for the Dice EM algorithm.

        Args:
            split (list): A list of paths to the input XYZ files.

        Returns:
            dict: A dictionary containing the following keys:
                - XYZ_inputs (list): A list of pathlib.Path objects representing
                    the input XYZ files.
                - XYZ_fixed_track (None): A placeholder for the fixed structure.
                - cycle (int): The current cycle number (initialized to 0).
        """
        run_track = {
            "XYZ_inputs": [Path(xyz) for xyz in split],
            "XYZ_fixed_track": None,
            "Cycle": 0,
        }
        return run_track


class SubmitPowerfit:
    """
    A class for submitting powerfit jobs.

    Args:
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        logger (logging.Logger): The logger object for logging messages.
        file_tracker (FileTracker): An instance of the FileTracker class.

    Attributes:
        logger (logging.Logger): The logger object for logging messages.
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        file_tracker (FileTracker): An instance of the FileTracker class.
        name_prefix (str): The prefix for the job name.
        submithelper (JobSubmissionHelper): An instance of the
        JobSubmissionHelper class.
    """

    def __init__(self, ems, run_track, logger, file_tracker):
        self.logger = logger
        self.ems = ems
        self.run_track = run_track
        self.file_tracker = file_tracker
        self.name_prefix = "powerfit"
        self.submithelper = JobSubmissionHelper(logger)

    def run_powerfit(self, args, xyz) -> None:
        """
        Runs powerfit on the given input structure.

        Args:
            args (argparse.Namespace): The parsed command-line arguments.
            xyz (pathlib.Path): The path to the input structure.

        Returns:
            None
        """
        self.ems.models = [xyz]
        self.ems.submit_powerfit_job(
            submit_qtype=args.submit_qtype,
            submit_queue=args.submit_queue,
        )

    def powerfit_scoring(self, xyz) -> Optional[DataFrame]:
        """
        Runs PowerFit scoring on the given input structure.

        Args:
            xyz (pathlib.Path): The path to the input structure.

        Returns:
            pandas.DataFrame: A DataFrame containing the successful scores.
        """
        self.ems.get_scoring_inputs(os.path.join("powerfit", "*.pdb"))

        if len(self.ems.scoring_models) == 0:
            self.logger.info(
                "\nNo PowerFit runs completed successfully for %s ", Path(xyz).name
            )
            return None

        successful_scores_df = self.submithelper.generate_scores_and_run_classifier(
            self.ems, self.name_prefix
        )
        if successful_scores_df is None:
            self.logger.info(
                "\nNo PowerFit placements passed the classifier for %s. \
This model will not be used as a fixed model going forward.",
                Path(xyz).name,
            )
            return None
        self.logger.info(
            "\nPowerFit placements passed the classifier for %s. \
This model be used as a fixed model going forward.",
            Path(xyz).name,
        )

        return successful_scores_df

    def non_overlapping(self, successful_scores_df) -> None:
        """
        Copies the output files of the best non-overlapping models to the file tracker.

        Args:
            successful_scores_df (pandas.DataFrame): A DataFrame
            containing the successful scores.

        Returns:
            None
        """
        non_overlapping_models = find_best_non_overlapping_models(
            successful_scores_df, "proba"
        )
        non_overlapping_models = self.submithelper.no_mol_limiter(
            non_overlapping_models, self.ems.nmol
        )
        self.logger.debug("\nNon-overlapping models: %s", non_overlapping_models)
        self.submithelper.copy_files_to_results(
            self.file_tracker, non_overlapping_models
        )

    def get_fixed_structure(self) -> dict:
        """
        Retrieves the fixed structure from the run track and file tracker.

         Returns:
            dict: A dictionary containing the updated run track information.
        """
        return self.submithelper.get_fixed_structure(self.run_track, self.file_tracker)


class SubmitMolrep:
    """
    A class for submitting Molrep jobs.

    Args:
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        logger (logging.Logger): The logger object for logging messages.
        file_tracker (FileTracker): An instance of the FileTracker class.

    Attributes:
        logger (logging.Logger): The logger object for logging messages.
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        file_tracker (FileTracker): An instance of the FileTracker class.
        name_prefix (str): The prefix for the job name.
        submithelper (JobSubmissionHelper): An instance of the
            JobSubmissionHelper class.
    """

    def __init__(self, ems, run_track, logger, file_tracker):
        self.logger = logger
        self.ems = ems
        self.run_track = run_track
        self.file_tracker = file_tracker
        self.name_prefix = "molrep"
        self.submithelper = JobSubmissionHelper(logger)

    def run_molrep(self, args) -> None:
        """
        Submits a Molrep job for each input XYZ file in the run track.

        Args:
            args (argparse.Namespace): The parsed command-line arguments.

        Returns:
            None
        """
        if len(self.run_track["XYZ_inputs"]) == 0:
            return

        self.ems.submit_molrep_job_em(
            submit_qtype=args.submit_qtype,
            submit_queue=args.submit_queue,
        )

    def molrep_scoring(self) -> Optional[DataFrame]:
        """
        Gets Molrep output files, splits them into chains,
        generates scores and runs a classifier.

        The Molrep output files are split into chains because
        the Molrep output files contain
        multiple models, and the scoring is done on
        a per-model basis.

        Returns:
            pandas.DataFrame: A DataFrame containing the successful scores.
        """

        self.ems.get_scoring_inputs(
            "*-molrep-origin-shift.pdb"
        )  # Easy way to get the molrep output

        if len(self.ems.scoring_models) == 0:
            self.logger.info("\nNo Molrep outputs found, skipping scoring...")
            return None

        placed_model_length = len(self.file_tracker.return_all_file_pathways())
        for molrep_output in self.ems.scoring_models:
            molrep_output_extra = molrep_output.parent.joinpath(
                molrep_output.name.replace("-molrep-origin-shift", "_molrep")
            )
            self.submithelper.split_model_into_chains(
                molrep_output, placed_model_length
            )
            self.submithelper.split_model_into_chains(
                molrep_output_extra, placed_model_length
            )

        self.ems.get_scoring_inputs("*-molrep-origin-shift_*.pdb")

        # Generate scores and run classifier
        successful_scores_df = self.submithelper.generate_scores_and_run_classifier(
            self.ems, self.name_prefix
        )

        if successful_scores_df is None:
            self.logger.info("\nNo Molrep placements passed the classifier.")
            return None
        self.logger.info("\nMolrep placements passed the classifier.")

        return successful_scores_df

    def non_overlapping(self, successful_scores_df) -> None:
        """
        Copies the best non-overlapping models to the results directory.

        Args:
            successful_scores_df (pandas.DataFrame): A DataFrame
                containing the successful scores.

        Returns:
            None
        """
        non_overlapping_models = find_best_non_overlapping_models(
            successful_scores_df, "proba"
        )
        non_overlapping_models = self.submithelper.no_mol_limiter(
            non_overlapping_models,
            self.ems.nmol,
            names=[x.stem for x in self.ems.models],
        )
        self.submithelper.copy_files_to_results(
            self.file_tracker, non_overlapping_models
        )
        extra_models = [
            x.replace("-molrep-origin-shift", "_molrep") for x in non_overlapping_models
        ]
        extra_models = self.submithelper.no_mol_limiter(extra_models, self.ems.nmol)
        self.submithelper.copy_files_to_results(
            self.file_tracker, extra_models, key="extra"
        )

    def get_fixed_structure(self) -> dict:
        """
        Returns a dictionary containing the run track information with the
        path to the fixed file updated.

        This method calls the `get_fixed_structure` method of the
        `JobSubmissionHelper` class to update the path to the fixed file
        in the run track information dictionary. The updated dictionary
        is then returned.

        Returns:
            dict: A dictionary containing the updated run track information.

        Raises:
            None
        """
        return self.submithelper.get_fixed_structure(
            self.run_track, self.file_tracker, key="extra"
        )


class SubmitEMPlacement:
    """
    A class for submitting em_placement jobs.

    Args:
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        logger (logging.Logger): The logger object for logging messages.
        file_tracker (FileTracker): An instance of the FileTracker class.

    Attributes:
        logger (logging.Logger): The logger object for logging messages.
        ems (EmSubmit): An instance of the EmSubmit class.
        run_track (dict): A dictionary containing the run track information.
        file_tracker (FileTracker): An instance of the FileTracker class.
        name_prefix (str): The prefix for the job name.
        submithelper (JobSubmissionHelper): An instance of the
            JobSubmissionHelper class.
    """

    def __init__(self, ems, run_track, logger, file_tracker):
        self.logger = logger
        self.ems = ems
        self.run_track = run_track
        self.file_tracker = file_tracker
        self.name_prefix = "em_placement"
        self.submithelper = JobSubmissionHelper(logger)

    def run_em_placement(self, args) -> None:
        """
        Submits an em_placement job for each input XYZ file in the run track.

        Args:
            args (argparse.Namespace): The parsed command-line arguments.

        Returns:
            None
        """
        if len(self.run_track["XYZ_inputs"]) == 0:
            return

        self.ems.submit_em_placement_job(
            submit_qtype=args.submit_qtype,
            submit_queue=args.submit_queue,
        )

    def get_fixed_structure(self) -> dict:
        """
        Retrieves the fixed structure from the run track and file tracker.

         Returns:
            dict: A dictionary containing the updated run track information.
        """
        return self.submithelper.get_fixed_structure(self.run_track, self.file_tracker)

    def non_overlapping(self, successful_scores_df) -> None:
        """
        Copies the best non-overlapping models to the results directory.

        Args:
            successful_scores_df (pandas.DataFrame): A DataFrame containing
                the successful scores.

        Returns:
            None
        """
        non_overlapping_models = find_best_non_overlapping_models(
            successful_scores_df, "mapLLG"
        )
        non_overlapping_models = self.submithelper.no_mol_limiter(
            non_overlapping_models,
            self.ems.nmol,
            names=[x.stem for x in self.ems.models],
        )
        self.submithelper.copy_files_to_results(
            self.file_tracker, non_overlapping_models
        )


class JobSubmissionHelper:
    """
    A class that provides helper methods for submitting jobs.

    Args:
        logger (logging.Logger): The logger object for logging messages.

    Attributes:
        logger (logging.Logger): The logger object for logging messages.
    """

    def __init__(self, logger):
        self.logger = logger

    def get_fixed_structure(
        self, run_track: dict, file_tracker: FileTracker, key: str = "cycle"
    ) -> dict:
        """
        Updates the path to the fixed file in the run track information dictionary
        and returns the updated dictionary.

        Args:
            run_track (dict): A dictionary containing the run track information.
            file_tracker (FileTracker): An instance of the FileTracker class.
            key (str): The key to specify which files to retrieve from the file tracker.
                Defaults to "cycle".

        Returns:
            dict: A dictionary containing the updated run track information.

        Raises:
            None
        """
        current_successful_placements = file_tracker.return_all_file_pathways(key=key)

        fixed_file = combine_structures(
            current_successful_placements, file_tracker.results_directory
        )

        run_track["XYZ_fixed_track"] = fixed_file
        file_tracker.update_fixed_file(fixed_file)

        return run_track

    def copy_files_to_results(self, file_tracker, models, key="cycle"):
        """
        Copies the output files to the results directory using the FileTracker instance.

        Args:
            file_tracker (FileTracker): An instance of the FileTracker class.
            models (list): A list of file paths to the output files.
            key (str): The key to specify which files to retrieve from the
                file tracker. Defaults to "cycle".

        """
        [file_tracker.copy_output_file(x, key=key) for x in models]

    def split_model_into_chains(
        self, model: str, xyz_fixed_len: Optional[int] = None
    ) -> None:
        """
        Splits a PDB file containing a single model into separate files,
        each containing a single chain.

        Args:
            model (str): The path to the input PDB file.
            xyz_fixed_len (Optional[int]): The number of fixed structures to
                skip before splitting the model.
                Defaults to None.

        Returns:
            None

        Raises:
            None
        """

        structure = Structure.from_file(model)
        structures = structure.split_into_chains()
        if xyz_fixed_len is not None:
            structures = structures[xyz_fixed_len:]

        [  # sorry
            structure.to_file(
                filename=str(
                    Path(structure.filepathway)
                    .parent.joinpath(
                        Path(structure.filename).stem[:-1]
                        + str((ord(Path(structure.filename).stem[-1]) - 64))
                    )
                    .with_suffix(Path(structure.filepathway).suffix)
                )
            )
            for structure in structures
        ]

    def generate_scores_and_run_classifier(self, ems, name_prefix):
        """
        Generates scores and runs the fitting scores classifier.

        Args:
            ems (EmSubmit): An instance of the EmSubmit class.
            name_prefix (str): The prefix for the job name.

        Returns:
            successful_scores_df (pandas.DataFrame): A DataFrame containing
                the successful scores.

        Raises:
            ValueError: If no scoring models are found.
                In the EMSubmit class, the scoring models are
                found using a recursive glob or by setting a list of Path objects
                to the `scoring_models` attribute.
        """
        if len(ems.scoring_models) == 0:
            msg = "No scoring models found"
            raise ValueError(msg)
        self.logger.info("\nGenerating fitting scores for use in the classifier")
        scores_df_pathway = ems.submit_fitting_scores_job(
            submit_qtype="local",
            submit_queue=None,
        )
        # read in the csv file
        try:
            scores_df = read_csv(scores_df_pathway, index_col=0)
        except errors.EmptyDataError:
            self.logger.info("\nNo fitting scores generated")
            self.logger.error(
                "\nPlease check the fitting scores python executable and site-packages"
            )
            return None

        self.logger.info("\nRunning Classifier on generated fitting scores")
        # how to get the value of the classifier threshold when it's magic mock

        cl = Classifier(
            scores_df,
            dev=ems.dev,
            logger=self.logger,
            classifier_threshold=ems.classifier_threshold,
        )
        scores_df_name = f"{name_prefix}_fitting_scores.csv"
        # Keep a record of the successful runs
        if ems.working_dir.joinpath(scores_df_name).exists():
            cl.scores_df.to_csv(
                str(ems.working_dir.joinpath(scores_df_name)),
                mode="a",
                header=False,
            )
        else:
            cl.scores_df.to_csv(str(ems.working_dir.joinpath(scores_df_name)))

        successful_scores_df = cl.scores_df[cl.scores_df["class"] == 1]
        # check to see if any of the runs were successful
        if successful_scores_df.empty:
            return None

        self.logger.info("\nClassifier run complete")
        return successful_scores_df

    def no_mol_limiter(self, non_overlapping_models, no_mols, names=None):
        """
        Limits the number of models to be placed to the value of the `no_mols` argument.

        Args:
            non_overlapping_models (list): A list of file paths to the output
            no_mols (int): The number of models to be placed.
            names (list): A list of names to be used to limit the models.

        Returns:
            list: A list of file paths to the output models.
        """
        if names is None:
            if len(non_overlapping_models) > no_mols:
                non_overlapping_models = non_overlapping_models[:no_mols]
        else:
            models_temp = []
            for name in names:
                non_overlapping_models_named = [
                    x for x in non_overlapping_models if name in Path(x).stem
                ]
                if len(non_overlapping_models_named) > no_mols:
                    non_overlapping_models_named = non_overlapping_models_named[
                        :no_mols
                    ]
                models_temp.extend(non_overlapping_models_named)
            non_overlapping_models = models_temp

        return non_overlapping_models

    def combine_csvs(self, csv_files, outfile):
        """Combine the csv files into a single csv file,"""
        from pandas import read_csv, concat

        if len([f for f in csv_files if f.exists()]) == 0:
            self.logger.debug("\nNo csv files to combine, returning empty csv")
            Path(outfile).touch()
            return

        """Combine the csv files"""
        df = concat((read_csv(f, header=0) for f in csv_files if f.exists()))
        df.to_csv(outfile, index=False)

        return df

    @staticmethod
    def rename_rows_to_files(csv_file):
        """
        Renames the rows of a DataFrame to the file names.


        Args:
            csv_file (pathlib.Path): The path to the CSV file.

        Returns:
            pandas.DataFrame: The DataFrame with updated index.
        """
        df = pd.read_csv(csv_file, index_col=0, header=0)
        csv_path = Path(csv_file).parent
        model_name = Path(df["Model"].values[0]).stem

        def update_index(index):
            if isinstance(index, str):
                num = index.split("-")[-1].split(".")[0]
                return str(csv_path / f"{model_name}_docked_model{num}.pdb")
            return index

        df.index = df.index.map(update_index)
        df.to_csv(csv_file)
