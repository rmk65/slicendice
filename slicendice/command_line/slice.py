import os
from pathlib import Path

from slicendice.slice import slice
from struvolpy.Structure import Structure


def slice_model(
    args, xyzin, working_dir, min_splits, max_splits, output_json, **kwargs
):
    if int(max_splits) < int(min_splits):
        msg = "max_splits must be greater than or equal to min_splits"
        raise RuntimeError(msg)

    slice_kwargs = {}
    slice_options = [
        "bandwidth",
        "bfactor_column",
        "eps_value",
        "graph_resolution",
        "molecule_type",
        "no_mols",
        "pae_cutoff",
        "pae_file",
        "pae_power",
        "plddt_threshold",
        "rms_threshold",
        "defragger",
    ]
    for arg in args._get_kwargs():
        if arg[0] in slice_options and arg[1] is not None:
            slice_kwargs[arg[0]] = arg[1]

    xyz_list_mode = kwargs.get("xyz_list", False)  # Fix for json output

    sp = slice.Slice(
        xyzin,
        args.clustering_method,
        working_dir=working_dir,
        min_splits=min_splits,
        max_splits=max_splits,
        **slice_kwargs,
    )

    sp.run()
    sp.output_json_file(output_json, xyz_list_mode=xyz_list_mode)


class Slice(object):
    """Class to slice predicted models"""

    def __init__(self, logger, working_dir, output_json):
        self.logger = logger
        self.working_dir = working_dir
        self.output_json = output_json
        self.results = [[]]

    def run_slice(self, args):
        if args.xyzin and args.xyz_list:
            msg = "Provide xyzin or xyz_list, not both"
            raise RuntimeError(msg)
        if args.xyzin:
            self.logger.info(f"Slicing up model: {args.xyzin}\n")
            slice_model(
                args,
                args.xyzin,
                self.working_dir,
                args.min_splits,
                args.max_splits,
                self.output_json,
            )
            results = []
            for split_dir in self.working_dir.glob("split_*"):
                xyz_list = [str(xyz) for xyz in split_dir.glob("pdb_*_cluster*.pdb")]
                # Ensure that all splits contain atoms
                for xyz in xyz_list:
                    try:
                        Structure.from_file(xyz)
                    except AssertionError:
                        self.logger.debug(
                            f"No atoms found in {xyz}, removing from slice results"
                        )
                        xyz_list.remove(xyz)

            xyz_list.sort(
                key=lambda xyz: len(Structure.from_file(xyz).coor[0]), reverse=True
            )
            results.append(xyz_list)
        results.sort(key=len)

        self.results = results  # Not similar to the code below but should work

    def slice_xyz_list(self, args):
        args.mr_program == "hybrid"  # Set to hybrid mode if using xyz_list
        # - avoids re-running MR jobs
        for i, xyzin in enumerate(args.xyz_list):
            self.logger.info(f"\nSlicing up model: {xyzin}\n")
            name = Path(xyzin).stem
            slice_dir = self.working_dir.joinpath(name)
            if not slice_dir.exists():
                slice_dir.mkdir()

            if args.xyz_list_splits:
                if len(args.xyz_list_splits) == len(args.xyz_list):
                    min_splits = args.xyz_list_splits[i]
                    max_splits = args.xyz_list_splits[i]
                else:
                    msg = "Make sure length of xyz_list_splits matches length \
of xyz_list"
                    raise RuntimeError(msg)
            else:
                min_splits = 2
                max_splits = 2
            slice_model(
                args,
                xyzin,
                slice_dir,
                min_splits,
                max_splits,
                self.output_json,
                xyz_list=True,
            )

            split_dirs = list(self.working_dir.glob(os.path.join("*", "split_*")))
            for split_dir in split_dirs:
                xyz_list = [str(xyz) for xyz in split_dir.glob("pdb_*_cluster*.pdb")]
                for xyz in xyz_list:
                    try:
                        Structure.from_file(xyz)
                    except AssertionError:
                        self.logger.debug(
                            f"No atoms found in {xyz}, removing from slice results"
                        )
                        xyz_list.remove(xyz)

            self.results[0] += xyz_list

            self.results[0].sort(
                key=lambda xyz: len(Structure.from_file(xyz).coor[0]), reverse=True
            )
