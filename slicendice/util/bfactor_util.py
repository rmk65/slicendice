__author__ = "Adam Simpkin"
__date__ = "01 Feb 2022"
__version__ = "0.1"

import math

import gemmi
import numpy as np


def bfactor_best_guess(xyzin) -> str:
    """
    Function to try and guess b-factor column type of an input xyz file
    based on xyz source
    """

    struct = gemmi.read_structure(xyzin)
    struct.setup_entities()
    bfacs = []
    for chain in struct[0]:
        for residue in chain:
            for atom in residue:
                bfacs.append(atom.b_iso)
    min_bfac = min(bfacs)
    max_bfac = max(bfacs)

    possible_af2 = True
    if max_bfac > 100:
        possible_af2 = False

    with open(xyzin, "r") as f:
        header = f.readline()
        if "MODEL     1" in header and possible_af2:  # AlphaFold and Colabfold
            return "plddt"
        if "HEADER" in header:
            title = f.readline()
            if "ALPHAFOLD" in title:  # AFDB
                return "plddt"
            elif "ESMFOLD" in title:  # ESMFOLD
                return "fractional_plddt"
            else:
                return "bfactor"  # PDB
        if "ATOM" in header and possible_af2:
            if (
                0 <= min_bfac and 100 >= max_bfac
            ):  # AF2 doesn't always have MODEL in header, try and catch that
                return "plddt"

        line = f.readline()
        while line:
            if "AUTHOR BAKER-ROBETTA" in line:  # RosettaFold
                return "rms"
            line = f.readline()

    return "bfactor"  # Do nothing if we don't recognise the source


def convert_fractional_plddt_to_plddt(struct):
    for chain in struct[0]:
        for residue in chain:
            for atom in residue:
                lddt_value = atom.b_iso
                atom.b_iso = lddt_value * 100
    return struct


def convert_plddt_to_bfactor(struct):
    for chain in struct[0]:
        for residue in chain:
            for atom in residue:
                plddt_value = atom.b_iso
                atom.b_iso = _convert_plddt_to_bfactor(plddt_value)
    return struct


def _convert_plddt_to_bfactor(plddt):
    lddt = plddt / 100
    if lddt <= 0.5:
        return 657.97  # Same as the b-factor value with an rmsd estimate of 5.0
    rmsd_est = 0.6 / (lddt**3)
    bfactor = ((8 * (np.pi**2)) / 3.0) * (rmsd_est**2)
    return bfactor


def convert_rms_to_bfactor(struct):
    for chain in struct[0]:
        for residue in chain:
            for atom in residue:
                rms_value = atom.b_iso
                atom.occ, atom.b_iso = _convert_rms_to_bfactor(rms_value)
    return struct


def _convert_rms_to_bfactor(rms):
    multiplier = 8.0 / 3.0 * math.pi**2
    bmax = 999.0
    rms_max = math.sqrt(bmax / multiplier)
    rms_big = 4.0

    occ = max(min(1.0 - (rms - rms_big) / (rms_max - rms_big), 1.0), 0.0)
    bfactor = min(multiplier * rms**2, bmax)
    return occ, bfactor


def remove_residues_above_threshold(struct, threshold):
    for chain in struct[0]:
        res_to_remove = []
        for i, residue in enumerate(chain):
            if all([atom.b_iso > threshold for atom in residue]):
                res_to_remove.append(i)
            else:
                atoms_to_remove = []
                for j, atom in enumerate(residue):
                    value = atom.b_iso
                    if value > threshold:
                        atoms_to_remove.append(j)

                for j in atoms_to_remove[::-1]:
                    del residue[j]
        for i in res_to_remove[::-1]:
            del chain[i]
    return struct


def remove_residues_below_threshold(struct, threshold):
    for chain in struct[0]:
        res_to_remove = []
        for i, residue in enumerate(chain):
            if all([atom.b_iso < threshold for atom in residue]):
                res_to_remove.append(i)
            else:
                atoms_to_remove = []
                for j, atom in enumerate(residue):
                    value = atom.b_iso
                    if value < threshold:
                        atoms_to_remove.append(j)

                for j in atoms_to_remove[::-1]:
                    del residue[j]
        for i in res_to_remove[::-1]:
            del chain[i]
    return struct
