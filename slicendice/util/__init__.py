import logging
from pathlib import Path
from struvolpy import Structure
import subprocess
import os

logger = logging.getLogger(__name__)


def check_powerfit_installation() -> str:
    """
    Check if PowerFit is installed.

    Returns:
        str: The path to the PowerFit executable
        bool: True if PowerFit is installed
    """

    from shutil import which

    powerfit_exe = which("powerfit")

    if powerfit_exe is None:
        msg = "PowerFit is not installed or is not in your PATH. \
Please install PowerFit and try again."
        raise FileNotFoundError(msg)

    return powerfit_exe


def check_powerfit_gpu_functionality():
    """
    Check if PowerFit is installed with GPU functionality.

    Returns:
        bool: True if PowerFit is installed with GPU functionality
    """

    powerfit_exe = check_powerfit_installation()

    run = subprocess.run(
        [powerfit_exe, "a", "1.1", "b", "-g"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    if b"clGetPlatformIDs failed" in run.stderr:
        print(run.stderr.decode("utf-8"))
        raise RuntimeError(
            "PowerFit is installed but does not have GPU functionality."
            " Please install PowerFit with GPU functionality and try again."
        )

    # powerfit.log gets created in the current working directory,
    # so we need to remove it
    if Path("powerfit.log").exists():
        Path("powerfit.log").unlink()


def find_best_non_overlapping_models(model_scores, score_col):
    """
    Find the best non-overlapping models from a list of models and their scores.

    Args:
        model_scores (pd.DataFrame): A dataframe containing the models and their scores
        score_col (str): The name of the column containing the scores

    Returns:
        list: A list of the best non-overlapping models
    """

    def find_best_non_overlapping_models_helper(
        models,
        best_models,
    ):
        if len(models) == 0:
            return [x for x in best_models]

        if isinstance(models, dict):
            model = list(models.keys())[0]
        else:
            model = models[0]

        if isinstance(model, Structure):
            structure = model

        else:
            structure = Structure.from_file(model)
            structure.filename = model

        overlaps_with_best = False
        for best_model in best_models.values():
            # If the model is a string, convert it to a Structure instance
            if not isinstance(best_model, Structure):
                best_model = Structure.from_file(best_model)

            if structure.overlap(best_model):
                overlaps_with_best = True
                break
        # If the model does not overlap with any of the current best models,
        # add it to the list of best models
        if not overlaps_with_best:
            best_models[model] = structure

        # Recurse to find the best non-overlapping models
        return find_best_non_overlapping_models_helper(models[1:], best_models)

    # check all models are structures

    model_scores = {
        model_scores.index[i]: model_scores[score_col][i]
        for i in range(len(model_scores))
    }  # converts the index to a dict of the model names and their scores

    sorted_models = sorted(
        model_scores.keys(), key=lambda x: model_scores[x], reverse=True
    )  # sorts the models by their scores
    # Start the recursion
    best_non_overlapping_models = find_best_non_overlapping_models_helper(
        sorted_models, {}
    )

    # Return the list of best non-overlapping models
    return best_non_overlapping_models


def combine_structures(structures, working_dir, output_file_name="xyz_fixed.pdb"):
    """
    Combine a list of structures into one structure.

    Args:
        structures (list): A list of structures to combine
        working_dir (str): The working directory
        output_file_name (str): The name of the output file

    Returns:
        str: The path to the output file
    """

    working_dir = Path(working_dir)
    # Easier to initialize with a single structure out
    # of the loop and then combine the rest
    out_structure = Structure.from_file(structures[0])
    if not len(structures) == 1:
        for structure in structures[1:]:
            structure = Structure.from_file(structure)
            out_structure = out_structure.combine(structure)

    if working_dir.joinpath(output_file_name).exists():
        out_structure = out_structure.combine(
            Structure.from_file(working_dir.joinpath(output_file_name))
        )

    # Write the structure to a file
    out_structure.to_file(str(working_dir.joinpath(output_file_name)))

    return out_structure.filename  # return the pathway


def get_emplacement() -> str:
    """
    Check if emplacement is installed.

    Returns:
        str: The path to the emplacement executable
    """

    lib_path = os.environ["CCP4"]
    emplacement_path = os.path.join(lib_path, "libexec", "phaser_voyager.em_placement")

    if not os.path.exists(emplacement_path):
        return ""

    return emplacement_path
