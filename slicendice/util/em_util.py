import sys
import site
from shutil import which
import os
from pathlib import Path
from slicendice.dice import EmSubmit, CMD_PREFIX
from pyjob.script import Script, ScriptCollector
from simbad.util import submit_chunk
import re
import tempfile


OUTPUT_FILES = ["tmppyjob.sh", "tmppyjob.log"]


class EmSubmit_lite(EmSubmit):
    def __init__(self, python_exe=None, python_site_packages=None, **kwargs):
        self.external_python = python_exe
        self.external_python_path = python_site_packages
        self.collector = ScriptCollector(None)
        self.nproc = 1
        self.name = "em"
        self._parent_dir = kwargs.get("parent_dir", Path.cwd())
        self._working_dir = self._parent_dir

        self.excecutable = kwargs.get("excecutable", "python")

    def fitting_scores_em_external_help(self):
        """Generate the fitting scores scripts for EM processing
        Note: this is for external use, i.e. not using the CCP4 source
        This is for taking an input of a different python executable
        """
        pathway = Path(os.path.dirname(os.path.realpath(__file__)))
        fitting_scores_pathway = pathway.parent.joinpath(
            "dice", "scoresclassifier", "fittingscores.py"
        )
        fs_cmd = [
            CMD_PREFIX,
            self.excecutable,
            str(fitting_scores_pathway),
            "-h",
        ]

        cmd = [
            [CMD_PREFIX],
            ["export", f'"PATH={self.external_python}:$PATH"'],
            [
                "export",
                f'"PYTHONPATH={self.external_python_path}:$PYTHONPATH"',
            ],
            fs_cmd + ["\n"],
        ]

        run_script = Script(directory=self._working_dir)

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        return run_script, ""

    def submit_fitting_scores_job(self, submit_qtype, submit_queue):
        """Submit the fitting scores job"""
        run_files = []
        script, run_file = self.fitting_scores_em_external_help()

        self.collector.add(script)
        run_files.append(run_file)

        input_arguments = self.input_argument_generator(
            self.nproc,
            submit_qtype,
            submit_queue,
        )

        submit_chunk(*input_arguments)


def em_check(python_exe=None, python_site_packages=None):
    """Check if EM is possible.

    Keyword Arguments:
        python_exe {str} -- Path to python executable (default: {None})
        python_site_packages {str} -- Path to python site-packages (default: {None})

    Returns:
        bool -- True if EM is possible, False otherwise

    """

    if python_exe is None:
        fs_executables = em_fitting_scores()

        if fs_executables is not None:
            python_exe = executable = fs_executables[0]
            python_site_packages = fs_executables[1]
        else:
            # finds the ccp4-python executable
            python_exe = sys.executable
            python_exe = str(Path(python_exe).parent.parent.joinpath("bin"))
            executable = "ccp4-python"
            python_site_packages = site.getsitepackages()[0]

    else:
        executable = python_exe
    if not Path(python_exe).exists():
        return False

    if not Path(python_site_packages).exists():
        return False

    with tempfile.TemporaryDirectory() as temp_dir:
        emsl = EmSubmit_lite(
            python_exe,
            python_site_packages,
            excecutable=executable,
            parent_dir=Path(temp_dir),
        )

        emsl._working_dir = Path(temp_dir)
        emsl.submit_fitting_scores_job("local", None)
        error_checked = error_check(emsl._working_dir.joinpath(OUTPUT_FILES[1]))

    return error_checked


def error_check(log_file: Path):
    """Check the log file for errors"""
    error_message = re.compile(r"ModuleNotFoundError|ImportError")
    with open(str(log_file), "r") as f:
        for line in f:
            if error_message.search(line):
                return False
    return True


def temporary_directory():
    """Create a temporary directory"""
    with tempfile.TemporaryDirectory() as tmpdirname:
        yield tmpdirname


def em_fitting_scores():
    scores_exe = which("slicendice_fitting_scores")

    if scores_exe is None:
        return scores_exe
    else:
        return os.popen(scores_exe).read().split("\n")[:-1]


if __name__ == "__main__":
    em_check()
