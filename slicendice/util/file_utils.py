import json
import logging
import os
from pathlib import Path
from tabulate import tabulate
from typing import List, Dict, Tuple, Union
import pandas as pd

RESULTS_DIR = "results"
OUTPUT_DIR = "ungrouped-passed-output"
GROUPED_DIR = "grouped-passed-output"
NON_PASSED = "output"
FITTING_SCORES_NAME = "fittingscores"
FILENAME = "passed-models.json"

logger = logging.getLogger(__name__)


class FileTracker(object):
    """
    Class for tracking the output files of slicendice

    Args:
        parent_directory (str): The directory that the output files will be placed in

    Keyword Args:
        results_directory (str): The directory that the output files will be placed in
        json_file (str): The json file that will be used to track the output files

    """

    def __init__(self, parent_directory, **kwargs):
        self.parent_directory = Path(parent_directory)
        self.results_directory = kwargs.get(
            "results_directory", self.__results_directory()
        )

        if not self.results_directory.joinpath(
            FILENAME
        ).exists():  # check to see if it already exists
            self.json_file_pathway = kwargs.get("json_file", self.__initalise_json())
            self.__create_json_dict()
            self.__update_json()
            self.__current_cycle = 1
        else:
            logger.debug("\nLoading existing json file")
            self.json_file_pathway = kwargs.get(
                "json_file",
                self.results_directory.joinpath(FILENAME),
            )
            self.json_dict = self.__load_json()
            self.__get_current_cycle()

    def __load_json(self) -> Dict:
        """Loads the json file into a dictionary"""
        with open(self.json_file_pathway, "r") as json_file:
            json_dict = json.load(json_file)
        return json_dict

    def __initalise_json(self) -> Path:
        """Creates the json file"""
        json_path = self.results_directory.joinpath(FILENAME)
        json_path.touch()
        return json_path

    def __create_json_dict(self) -> None:
        """Creates the json dictionary"""
        self.json_dict = {}
        self.json_dict["parent_directory"] = str(self.parent_directory)
        self.json_dict["results_directory"] = str(self.results_directory)
        self.json_dict["fixed_xyz"] = None
        self.json_dict["cycle_1"] = []
        self.json_dict["extra_1"] = []

    def __update_json(self) -> None:
        """Updates the json file"""
        with open(self.json_file_pathway, "w") as json_file:
            json.dump(self.json_dict, json_file, indent=4)

    def __get_current_cycle(self) -> None:
        """Gets the current cycle number"""
        current_cycle = 0
        for key in self.json_dict:
            if key.startswith("cycle_"):
                if current_cycle <= int(key.split("_")[1]):
                    current_cycle = int(key.split("_")[1]) + 1

        self.json_dict[f"cycle_{current_cycle}"] = []
        self.json_dict[f"extra_{current_cycle}"] = []
        self.__current_cycle = current_cycle

    def __results_directory(self) -> Path:
        """Creates the results directory if it doesn't exist"""
        results_directory = self.parent_directory.joinpath(RESULTS_DIR)
        if not results_directory.exists():
            results_directory.mkdir()

        return results_directory

    def __track_output_file(self, output_file_path, key) -> None:
        """Tracks the output file and adds it to the json file"""
        self.json_dict[f"{key}_{self.__current_cycle}"].append(str(output_file_path))
        self.__update_json()

    def __add_value(
        self,
        fitting_scores_df: pd.DataFrame,
        file_stem: str,
        value_column: str = "proba",
    ) -> float:
        """Finds the proba value of a file"""

        fitting_scores_df = fitting_scores_df[
            fitting_scores_df["model_stem"] == file_stem
        ]
        return float(fitting_scores_df[value_column].values[0])

    def __find_score_df_and_slice_number(
        self,
        file: Path,
        prefix: str,
        subset_slice_number=None,
    ) -> Union[Tuple[None, None], Tuple[pd.DataFrame, str]]:
        """Finds the fitting score dataframe and slice number from the file name"""
        fitting_scores_df: pd.DataFrame = pd.read_csv(
            str(file),
            index_col=0,
            header=0,
        )
        fitting_scores_df["model_stem"] = [
            Path(x).stem for x in fitting_scores_df.index
        ]

        if fitting_scores_df.empty:
            return None, None

        if not any(
            [
                model_stem.startswith(prefix)
                for model_stem in fitting_scores_df["model_stem"].values
            ]
        ):
            return None, None

        fitting_scores_df = fitting_scores_df[
            fitting_scores_df["model_stem"].str.contains(prefix)
        ]

        if fitting_scores_df.empty:
            return None, None

        if subset_slice_number is not None:
            fitting_scores_df["n"] = fitting_scores_df["model_stem"].apply(
                lambda x: digit_finder(x.split("_")[-2])
            )
            fitting_scores_df = fitting_scores_df[
                fitting_scores_df["n"] == subset_slice_number
            ]
            fitting_scores_df.drop(columns=["n"], inplace=True)

            if fitting_scores_df.empty:
                return None, None

        slice_number: str = digit_finder(
            fitting_scores_df["model_stem"].values[0].split("_")[-2]
        )
        return fitting_scores_df, slice_number

    def find_common_prefixes(self, paths: List[str]) -> List[str]:
        paths = [
            "cluster".join(file_name.split("cluster")[:-1]) for file_name in paths
        ]  # if cluster is in filename too
        longest_prefixes: set = set()
        for i in range(len(paths)):
            for j in range(i + 1, len(paths)):
                new_prefix = os.path.commonprefix([paths[i], paths[j]])
                if all(
                    len(new_prefix) > len(existing_prefix)
                    for existing_prefix in longest_prefixes
                ):
                    longest_prefixes = {new_prefix}
                elif any(
                    len(new_prefix) == len(existing_prefix)
                    for existing_prefix in longest_prefixes
                ):
                    longest_prefixes.add(new_prefix)

        common_prefixes: List[str] = [
            Path(file_prefix).name for file_prefix in list(longest_prefixes)
        ] + self.non_common_prefixes(list(longest_prefixes), paths)
        return list(common_prefixes)

    def non_common_prefixes(
        self, common_prefixes: List[str], paths: List[str]
    ) -> List[str]:
        return [
            Path(path).stem
            for path in paths
            if not any(path.startswith(prefix) for prefix in common_prefixes)
        ]

    def copy_output_file(self, output_file_path, key="cycle"):
        """
        Copies the output file to the results directory and tracks it in the json file
        """
        output_file_path = Path(output_file_path)
        output_file_path.copy(
            self.results_directory.joinpath(output_file_path.name)
        )  # copy file to results directory

        self.__track_output_file(
            self.results_directory.joinpath(output_file_path.name), key=key
        )

    def return_all_file_pathways(self, key="cycle"):
        """Returns all the file pathways that have been tracked in the json file"""
        file_pathways = []
        [
            file_pathways.extend(self.json_dict[json_key])
            for json_key in self.json_dict.keys()
            if json_key.startswith(key)
        ]

        return file_pathways

    def update_fixed_file(self, fixed_file_path):
        """Updates the fixed file in the json file"""
        self.json_dict["fixed_xyz"] = str(fixed_file_path)
        self.__update_json()

    def move_files_to_grouped_directory(self, grouped_dictionary):
        """
        Moves the files to the grouped directory,
        the remains go to the ungrouped directory
        """

        grouped_directory = self.results_directory.joinpath(GROUPED_DIR)
        if not grouped_directory.exists():
            grouped_directory.mkdir()

        for group, structures in grouped_dictionary.items():
            for structure in structures:
                structure = Path(structure)
                new_name = structure.stem + f"-{group}" + structure.suffix
                structure.copy(grouped_directory.joinpath(new_name))
                structure.unlink()

    def move_remaining_files(self, copy: bool = False):
        """Moves the remaining files to the ungrouped directory"""
        ungrouped_directory = self.results_directory.joinpath(OUTPUT_DIR)
        if not ungrouped_directory.exists():
            ungrouped_directory.mkdir()
        for file_ in self.results_directory.glob("*.pdb"):
            file_path = Path(file_)
            file_path.copy(ungrouped_directory.joinpath(file_path.name))  # type: ignore
            if not copy:
                file_path.unlink()

    def move_file_to_directory(
        self, directory_name: str, files: List[str], copy: bool = False
    ):
        """Moves the files to a new directory"""
        directory = self.results_directory.joinpath(directory_name)
        if not directory.exists():
            directory.mkdir(exist_ok=True)
        for file_ in files:
            file_path: Path = Path(file_)
            file_path.copy(directory.joinpath(file_path.name))  # type: ignore
            if not copy:
                file_path.unlink()

    def top_n_non_passed(
        self,
        nmol: int,
        column_name: str = "proba",
    ):
        """
        Finds the top ranked non passed models
        and moves them to the non passed directory
        """
        passed_models: List[str] = [
            Path(file).stem for file in self.return_all_file_pathways()
        ]

        prefix_group_counts = self.get_prefix_group_counts(passed_models, nmol)

        for prefix, group_counts in prefix_group_counts.items():
            for model_name in passed_models:
                if not model_name.startswith(prefix):
                    continue

                slice_number: str = digit_finder(model_name.split("_")[-2])
                if slice_number in group_counts:
                    group_counts[slice_number] -= 1
                    if group_counts[slice_number] == 0:
                        del group_counts[slice_number]

            top_proba_models: List[str] = []
            search_csvs: List[Path] = self.get_search_csvs()

            for fitting_scores_df_path in search_csvs:
                fitting_scores_df, n = self.__find_score_df_and_slice_number(
                    fitting_scores_df_path, prefix
                )
                if fitting_scores_df is None or n not in group_counts:
                    continue

                fitting_scores_df.sort_values(
                    by=column_name, ascending=False, inplace=True
                )
                fitting_scores_df = fitting_scores_df[
                    fitting_scores_df[column_name] != -1
                ]
                fitting_scores_df["model_name"] = [
                    Path(x).name for x in fitting_scores_df.index
                ]

                for model in fitting_scores_df["model_name"]:
                    model = Path(model)

                    if model.stem not in passed_models:
                        continue
                    fitting_scores_df = fitting_scores_df[
                        fitting_scores_df["model_name"] != model.name
                    ]

                nmol = min(group_counts[n], len(fitting_scores_df))
                top_n_models: List[str] = list(fitting_scores_df.index[0:nmol])

                top_proba_models.extend(top_n_models)

            self.move_file_to_directory(NON_PASSED, top_proba_models, copy=True)

    def add_proba_json_to_directory(self, dir_: Path) -> Path:
        """Adds the proba json file to the results directory"""

        output_json: Dict[str, float] = {}
        common_prefix = self.find_common_prefixes(
            [file.stem for file in dir_.iterdir()]
        )

        for file in dir_.iterdir():
            file_name_check: str = file.stem.split("cluster")[-1]
            if (
                "molrep" in file_name_check
                and "-molrep-origin-shift" not in file_name_check
            ):
                continue

            for prefix in common_prefix:
                if not file.stem.startswith(prefix):
                    continue

                file_stem: str = (
                    file.stem
                    if not file.stem.split("-")[-1].isdigit()
                    else "-".join(file.stem.split("-")[:-1])
                )

                slice_number: str = digit_finder(file_stem.split("_")[-2])
                search_csvs = self.get_search_csvs()

                for fitting_scores_df_path in search_csvs:
                    fitting_scores_df, n = self.__find_score_df_and_slice_number(
                        fitting_scores_df_path, prefix, subset_slice_number=slice_number
                    )

                    if fitting_scores_df is None:
                        continue

                    if n != slice_number:
                        continue
                    value_column = (
                        "proba" if "proba" in fitting_scores_df.columns else "mapLLG"
                    )
                    output_json[file_stem] = self.__add_value(
                        fitting_scores_df, file_stem, value_column=value_column
                    )

        with open(dir_.joinpath("model_proba.json"), "w") as outfile:
            json.dump(output_json, outfile, indent=4)

        return dir_.joinpath("model_proba.json")

    def get_prefix_group_counts(
        self,
        passed_models: List[str],
        nmol: int,
    ) -> Dict[str, Dict[str, int]]:

        xyz_prefixes: List[str] = self.find_input_files(
            self.parent_directory
        )  # for xyz_lists

        prefix_passed_models: Dict[str, List[str]] = {
            prefix: [model for model in passed_models if model.startswith(prefix)]
            for prefix in xyz_prefixes
        }

        prefix_unique_groups: Dict[str, List[str]] = {
            prefix: (
                list(set([digit_finder(model.split("_")[-2]) for model in models]))
                if models
                else [digit_finder(prefix.split("_")[-1])]
            )
            for prefix, models in prefix_passed_models.items()
        }

        return {
            prefix: {group: nmol for group in groups}
            for prefix, groups in prefix_unique_groups.items()
        }

    def get_search_csvs(self) -> List[Path]:
        return [
            fitting_scores_df
            for fitting_scores_df in self.parent_directory.rglob("*fitting_scores.csv")
        ] + [
            fitting_scores_df
            for fitting_scores_df in self.parent_directory.rglob(
                "output_table_docking.csv"
            )
        ]

    def find_input_files(self, split_directory: Path) -> List[str]:
        """
        In the split directory, it finds any folders which end in a digit
        and then finds the input files ran in those
        folders by using rglob
        """
        input_files: List[str] = []
        dir_names = ["molrep", "powerfit", "em_placement"]
        for dir_ in split_directory.iterdir():
            if (
                not any(
                    [
                        dir_.name.startswith(docking_method)
                        for docking_method in dir_names
                    ]
                )
                or not dir_.is_dir()
            ):
                continue

            for file in dir_.rglob("*[0-9].pdb"):
                if "molrep" in file.stem and "-molrep-origin-shift" not in file.stem:
                    continue
                input_files.append("_".join(file.stem.split("_")[:-1]))

        return list(set(input_files))


class ReportOutput:
    """
    Class for reporting the output of slicendice

    Args:
        logger (logging.Logger): The logger to be used

    """

    def __init__(self, logger: logging.Logger):
        self.logger = logger

    def shorten_name(self, name: str) -> str:
        """Shortens the name of the file"""
        name = name.split("_cluster")[0]
        return name[4:24] + "..." if len(name) > 24 else name

    def extract_cluster_model_from_name(self, name: str) -> Tuple[str, str]:
        """Extracts the cluster and model number from the name"""
        return digit_finder(name.split("_")[-2]), name.split("_")[-1][0]

    def report_groupings(self, grouped_dict: Dict[str, List[str]]) -> bool:
        """Reports the groupings of the models"""
        if not grouped_dict:
            return False
        grouped_dict = {
            k: [Path(structure).stem for structure in grouped_dict[k]]
            for k in sorted(grouped_dict.keys())
        }

        self.boxed_title("Grouped models")
        self.logger.info(
            tabulate(
                [
                    [
                        self.shorten_name(structure),
                        group,
                        *self.extract_cluster_model_from_name(structure),
                    ]
                    for group, structures in grouped_dict.items()
                    for structure in structures
                ],
                headers=["Slice name", "Group", "Cluster", "Model number"],
                tablefmt="pipe",
            )
        )
        return True

    def report_probabilities(self, probas: List[Path]) -> None:
        """Reports the probabilities of the models"""
        model_proba: Dict[str, float] = {}

        for json_path in probas:
            if json_path.parent.name == NON_PASSED:
                continue
            if not json_path.exists():
                continue
            with open(json_path, "r") as json_file:
                json_dict = json.load(json_file)
                model_proba.update(json_dict)

        if not model_proba:
            return
        self.boxed_title("Model probabilities")
        self.logger.info(
            tabulate(
                [
                    [
                        self.shorten_name(name),
                        *self.extract_cluster_model_from_name(name),
                        "{:.4f}".format(proba),
                    ]
                    for name, proba in model_proba.items()
                ],
                headers=[
                    "Slice name",
                    "Cluster",
                    "Model number",
                    "Score",
                ],
                tablefmt="pipe",
            )
        )

    def report_non_passed_models(self, proba: List[Path]) -> None:
        model_proba: Dict[str, float] = {}

        for json_path in proba:
            if not json_path.parent.name == NON_PASSED:
                continue
            if not json_path.exists():
                continue
            with open(json_path, "r") as json_file:
                json_dict = json.load(json_file)
                model_proba.update(json_dict)

        if not model_proba:
            return
        self.boxed_title("Non-passed models")
        self.logger.info(
            tabulate(
                [
                    [
                        self.shorten_name(name),
                        *self.extract_cluster_model_from_name(name),
                        "{:.4f}".format(proba),
                    ]
                    for name, proba in model_proba.items()
                ],
                headers=[
                    "Slice name",
                    "Cluster",
                    "Model number",
                    "Score",
                ],
                tablefmt="pipe",
            )
        )

    def report_no_mols_passed(
        self, proba: List[Path], nmol: int, xyz_list: bool = False
    ) -> bool:
        model_proba: Dict[str, Dict[str, float]] = {"passed": {}, "non_passed": {}}
        for json_path in proba:
            if not json_path.exists():
                continue
            with open(json_path, "r") as json_file:
                json_dict = json.load(json_file)
                if json_path.parent.name == NON_PASSED:
                    model_proba["non_passed"].update(json_dict)
                else:
                    model_proba["passed"].update(json_dict)

        if not model_proba["passed"]:
            return False
        cluster_model_counts: Dict[str, int] = {}
        for name in model_proba["passed"]:
            cluster_model = "_".join(digit_finder(name.split("_")[-2]))

            if cluster_model not in cluster_model_counts:
                cluster_model_counts[cluster_model] = 1
            else:
                cluster_model_counts[cluster_model] += 1

        for name in model_proba["non_passed"]:
            cluster_model = self.extract_cluster_model_from_name(name)[0]
            if cluster_model not in cluster_model_counts:
                cluster_model_counts[cluster_model] = 0

        self.boxed_title(
            f"Number of models passed / {nmol}"
            if not xyz_list
            else "Number of models passed"
        )

        self.logger.info(
            tabulate(
                [
                    (
                        [cluster_model, f"{count} / {nmol}"]
                        if not xyz_list
                        else [cluster_model, str(count)]
                    )
                    for cluster_model, count in sorted(cluster_model_counts.items())
                ],
                headers=["Cluster", "Number of models"],
                tablefmt="pipe",
            )
        )

        return True

    def boxed_title(self, title: str) -> None:
        """
        Logs a boxed title
        """
        title_length = len(title)
        box_top = "+" + "-" * (title_length + 2) + "+"
        box_middle = "| " + title + " |"
        box_bottom = "+" + "-" * (title_length + 2) + "+"
        self.logger.info("\n")
        self.logger.info(box_top)
        self.logger.info(box_middle)
        self.logger.info(box_bottom)


def digit_finder(string: str) -> str:
    """Finds the first digit in a string"""
    digs: str = ""
    for char in string:
        if char.isdigit():
            digs += char
        else:
            break
    return digs


def _copy(self, new_path: Union[str, Path]) -> None:
    # taken from
    # https://stackoverflow.com/questions/33625931/copy-file-with-pathlib-in-python
    """
    Copy the file to a new location
    """
    import shutil

    assert self.is_file()
    shutil.copy(str(self), str(new_path))


Path.copy = _copy  # type: ignore
