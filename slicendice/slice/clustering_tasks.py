__author__ = "Ronan Keegan & Adam Simpkin"
__date__ = "25 Jan 2022"
__version__ = "0.1"

import abc
import logging
from mmtbx import domains_from_pae
import numpy as np
from pathlib import Path
import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    # Ignore issue with deprecated import, this will likely be fixed soon
    from sklearn import cluster

logger = logging.getLogger(__name__)


class ClusterTask(abc.ABC):
    """Abstract base class for clustering tasks"""

    def __init__(self, data, nclust=1, *args, **kwargs):
        self.data = data
        self.nclust = nclust

    def __enter__(self):
        """Contextmanager entry function"""
        return self

    def __exit__(self, *args):
        """Contextmanager exit function"""
        return

    @abc.abstractmethod
    def run(self):
        """Abstract property to run clustering task`"""
        return

    @staticmethod
    def convert_pae_result(result):
        """Converts result from PAE matrix to SKLearn format"""
        nres = sum([len(list(x)) for x in result])
        array = np.zeros(nres)
        result = sorted(result)
        for _, indices in enumerate(result):
            for idx in indices:
                array[idx] = cluster
        return array

    @staticmethod
    def merge_clusters(data, array, max_splits):
        centroids = list(np.zeros_like(array))

        for label in set(array):
            xyz_data = []
            for xyz, label2 in zip(data, array):
                if label2 == label:
                    xyz_data.append(xyz)
            for idx, label2 in enumerate(array):
                if label2 == label:
                    centroids[idx] = np.mean(xyz_data, axis=0)

        return (
            cluster.AgglomerativeClustering(n_clusters=max_splits)
            .fit(np.asarray(centroids))
            .labels_
        )


class SliceAgglometative(ClusterTask):
    def run(self):
        model = cluster.AgglomerativeClustering(n_clusters=self.nclust).fit(self.data)
        return model.labels_


class SliceBirch(ClusterTask):
    def run(self):
        model = cluster.Birch(n_clusters=self.nclust).fit(self.data)
        return model.labels_


class SliceDBscan(ClusterTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.eps = kwargs.get("eps_value", 10)

    def run(self):
        model = cluster.DBSCAN(eps=self.eps).fit(self.data)
        return model.labels_


class SliceKMeans(ClusterTask):
    def run(self):
        model = cluster.KMeans(n_clusters=self.nclust).fit(self.data)
        return model.labels_


class SliceMeanShift(ClusterTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bandwidth = kwargs.get("bandwidth", None)

    def run(self):
        model = cluster.MeanShift(bandwidth=self.bandwidth).fit(self.data)
        return model.labels_


class SliceOptics(ClusterTask):
    def run(self):
        model = cluster.OPTICS().fit(self.data)
        return model.labels_


class SlicePaeNetworkx(ClusterTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.graph_resolution = kwargs.get("graph_resolution", 1)
        self.pae_file = kwargs.get("pae_file", None)
        self.pae_power = kwargs.get("pae_power", 1)
        self.pae_cutoff = kwargs.get("pae_cutoff", 5)

        assert Path(
            self.pae_file
        ).exists(), f"{self.pae_file} not found, please supply PAE file"

    def run(self):
        matrix = domains_from_pae.parse_pae_file(self.pae_file)
        model = domains_from_pae.domains_from_pae_matrix_networkx(
            matrix,
            graph_resolution=self.graph_resolution,
            pae_power=self.pae_power,
            pae_cutoff=self.pae_cutoff,
        )
        array = self.convert_pae_result(model)
        if self.nclust and self.nclust != "auto":
            array = self.merge_clusters(
                data=self.data, array=array, max_splits=self.nclust
            )
        return array


class SlicePaeIGraph(ClusterTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.graph_resolution = kwargs.get("graph_resolution", 1)
        self.pae_file = kwargs.get("pae_file", None)
        self.pae_power = kwargs.get("pae_power", 1)
        self.pae_cutoff = kwargs.get("pae_cutoff", 5)

        assert Path(
            self.pae_file
        ).exists(), f"{self.pae_file} not found, please supply PAE file"

    def run(self):
        matrix = domains_from_pae.parse_pae_file(self.pae_file)
        model = domains_from_pae.domains_from_pae_matrix_igraph(
            matrix,
            graph_resolution=self.graph_resolution,
            pae_power=self.pae_power,
            pae_cutoff=self.pae_cutoff,
        )
        array = self.convert_pae_result(model)
        if self.nclust and self.nclust != "auto":
            array = self.merge_clusters(
                data=self.data, array=array, max_splits=self.nclust
            )
        return array
