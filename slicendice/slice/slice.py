__author__ = "Ronan Keegan & Adam Simpkin"
__date__ = "24 Jan 2022"
__version__ = "0.1"


import gemmi
import json
import logging
import numpy as np
from pathlib import Path
from slicendice.slice.cluster import clusterfactory
from slicendice.util import bfactor_util

logger = logging.getLogger(__name__)


class SliceRunError(Exception):
    pass


class Slice(object):
    """A class to cluster and split molecular models"""

    def __init__(self, xyzin, cluster_method, **kwargs):
        self.xyzin = xyzin
        self.cluster_method = cluster_method
        self.input_pdb = None
        self.labels = {}

        self.atomic_data = []
        self.bandwidth = kwargs.get("bandwidth", None)
        self.bfactor_column = kwargs.get("bfactor_column", "bfactor")
        self.defragger = kwargs.get("defragger", False)
        self.eps_value = kwargs.get("eps_value", 10)
        self.graph_resolution = kwargs.get("graph_resolution", 1)
        self.max_splits = kwargs.get("max_splits", 3)
        self.min_splits = kwargs.get("min_splits", 1)
        self.molecule_type = kwargs.get("molecule_type", "mixed")
        self.no_mols = kwargs.get("no_mols", 1)
        self.pae_cutoff = kwargs.get("pae_cutoff", 5)
        self.pae_file = kwargs.get("pae_file", None)
        self.pae_power = kwargs.get("pae_power", 1)
        self.plddt_threshold = kwargs.get("plddt_threshold", 100)
        self.residue_idx = []
        self.rms_threshold = kwargs.get("rms_threshold", 100)
        self.working_dir = kwargs.get("working_dir", Path.cwd())

    def get_atomic_data(self, xyzin, res_idx_to_keep=None):
        """Function to extract XYZ CA/P coordinates from input XYZ file.

        Args:
            xyzin: Input XYZ file.

        Returns:
            Numpy array of XYZ CA/P data.
        """
        array = []
        struct = gemmi.read_structure(str(xyzin))
        struct.setup_entities()
        struct.remove_ligands_and_waters()

        for model in struct:
            for chain in model:
                for i, residue in enumerate(chain):
                    if res_idx_to_keep and i not in res_idx_to_keep:
                        continue
                    else:
                        ca = residue.get_ca()
                        p = residue.get_p()
                        if ca and self.molecule_type in ["protein", "mixed"]:
                            array.append([ca.pos.x, ca.pos.y, ca.pos.z])
                            self.residue_idx.append(
                                f"{model.num}:{chain.name}:{residue.seqid}"
                            )
                        elif p and self.molecule_type in ["na", "mixed"]:
                            array.append([p.pos.x, p.pos.y, p.pos.z])
                            self.residue_idx.append(
                                f"{model.num}:{chain.name}:{residue.seqid}"
                            )

        if len(array) < 1:
            if self.bfactor_column in ["plddt", "lddt", "predicted_plddt"]:
                msg = f"No atoms found in your input model after removing residues \
below pLDDT threshold: {self.plddt_threshold}\nTry lowering the pLDDT threshold using  \
the --plddt_threshold flag"

                raise SliceRunError(msg)
            elif self.bfactor_column == "rms":

                msg = f"No atoms found in your input model after removing residues \
above RMS threshold: {self.rms_threshold}\nTry increasing the RMS threshold using the \
--rms_threshold flag"
                raise SliceRunError(msg)

        return np.array(array)

    def process_input_model(self, xyzin, xyzout):
        """Function to process input xyzin and remove residues below threshold

        Args:
            xyzin: Path to input XYZ file
            xyzout: Path to output XYZ file

        Returns:
            Output XYZ file
        """

        struct = gemmi.read_structure(str(xyzin))
        struct.setup_entities()
        struct.remove_ligands_and_waters()
        if self.bfactor_column == "plddt":
            struct = bfactor_util.remove_residues_below_threshold(
                struct, self.plddt_threshold
            )
            struct = bfactor_util.convert_plddt_to_bfactor(struct)
        if self.bfactor_column == "predicted_bfactor":
            bfactor_threshold = bfactor_util._convert_plddt_to_bfactor(
                self.plddt_threshold
            )
            struct = bfactor_util.remove_residues_above_threshold(
                struct, bfactor_threshold
            )
        elif self.bfactor_column == "rms":
            struct = bfactor_util.remove_residues_above_threshold(
                struct, self.rms_threshold
            )
            struct = bfactor_util.convert_rms_to_bfactor(struct)
        elif self.bfactor_column == "fractional_plddt":
            struct = bfactor_util.convert_fractional_plddt_to_plddt(struct)
            struct = bfactor_util.remove_residues_below_threshold(
                struct, self.plddt_threshold
            )
            struct = bfactor_util.convert_plddt_to_bfactor(struct)

        if all([model.count_atom_sites() > 0 for model in struct]):
            with open(xyzout, "w") as f_out:
                f_out.write(struct.make_minimal_pdb())

    def output_json_file(self, json_out, xyz_list_mode=False):
        struct = gemmi.read_structure(str(self.input_pdb))
        struct.setup_entities()
        struct.remove_ligands_and_waters()
        res_list = [
            f"M{model.num}:{chain.name}:{residue.seqid.num}"
            for model in struct
            for chain in model
            for residue in chain
        ]

        if Path(json_out).exists():  # so we can append to existing json
            with open(json_out, "r") as f:
                results = json.load(f)
        else:
            results = {"slice": {}}

        for label in sorted(self.labels.keys()):

            array = self.labels[label]
            res_ranges = {}
            idx = 0
            while idx < (len(array)):
                start_pos = idx
                val = array[idx]

                while idx < len(array) and array[idx] == val:
                    idx += 1
                end_pos = idx - 1

                if str(val) in res_ranges.keys():
                    res_ranges[str(val)].append(
                        [res_list[start_pos], res_list[end_pos]]
                    )
                else:
                    res_ranges[str(val)] = [[res_list[start_pos], res_list[end_pos]]]

            split_dir = Path(self.working_dir, f"split_{label}")

            split_key = (
                f"split_{label}"
                if not xyz_list_mode
                else f"{self.input_pdb.parent.name}_split_{label}"
            )

            slice_dict = {
                "residues_ranges": dict(sorted(res_ranges.items())),
                "models": sorted([str(x) for x in split_dir.glob("*.pdb")]),
            }

            if not results.get("slice", False):
                results["slice"].update({split_key: slice_dict})
            else:
                results["slice"][split_key] = slice_dict

        with open(json_out, "w") as outfile:
            json.dump(results, outfile, indent=4)

    @staticmethod
    def output_split_model(xyzin, xyzout, seqid_to_keep):
        """Function to take part of xyzin by index and output it as xyzout

        Args:
            xyzin (str): Path to input XYZ file
            xyzout (str): Path to output XYZ file
            seqid_to_keep (list): List of seqids in the form ['model:chain:seqid']

        Returns:
            None
        """
        struct = gemmi.read_structure(str(xyzin))
        struct.setup_entities()
        struct.remove_ligands_and_waters()
        count = 0
        for model in struct:
            for chain in model:
                to_remove = []
                for idx, residue in enumerate(chain):
                    if f"{model.num}:{chain.name}:{residue.seqid}" not in seqid_to_keep:
                        to_remove.append(idx)
                    count += 1

                for i in to_remove[::-1]:
                    del chain[i]

        if all([model.count_atom_sites() > 0 for model in struct]):
            with open(xyzout, "w") as f_out:
                f_out.write(struct.make_minimal_pdb())

    def run(self):
        """Run function for Slice class"""
        if self.cluster_method in ["dbscan", "meanshift", "optics"]:
            logger.info(
                f"Automatically splitting {Path(self.xyzin).name} into clusters"
            )
            self.split_model("auto")
        elif (
            self.cluster_method in ["pae_networkx", "pae_igraph"]
            and self.max_splits == "auto"
        ):
            logger.info(
                f"Automatically splitting {Path(self.xyzin).name} into clusters"
            )
            self.split_model("auto")
        else:
            for nclust in range(self.min_splits, int(self.max_splits) + 1):
                logger.info(f"Splitting {Path(self.xyzin).name} into {nclust} clusters")
                self.split_model(nclust)
        return

    def defrag_clusters(self, labels):  # noqa: C901
        """A function to defrag clusters based on DBSCAN clustering.

        Args:
            labels (np.array): Cluster labels.

        Returns:
            np.array: Updated cluster labels.
        """

        updated_labels = np.zeros_like(labels)
        for x in set(labels):
            res_idx_to_keep = np.where(labels == x)[0].tolist()
            seqid_to_keep = [self.residue_idx[x] for x in res_idx_to_keep]
            working_xyz = Path(self.working_dir, "input.pdb")
            struct = gemmi.read_structure(str(working_xyz))
            struct.setup_entities()
            struct.remove_ligands_and_waters()
            split_array = []
            for model in struct:
                for chain in model:
                    for idx, residue in enumerate(chain):
                        if f"{model.num}:{chain.name}:{residue.seqid}" in seqid_to_keep:
                            ca = residue.get_ca()
                            p = residue.get_p()
                            if ca and self.molecule_type in ["protein", "mixed"]:
                                split_array.append([ca.pos.x, ca.pos.y, ca.pos.z])
                            elif p and self.molecule_type in ["na", "mixed"]:
                                split_array.append([p.pos.x, p.pos.y, p.pos.z])

            with clusterfactory("dbscan", split_array) as cluster:
                split_labels = cluster.run()

            if len(set(split_labels)) == 1:
                logger.debug("No fragments found")
                for i in res_idx_to_keep:
                    updated_labels[i] = x
                continue

            logger.info("Defragging model")
            total_length = len(split_labels)
            unique_labels, counts = np.unique(split_labels, return_counts=True)

            threshold = 0.3 * total_length

            clusters_to_keep = [
                label
                for label, count in zip(unique_labels, counts)
                if count > threshold
            ]

            clusters_to_remove = [
                label for label in unique_labels if label not in clusters_to_keep
            ]

            if not clusters_to_keep:
                largest_cluster = unique_labels[np.argmax(counts)]
                clusters_to_keep.append(largest_cluster)

            if len(clusters_to_keep) > 1:
                logger.info("Input file might benefit from additional splitting.")

            for i in clusters_to_remove:
                split_labels[split_labels == i] = -1
            for i in clusters_to_keep:
                split_labels[split_labels == i] = x

            for i, j in enumerate(res_idx_to_keep):
                updated_labels[j] = split_labels[i]

        return updated_labels

    def split_model(self, nclust):
        """A function to split a model into n clusters.

        Args:
            nclust (int): Number of clusters to make.

        Returns:
            file: Output PDB file for each cluster.
        """
        split_dir = Path(self.working_dir, f"split_{nclust}")
        split_dir.mkdir()

        working_xyz = Path(self.working_dir, "input.pdb")

        # Need all atoms when clustering based on PAE for cluster merging step
        if self.cluster_method not in ["pae_networkx", "pae_igraph"]:
            self.process_input_model(self.xyzin, working_xyz)
            data = self.get_atomic_data(working_xyz)
        else:
            data = self.get_atomic_data(self.xyzin)

        cluster_kwargs = {
            "bandwidth": self.bandwidth,
            "eps_value": self.eps_value,
            "graph_resolution": self.graph_resolution,
            "pae_cutoff": self.pae_cutoff,
            "pae_file": self.pae_file,
            "pae_power": self.pae_power,
        }

        with clusterfactory(
            self.cluster_method, data, nclust, **cluster_kwargs
        ) as cluster:
            labels = cluster.run()

        if self.defragger:
            labels = self.defrag_clusters(labels)

        self.labels[nclust] = labels

        input_name = Path(self.xyzin).stem
        for x in set(labels):
            if x == -1:
                output_pdb = split_dir.joinpath(f"pdb_{input_name}_outliers.pdb")
            else:
                output_pdb = split_dir.joinpath(f"pdb_{input_name}_cluster_{x}.pdb")

            res_idx_to_keep = np.where(labels == x)[0].tolist()
            seqid_to_keep = [self.residue_idx[x] for x in res_idx_to_keep]
            if len(seqid_to_keep) > 0:
                # Process the model after clustering to convert B-factors and remove
                # low pLDDT regions
                if self.cluster_method in ["pae_networkx", "pae_igraph"]:
                    self.output_split_model(self.xyzin, working_xyz, seqid_to_keep)
                    self.process_input_model(working_xyz, output_pdb)
                    self.input_pdb = self.xyzin
                else:
                    self.output_split_model(working_xyz, output_pdb, seqid_to_keep)
                    self.input_pdb = working_xyz
