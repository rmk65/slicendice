__author__ = "Adam Simpkin"
__date__ = "25 Jan 2022"
__version__ = "0.1"

import importlib
import logging


CLUSTERING_METHODS = {
    "agglomerativeclustering": (
        "slicendice.slice.clustering_tasks",
        "SliceAgglometative",
    ),
    "birch": ("slicendice.slice.clustering_tasks", "SliceBirch"),
    "dbscan": ("slicendice.slice.clustering_tasks", "SliceDBscan"),
    "kmeans": ("slicendice.slice.clustering_tasks", "SliceKMeans"),
    "meanshift": ("slicendice.slice.clustering_tasks", "SliceMeanShift"),
    "optics": ("slicendice.slice.clustering_tasks", "SliceOptics"),
    "pae_networkx": ("slicendice.slice.clustering_tasks", "SlicePaeNetworkx"),
    "pae_igraph": ("slicendice.slice.clustering_tasks", "SlicePaeIGraph"),
}

logger = logging.getLogger(__name__)


class ClusteringMethodNotFoundError(Exception):
    pass


def clusterfactory(method, *args, **kwargs):
    """Accessibility function for SciPy clustering algorithms in Slice'n'Dice"""

    method = method.lower()
    if method in CLUSTERING_METHODS:
        logger.debug("Found method in available clustering methods")
        module, class_ = CLUSTERING_METHODS[method]
        return getattr(importlib.import_module(module), class_)(*args, **kwargs)
    else:
        raise ClusteringMethodNotFoundError(f"Unknown method: {method}")
