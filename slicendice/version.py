"""The version number of Slice'n'Dice is recorded here"""

# Maintain sematantic versioning. Further information can
# be found here [http://semver.org/]
__version_info__ = (0, 1, 3)

# ======================================================
# Do __NOT__ change anything below here
__snd_version__ = ".".join(map(str, __version_info__))
