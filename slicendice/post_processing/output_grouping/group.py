__author__ = "Luc Elliott"
__date__ = "06 Oct 2023"
__version__ = "1.0"


import itertools
import numpy as np
from pathlib import Path
from scipy.optimize import least_squares


class GroupOutputs:
    """A class representing a group of structures.

    Attributes:
        cluster_id_structures (dict): A dictionary mapping cluster IDs to lists of
            structure IDs.
            structures_connection_points (dict): A dictionary
            mapping structure IDs to their connection points.
        n (int): The number of groups.
        joining_models (dict): A dictionary mapping structure IDs to their joining
            models.
        cr: A chain restriction object.
    """

    def __init__(self, cr, cluster_id_structures, structures_connection_points, n):
        self.cluster_id_structures = cluster_id_structures
        self.structures_connection_points = structures_connection_points
        self.n = int(n)
        self.joining_models = {}
        self.cr = cr
        self.threshold = 20

    def shortest_distances(self):
        """
        Computes the shortest distances between all pairs of structures
        in different clusters.

        Returns:
            dict: A dictionary mapping cluster ID pairs to their shortest distance,
            and the filenames of the structures that are closest.
        """
        distances = {}
        for cluster_id_i, cluster_id_j in itertools.combinations(
            self.cluster_id_structures.keys(), 2
        ):
            structuresi = self.cluster_id_structures[cluster_id_i]
            structuresj = self.cluster_id_structures[cluster_id_j]

            if cluster_id_i == cluster_id_j:
                continue
            for structurei in structuresi:
                for structurej in structuresj:
                    intermediate_connection_magnitude = None
                    key1, key2 = (
                        self.__key_formatter(cluster_id_i, cluster_id_j),
                        self.__key_formatter(cluster_id_j, cluster_id_i),
                    )

                    if (self.cr.centre_of_mass_to_connection.get(key1) is None) or (
                        self.cr.centre_of_mass_to_connection.get(key2) is None
                    ):
                        # if there is no direct connection between the two clusters
                        intermediate_connection = self.__single_intermediate_connection(
                            cluster_id_i, cluster_id_j
                        )

                        key1, key2, intermediate_connection_magnitude = (
                            intermediate_connection
                        )

                    if key1 is None or key2 is None:
                        continue

                    point1 = self.__get_structure_connection_point(structurei, key1)
                    point2 = self.__get_structure_connection_point(structurej, key2)

                    difference = np.linalg.norm(point1 - point2)
                    if not self.__whithin_distance_threshold_check(
                        difference, intermediate_connection_magnitude
                    ):
                        continue

                    new_key = self.__key_formatter(cluster_id_i, cluster_id_j)
                    if distances.get(new_key, (np.inf, None, None))[0] > difference:
                        distances[new_key] = (
                            difference,
                            Path(structurei.filename).stem,
                            Path(structurej.filename).stem,
                        )

        return distances

    def __whithin_distance_threshold_check(self, difference, magnitude):
        """
        Checks if the difference between two points is within a threshold.
        """
        difference -= magnitude if magnitude is not None else 0

        if (difference > self.threshold) or (difference < 0):
            return False
        else:
            return True

    def __single_intermediate_connection(self, cluster_id_i, cluster_id_j):
        for key in self.cr.connection_magnitudes:
            if key.startswith(
                "_".join([str(x) for x in sorted([cluster_id_i, cluster_id_j])])
            ):
                joining_model = key.split("-")[-1]

                magnitude = self.cr.connection_magnitudes.get(key)
                key1 = self.__key_formatter(cluster_id_i, joining_model)
                key2 = self.__key_formatter(joining_model, cluster_id_j)
                key2 = self.__key_formatter(key2.split("_")[1], key2.split("_")[0])

                return key1, key2, magnitude
        else:
            return None, None, None

    def __get_structure_connection_point(self, structure, key):
        """
        Get the connection point of a structure given its filename and key.
        """

        return self.structures_connection_points.get(Path(structure.filename).stem).get(
            key
        )

    def __key_formatter(self, half1, half2):
        return "{}_{}".format(half1, half2)

    def keep_structures(self, distances, possible_ids):
        """Returns a list of structure IDs to keep based on a dictionary of
        distances and a list of possible IDs.

        Args:
            distances (dict): A dictionary of distances between points.
            possible_ids (list): A list of possible IDs to keep.

        Returns:
            list: A list of structure IDs to keep.
        """
        to_keep = []

        for key, value in distances.items():
            point1, point2 = key.split("_")
            point1, point2 = int(point1), int(point2)
            if value[1] not in to_keep and point1 in possible_ids:
                to_keep.append(value[1])
            if value[2] not in to_keep and point2 in possible_ids:
                to_keep.append(value[2])

            if point1 in possible_ids:
                possible_ids.remove(point1)
            if point2 in possible_ids:
                possible_ids.remove(point2)
            if len(possible_ids) == 0:
                break
        return to_keep

    def grouping(self):
        """Function to run the grouping algorithm using the shortest distances
        between structures. The output can be accessed using the joining_models
        attribute.

        Returns:
            None
        """
        for i in range(self.n):
            possible_ids = [x for x in self.cluster_id_structures.keys()]
            possible_ids.sort()

            distances = self.shortest_distances()
            if not distances:
                break
            distances = {
                k: v for k, v in sorted(distances.items(), key=lambda item: item[1][0])
            }
            to_keep = self.keep_structures(distances, possible_ids)
            new_cluster_id_structures = {}
            for cluster_id, structures in self.cluster_id_structures.items():
                for structure in structures:
                    if Path(structure.filename).stem not in to_keep:
                        new_cluster_id_structures.setdefault(cluster_id, [])
                        new_cluster_id_structures[cluster_id].append(structure)
                    else:
                        if self.joining_models.get(i + 1) is None:
                            self.joining_models[i + 1] = []
                        self.joining_models[i + 1].append(structure.filepathway)

            self.cluster_id_structures = new_cluster_id_structures


class ConnectionPointCalculator:
    """Takes an instance of ChainRestriction and calculates the
    connection points between the structures."""

    def __init__(self, cr) -> None:
        self.cr = cr

    def trilaterate_new_connection_point(self, points, magnitudes):
        """
        Returns the coordinates of a new connection point given the coordinates of
        the open residues and their magnitudes.

        Args:
            points (np.array): An array of points.
            magnitudes (np.array): An array of magnitudes.

        Returns:
            np.array: The coordinates of the new connection point.

        """

        def residuals(x):
            return np.linalg.norm(points - x, axis=1) - magnitudes

        x0 = np.array(points[0])
        result = least_squares(residuals, x0)

        return result

    @staticmethod
    def vector_from_open_residue(openresidue, cluster_id):
        """Returns the residue number and vector of an open residue given its cluster ID.

        Args:
            openresidue (OpenResidue): An instance of OpenResidue.
            cluster_id (int): The cluster ID of the open residue.

        Returns:
            int, np.array: The residue number and vector of the open residue.

        """
        if cluster_id == openresidue.cluster_id:
            res_number = openresidue.residue_number
            vector = openresidue.vector
        elif cluster_id == openresidue.connected_model:
            res_number = openresidue.connector
            vector = openresidue.connected_residue_to_connection_point()
        else:
            return None, None
        return res_number, vector

    def structures_connection_points(self, cluster_id_structures, structure_magnitudes):
        """
        Calculates the connection points between structures in a cluster.

        Args:
            cluster_id_structures (dict): A dictionary containing cluster IDs as keys
                and a list of structures as values.
            structure_magnitudes (dict): A dictionary containing structure IDs as keys
                and a dictionary of residue magnitudes as values.

        Returns:
            dict: A dictionary containing structure IDs as keys and a dictionary of
                connection points as values.
        """
        structures_connection_points = {}
        for cluster_id, structures in cluster_id_structures.items():

            for structure in structures:
                for key, res_magnitudes in structure_magnitudes.items():
                    if not key.startswith(str(cluster_id)):
                        continue
                    coordinates = [
                        structure.get_coordinate_of_residue(x)
                        for x in res_magnitudes.keys()
                    ]
                    magnitudes = list(res_magnitudes.values())

                    coordinates += [structure.centre_of_mass]
                    magnitudes += [
                        self.cr.magnitude_of_vector(
                            self.cr.centre_of_mass_to_connection[key]
                        )
                    ]
                    result = self.trilaterate_new_connection_point(
                        np.array(coordinates), np.array(magnitudes)
                    )

                    structures_connection_points.setdefault(
                        Path(structure.filename).stem, {}
                    )
                    structures_connection_points[Path(structure.filename).stem][
                        key
                    ] = result.x
        return structures_connection_points

    def structure_magnitudes(self):
        """Calculates the magnitudes of the vectors between open residues in each cluster pair.

        Returns:
            dict:
            A dictionary containing the magnitudes of the vectors
            between open residues in each cluster pair.
            The keys of the dictionary are formatted as "<cluster_id_i>
            -<cluster_id_j>" and "<cluster_id_j>-<cluster_id_i>".
            The values of the dictionary are dictionaries themselves,
            where the keys are the residue numbers and the values are the
            magnitudes of the vectors.
        """
        structure_magnitudes = {}
        for cluster_id_pair, open_residues in self.cr.cluster_id_openresidues.items():
            cluster_id_i, cluster_id_j = cluster_id_pair.split("_")
            cluster_id_i, cluster_id_j = int(cluster_id_i), int(cluster_id_j)
            for open_residue in open_residues:
                res_number1, vector1 = self.vector_from_open_residue(
                    open_residue, cluster_id_i
                )
                res_number2, vector2 = self.vector_from_open_residue(
                    open_residue, cluster_id_j
                )
                key1, key2 = (
                    self.__key_formatter(cluster_id_i, cluster_id_j),
                    self.__key_formatter(cluster_id_j, cluster_id_i),
                )

                structure_magnitudes.setdefault(key1, {})
                structure_magnitudes.setdefault(key2, {})
                if vector1 is None or vector2 is None:
                    continue
                magnitude1 = self.cr.magnitude_of_vector(vector1)
                magnitude2 = self.cr.magnitude_of_vector(vector2)

                structure_magnitudes[key1][res_number1] = magnitude1
                structure_magnitudes[key2][res_number2] = magnitude2

        return structure_magnitudes

    def __key_formatter(self, half1, half2):
        """Returns a key formatted as "<half1>_<half2>"."""
        return "{}_{}".format(half1, half2)
