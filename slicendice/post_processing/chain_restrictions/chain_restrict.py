__author__ = "Luc Elliott"
__date__ = "06 Oct 2023"
__version__ = "1.0"


import json
import logging
import numpy as np
from pathlib import Path
import re
from struvolpy.Structure import Structure
from typing import Optional, Dict


logger = logging.getLogger(__name__)

DISTANCE_COEFFICIENT = 3.8  # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3892923/


class ExtractSliceInfo:
    """
    A class for extracting information from the slice JSON file.

    Args:
        slice_json (str): The path to the slice JSON file.
        split_key (str, optional): The key to split the slice on. Defaults to None.
    """

    def __init__(self, slice_json, **kwargs) -> None:
        with open(slice_json, "r") as f:
            data = json.load(f)
            self.slice_data = data.get("slice", False)

        self.split_key = kwargs.get("split_key", None)

    def cluster_id(self, structure_filename) -> Optional[int]:
        """Returns the cluster ID of a given structure filename.

        Args:
            structure_filename (str or Structure): The path to the structure file.

        Returns:
            int: The cluster ID of the structure.
        """

        if isinstance(structure_filename, Structure):
            structure_filename = structure_filename.filename

        cluster_string = re.compile(r"cluster_\d")
        cluster_number = cluster_string.findall(Path(structure_filename).name)
        if cluster_number:
            return int(cluster_number[-1].split("_")[-1])
        else:
            return None

    def to_dict(self) -> dict:
        """Returns the slice residue ranges as a dictionary."""
        return {
            k: v
            for (k, v) in self.slice_data[self.split_key]["residues_ranges"].items()
            if k != "-1"
        }


class OpenResidue:
    def __init__(self, residue_number, cluster_id) -> None:
        """
        A class for storing information about an exposed residue.

        Args:
            residue_number (int): The residue number of the exposed residue.
            cluster_id (int): The cluster ID of the exposed residue.


        """
        self.residue_number = residue_number
        self.cluster_id = int(cluster_id)
        self.__connector = None
        self.__connected_model = None
        self.residue_difference = -1
        self.__placed = False
        self.__coordinate = None
        self.__vector = None
        self.__vector_residue = None

    def __repr__(self) -> str:
        """Returns a string representation of the OpenResidue object."""

        return f"""
OpenResidue(
    Residue number:    {self.residue_number:4},
    Cluster:           {self.cluster_id:4},
    Connected Residue: {self.__connector:4},
    Connected Cluster: {self.connected_model:4},
    Vector to connection point:  {self.__vector}
    Vector to connected residue: {self.__vector_residue}
)
                   """

    @property
    def connector(self):
        return self.__connector

    @connector.setter
    def connector(self, openresidue):
        if isinstance(openresidue, int):
            self.__connector = openresidue
            self.residue_difference = abs(self.residue_number - openresidue)

        elif isinstance(openresidue, OpenResidue):
            self.__connector = openresidue.residue_number
            openresidue.connector = self.residue_number

    @property
    def connected_model(self):
        """Returns the connector attribute of the current residue."""
        return self.__connected_model

    @connected_model.setter
    def connected_model(self, connected_model_or_int):
        """
        Sets the connector attribute of the current residue.

        Args:
            openresidue (int or OpenResidue): The residue number of the connector
                residue or an OpenResidue object.
        """
        if isinstance(connected_model_or_int, int):
            connected_model_int = connected_model_or_int
            self.__connected_model = connected_model_int

        elif isinstance(connected_model_or_int, OpenResidue):
            connected_model_or = connected_model_or_int
            self.__connected_model = connected_model_or.cluster_id
            connected_model_or.connected_model = self.cluster_id

    @property
    def placed(self):
        """Getting: Returns the placed attribute (booleon) of the current residue."""
        return self.__placed

    @placed.setter
    def placed(self, placed):
        """Setting: Sets the placed attribute of the current residue.

        Args:
            placed (bool): The value to set the placed attribute to.

        Raises:
            TypeError: If the placed attribute is not a boolean.
        """
        if not isinstance(placed, bool):
            raise TypeError("placed must be a boolean")
        self.__placed = placed

    @property
    def coordinate(self):
        """Getting: Returns the coordinate attribute of the current residue."""
        return self.__coordinate

    @coordinate.setter
    def coordinate(self, coordinate):
        """Setting: Sets the coordinate attribute of the current residue.

        Args:
            coordinate (np.array): The value to set the coordinate attribute to.

        Raises:
            TypeError: If the coordinate attribute is not a numpy array."""
        if not isinstance(coordinate, np.ndarray):
            raise TypeError("coordinate must be a numpy array")
        self.__coordinate = coordinate

    @property
    def vector(self):
        """Getting: Returns the vector attribute of the current residue."""
        return self.__vector

    @vector.setter
    def vector(self, vector):
        """Setting: Sets the vector attribute of the current between it and the
        connection point.

        Args:
            vector (np.array): The value to set the vector attribute to.

        Raises:
            TypeError: If the vector attribute is not a numpy array.
        """
        if not isinstance(vector, np.ndarray):
            raise TypeError("vector must be a numpy array")
        self.__vector = vector

    @property
    def vector_residue(self):
        """Getting: Returns the vector_residue attribute of the current residue."""
        return self.__vector_residue

    @vector_residue.setter
    def vector_residue(self, openresidue):
        """Setting: Sets the vector_residue attribute of the current residue.

        Args:
            openresidue (OpenResidue): The other exposed residue to calculate
                the vector to.

        Raises:
            TypeError: If the vector_residue attribute is not an OpenResidue object.
            ValueError: If either the current residue or the other residue do not
                have coordinates.

        """
        if not isinstance(openresidue, OpenResidue):
            msg = "vector_residue must be an OpenResidue object"
            raise TypeError(msg)
        if openresidue.coordinate is None or self.coordinate is None:
            msg = "Must have coordinates to calculate vector"
            raise ValueError(msg)
        self.__vector_residue = openresidue.coordinate - self.coordinate

    def connection_to_string(self):
        """
        Returns a string representing the connection between the current residue and
        the connector residue.

        Returns:
            str: A string representing the connection between the current residue and
                the connector residue.
        """
        if self.cluster_id is None or self.connected_model is None:
            return
        number1, number2 = sorted([self.cluster_id, self.connected_model])

        return f"{number1}_{number2}"

    def connected_residue_to_connection_point(self):
        """
        Returns the vector from the connected residue to the connection point.

        Returns:
            np.array: The vector from the connected residue to the connection point.
        """

        return self.vector - self.vector_residue


class ChainRestriction:
    """A class for generating chain restrictions from a slice JSON file.

    Args:
        input_json (str): The path to the slice JSON file.
        split_key (str, optional): The key to split the slice on. Defaults to None.

    """

    def __init__(self, input_json, split_key) -> None:
        self.cluster_id_openresidues: dict = {}
        self.structure_cache: dict = {}

        self.input_json = input_json
        self.split_key = split_key
        self.esi = ExtractSliceInfo(self.input_json, split_key=self.split_key)

        self.generate_open_residues()
        self.find_connection_points()
        self.add_vectors_to_open_residues()
        self.calculate_connection_point_magnitudes()
        self.vectors_to_centre_of_mass()

    def generate_open_residues(self):
        """
        Generates OpenResidue objects for each exposed residue in the slice JSON file.

        Returns:
            dict: A dictionary containing the OpenResidue objects for each cluster.
        """
        d = self.esi.to_dict()

        # Reorder the dictionary keys based on residue range
        re_ordered_dict = {
            "_".join([residue.split(":")[-1] for residue in residue_range]): cluster_id
            for cluster_id, residue_ranges in d.items()
            for residue_range in residue_ranges
        }

        sorted_keys = sorted(re_ordered_dict.keys(), key=lambda x: int(x.split("_")[0]))
        re_ordered_dict = {key: re_ordered_dict[key] for key in sorted_keys}

        cluster_id_openresidues = {}
        # Iterate through the sorted dictionary keys
        keys = list(re_ordered_dict.keys())

        for i, key in enumerate(keys):
            if i == 0:
                continue
            # Create OpenResidue objects for the start and end residues
            end_residue = OpenResidue(
                int(keys[i - 1].split("_")[-1]), re_ordered_dict[keys[i - 1]]
            )

            start_residue = OpenResidue(int(key.split("_")[0]), re_ordered_dict[key])

            # Connect the start and end residues
            start_residue.connector = end_residue
            start_residue.connected_model = end_residue

            if start_residue.cluster_id == end_residue.cluster_id:
                continue

            # add the coordinates to the open residues
            if self.structure_cache.get(start_residue.cluster_id) is None:
                self.structure_cache[start_residue.cluster_id] = Structure.from_file(
                    self.esi.slice_data[self.split_key]["models"][
                        start_residue.cluster_id
                    ]
                )
            if self.structure_cache.get(end_residue.cluster_id) is None:
                self.structure_cache[end_residue.cluster_id] = Structure.from_file(
                    self.esi.slice_data[self.split_key]["models"][
                        end_residue.cluster_id
                    ]
                )

            start_residue.coordinate = self.coordinate_of_placed_residue(
                self.structure_cache[start_residue.cluster_id],
                start_residue.residue_number,
            )

            end_residue.coordinate = self.coordinate_of_placed_residue(
                self.structure_cache[end_residue.cluster_id],
                end_residue.residue_number,
            )
            start_residue.vector_residue = end_residue
            end_residue.vector_residue = start_residue

            cluster_id_openresidues = self.add_open_residue(
                cluster_id_openresidues, start_residue
            )

        # strip the 2 level dictionary to a list of open residues

        cluster_id_openresidues = {
            cluster_id: list(openresidues.values())
            for cluster_id, openresidues in cluster_id_openresidues.items()
        }

        cluster_id_openresidues = self.remove_outliers(cluster_id_openresidues)

        self.cluster_id_openresidues = cluster_id_openresidues

    @staticmethod
    def add_open_residue(cluster_id_openresidues, residue):
        """
        Adds an OpenResidue object to the cluster_id_openresidues dictionary.

        Args:
            cluster_id_openresidues (dict): A dictionary containing the OpenResidue
                objects for each cluster.
            residue (OpenResidue): The OpenResidue object to add to the dictionary.

        Returns:
            dict: A dictionary containing the OpenResidue objects for each cluster.

        """
        cluster_id_openresidues.setdefault(residue.connection_to_string(), {})
        cluster_id_openresidues[residue.connection_to_string()][
            residue.residue_number
        ] = residue
        return cluster_id_openresidues

    def coordinate_of_placed_residue(self, structure: Structure, residue_number: int):
        """
        Returns the coordinate of a residue in a structure.

        Args:
            structure (Structure): The structure to search for the residue in.
            residue_number (int): The residue number of the residue to find.

        Returns:
            np.array: The coordinate of the residue.

        """
        return structure.get_coordinate_of_residue(residue_number)

    def find_connection_points(self) -> None:
        """
        Calculates the average coordinates of each cluster,
        these are the connection points.

        The connection points are stored in the connection_point_centres attribute.
        """

        connectors_coordinates: Dict[str, list] = {}
        for open_residues in self.cluster_id_openresidues.values():
            for open_residue in open_residues:
                key = open_residue.connection_to_string()
                if connectors_coordinates.get(key) is None:
                    connectors_coordinates[key] = []
                connectors_coordinates[key].append(open_residue.coordinate)

        self.connection_point_centres = {
            k: np.mean(coordinates, axis=0)
            for k, coordinates in connectors_coordinates.items()
        }

    def iqr(self, vectors):
        """
        Calculates the interquartile range of a list of vectors.

        Args:
            vectors (list): A list of vectors.

        Returns:
            list: A list of booleans representing whether
                each vector is an outlier or not.
        """
        vectors = np.array(vectors)
        q1, q3 = np.percentile(vectors, [25, 75], axis=0)
        iqr = q3 - q1
        lower_bound = q1 - (1.5 * iqr)
        upper_bound = q3 + (1.5 * iqr)

        filtered_vectors = [
            np.all(lower_bound <= v) and np.all(v <= upper_bound) for v in vectors
        ]

        return filtered_vectors

    def remove_outliers(self, cluster_id_openresidues):
        """
        Removes outliers from the cluster_id_openresidues dictionary using
        interquartule differences.

        Args:
            cluster_id_openresidues (dict): A dictionary containing the
                OpenResidue objects for each cluster.

        Returns:
            dict: A dictionary containing the OpenResidue objects for each
                cluster with outliers removed.
        """
        connecting_models_open_residues = {}

        for cluster_id, open_residues in cluster_id_openresidues.items():
            for open_residue in open_residues:
                if open_residue.residue_number:
                    vector_key = f"{cluster_id}_{open_residue.connected_model}"
                    if connecting_models_open_residues.get(vector_key) is None:
                        connecting_models_open_residues[vector_key] = []
                    connecting_models_open_residues[vector_key].append(open_residue)

        vectors = []
        [
            vectors.extend([op.vector_residue for op in ops])
            for ops in connecting_models_open_residues.values()
        ]
        # find outliers
        if len(vectors) == 0:
            return cluster_id_openresidues
        vectorsiqr = iter(self.iqr(vectors))

        # use booleans to index the dictionary containing the connectors
        connecting_models_open_residues = {
            k: [v for v in vectors if next(vectorsiqr)]
            for k, vectors in cluster_id_openresidues.items()
        }

        return connecting_models_open_residues

    def add_vectors_to_open_residues(self):
        """
        Adds the average coordinates of each cluster to the OpenResidue objects.

        Args:
            average_coordinates (list): A list of average coordinates for each cluster.
        """
        # turn the keys of average_coordinates into a generator
        for open_residues in self.cluster_id_openresidues.values():
            for open_residue in open_residues:
                open_residue.vector = (
                    self.connection_point_centres[open_residue.connection_to_string()]
                    - open_residue.coordinate
                )

    def calculate_connection_point_magnitudes(self):
        """
        Calculates the magnitudes of the vectors between each connection
        point if they have one intermedite model.

        The magnitudes are stored in the connection_magnitudes attribute.

        """
        self.connection_magnitudes = {}
        for connection_i, center_i in self.connection_point_centres.items():
            for connection_j, center_j in self.connection_point_centres.items():
                if connection_i == connection_j:
                    continue

                point1, point2 = connection_i.split("_")
                point3, point4 = connection_j.split("_")
                points = [point1, point2, point3, point4]
                uniques = set([point1, point2, point3, point4])
                # if the connection points have more than one model in between them
                # then we don't want to consider them a valid connection
                if len(uniques) != 3:
                    continue

                # put repeating point at end of list
                joining_point = [p for p in points if points.count(p) > 1][0]
                points = [p for p in points if p != joining_point]

                pointi, pointj = sorted(points)

                center_point_name = pointi + "_" + pointj + "-" + joining_point
                if self.connection_magnitudes.get(center_point_name) is None:
                    self.connection_magnitudes[center_point_name] = np.linalg.norm(
                        center_i - center_j
                    )

    def vectors_to_centre_of_mass(self):
        """
        Calculates the vectors from the centre of mass of each model
        to the connection point.

        The vectors are stored in the centre_of_mass_to_connection attribute.
        """
        self.centre_of_mass_to_connection = {}
        centres_of_mass = {
            cluster_id: structure.centre_of_mass
            for cluster_id, structure in self.structure_cache.items()
        }
        for _, open_residues in self.cluster_id_openresidues.items():
            for open_residue in open_residues:
                id1, id2 = open_residue.cluster_id, open_residue.connected_model
                key1, key2 = f"{id1}_{id2}", f"{id2}_{id1}"
                if (
                    self.centre_of_mass_to_connection.get(key1) is not None
                    and self.centre_of_mass_to_connection.get(key2) is not None
                ):
                    continue

                # find vector from or coordinate to connection point
                id1_vector = open_residue.vector
                # get the centre of mass of the model
                com1 = centres_of_mass[id1]
                com2 = centres_of_mass[id2]
                # calculaye the vector from the centre of mass to the connection point
                model1_coor = open_residue.coordinate
                com1_to_model1 = model1_coor - com1
                com1_to_connection = com1_to_model1 + id1_vector
                # calculate the vector from the centre of mass to the other connection
                # point
                com1_to_com2 = com2 - com1
                com2_to_connection = com1_to_connection - com1_to_com2
                self.centre_of_mass_to_connection[f"{id1}_{id2}"] = com1_to_connection
                self.centre_of_mass_to_connection[f"{id2}_{id1}"] = com2_to_connection

    @staticmethod
    def magnitude_of_vector(vector):
        """
        Calculates the magnitude of a vector.

        Args:
            vector (np.array): A vector.

        Returns:
            float: The magnitude of the vector.
        """
        return np.linalg.norm(vector)
