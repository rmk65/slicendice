__author__ = "Luc Elliott"
__date__ = "06 Oct 2023"
__version__ = "1.0"


from slicendice.post_processing.chain_restrictions import (
    ChainRestriction,
    ExtractSliceInfo,
)
from slicendice.post_processing.output_grouping import (
    GroupOutputs,
    ConnectionPointCalculator,
)
from struvolpy.Structure import Structure


class PostProcessGrouping:
    """
    Class linking the post processing classes together to group the docked models
    """

    def __init__(self, *args, **kwargs):
        """
        Args are the docked models
        """

        self.models = args

        self.input_json = kwargs.get("input_json", None)
        self.split_key = kwargs.get("split_key", None)
        self.no_mols = kwargs.get("no_mols", None)

        # These are all required so add checks
        if self.input_json is None:
            msg = "No input json provided"
            raise ValueError(msg)
        if self.split_key is None:
            msg = "No split key provided"
            raise ValueError(msg)
        self.ei = ExtractSliceInfo(self.input_json, split_key=self.split_key)
        self.cr = ChainRestriction(self.input_json, self.split_key)
        self.cpc = ConnectionPointCalculator(self.cr)

        self.run()

    def run(self):
        cluster_id_structures = {}
        for structure in self.models:
            structure = Structure.from_file(structure)
            cluster_id = self.ei.cluster_id(structure)
            if cluster_id_structures.get(cluster_id) is None:
                cluster_id_structures[self.ei.cluster_id(structure)] = [structure]
            else:
                cluster_id_structures[self.ei.cluster_id(structure)].append(structure)

        cluster_id_structures = {
            k: v
            for k, v in sorted(
                cluster_id_structures.items(),
                reverse=True,
                key=lambda item: len(item[1]),
            )
        }

        # Calculate the the magnitudes the open residues have have to their repecitve
        # conneciton points
        structure_magnitudes = self.cpc.structure_magnitudes()

        # Calculates the connection points for each structure in their docked position
        structures_connection_points = self.cpc.structures_connection_points(
            cluster_id_structures, structure_magnitudes
        )

        self.go = GroupOutputs(
            self.cr, cluster_id_structures, structures_connection_points, self.no_mols
        )

        self.go.grouping()

    @property
    def groups(self):
        return self.go.joining_models
