from emda.core import fsc, restools
import logging
import numpy as np
from pandas import DataFrame
from pathlib import Path
from struvolpy import Volume, Structure
from struvolpy.Volume import SimulatedMap
import sys
from TEMPy.protein.scoring_functions import FastSMOC, ScoringFunctions
from typing import Union, Dict


logger = logging.getLogger(__name__)

VARIABLE_ORDER = [
    "CCC",
    "FSC_avg",
    "Local_MI",
    "MI",
    "Overlap_Map",
    "Overlap_Model",
    "Smoc_avg",
]


class Fittingscores(object):
    def __init__(self, mapin: Union[Path, Volume], resolution: float, threshold: float):
        self.__volumeobj = self.__mapchecker(mapin)
        self.__volumeobj.resolution = resolution
        self.__resolution = resolution
        if threshold != -1.0:
            self.__volumeobj.threshold = threshold
        else:
            logger.info("\nNo threshold was given, calculating threshold automatically")
            self.__volumeobj.calculate_threshold()

        logger.info(f"\nThreshold for the input map is {self.__volumeobj.threshold}")

        if np.sum(self.__volumeobj.grid > self.__volumeobj.threshold) == 0:
            msg = (
                "The amount of voxels above the threshold "
                "for the input volume is 0, please lower the threshold"
            )
            raise ValueError(msg)

        self.__bfac = False
        self.__model: list = []
        self.__sf = ScoringFunctions()
        self.__scores: dict = {}

    @property
    def volume(self):
        return self.__volumeobj

    @volume.setter
    def volume(self, new_volumeobj: Volume):
        self.__volumeobj = self.__mapchecker(new_volumeobj)

    @property
    def bfac(self):
        return self.__bfac

    @bfac.setter
    def bfac(self, bfac):
        self.__bfac = bfac

    @property
    def model(self):
        return self.__model

    @model.setter
    def model(self, new_model: Union[Path, str]):
        # Add check to see if there are more than one chain
        filename = str(new_model)

        structure = Structure.from_file(filename)

        structure.filename = filename
        simulated_map = self.simulatemap(structure)
        simulated_map.calculate_threshold(simulated=True)

        self.__model = structure
        self.__simulated_map = simulated_map

    @property
    def scores(self):
        return str(self.__scores)

    @staticmethod
    def __mapchecker(new_volumeobj: Union[str, Path, Volume]) -> Volume:
        if isinstance(new_volumeobj, (Path, str)):
            vol = Volume.from_file(new_volumeobj)
            vol.grid = pad_to_even_dimensions(vol.grid)
            return vol

        elif isinstance(new_volumeobj, Volume):
            vol = new_volumeobj
            vol.grid = pad_to_even_dimensions(vol.grid)
            return vol

        msg = (
            f"Volume must be a file pathway or Volume object, not {type(new_volumeobj)}"
        )

        ValueError(msg)

    def simulatemap(self, model: Structure) -> Volume:
        simulated_volume = SimulatedMap.simulate_map(
            self.__volumeobj,
            model,
            self.__resolution,
            normalise=True,
        )

        simulated_volume.resolution = self.__resolution
        simulated_volume.filename = (
            f"{Path(self.__volumeobj.filename).stem}_simulated.mrc"
        )
        return simulated_volume

    def overlap(self, simulated_map: Volume, scores_dict: dict = {}):
        # assumption that maps will be simulated before hand
        # Also taken from TEMPy SF class but it doesn't work there

        mask_model = simulated_map.grid > float(simulated_map.threshold)
        mask_target = self.__volumeobj.grid > float(self.__volumeobj.threshold)

        mask_array = (mask_target * mask_model) > 0
        map_size = np.sum(mask_target)
        mask_size = np.sum(mask_model)

        overlapmap = float(np.sum(mask_array)) / map_size
        overlapmodel = float(np.sum(mask_array)) / mask_size

        overlaps = {"Overlap_Map": overlapmap, "Overlap_Model": overlapmodel}

        if not scores_dict:
            scores_dict.update(overlaps)

        return overlapmap, overlapmodel

    def __cross_correlation_coeff(self, simulated_map: Volume, scores_dict: dict = {}):
        ccc, _ = self.__sf.CCC_map(
            self.__volumeobj.TEMPy_map,
            simulated_map.TEMPy_map,
            map_target_threshold=self.__volumeobj.threshold,
            map_probe_threshold=simulated_map.threshold,
            mode=2,
            meanDist=True,
            cmode=False,
        )

        if scores_dict:
            scores_dict.update({"CCC": ccc})

        return ccc

    def __local_ccc(self, simulated_map: Volume, scores_dict: dict = {}):
        loc_ccc, _ = self.__sf.CCC_map(
            self.__volumeobj.TEMPy_map,
            simulated_map.TEMPy_map,
            map_target_threshold=self.__volumeobj.threshold,
            map_probe_threshold=simulated_map.threshold,
            mode=3,
            meanDist=True,
            cmode=False,
        )

        if scores_dict:
            scores_dict.update({"Local_CCC": loc_ccc})

        return loc_ccc

    def __mutual_information(self, simulated_map: Volume, scores_dict: dict = {}):
        mi = self.__sf.MI(
            self.__volumeobj.TEMPy_map,
            simulated_map.TEMPy_map,
            map_target_threshold=self.__volumeobj.threshold,
            map_probe_threshold=simulated_map.threshold,
            mode=1,
            cmode=False,
        )
        if scores_dict:
            scores_dict.update({"MI": mi})

        return mi

    def __local_MI(self, simulated_map: Volume, scores_dict: dict = {}):
        loc_mi = self.__sf.MI(
            self.__volumeobj.TEMPy_map,
            simulated_map.TEMPy_map,
            map_target_threshold=self.__volumeobj.threshold,
            map_probe_threshold=simulated_map.threshold,
            mode=3,
            cmode=False,
        )

        if scores_dict:
            scores_dict.update({"Local_MI": loc_mi})

        return loc_mi

    def __fsc(self, simulated_map, scores_dict: dict = {}):
        cell = np.array([dim for dim in self.__volumeobj.shape] + [90.0, 90.0, 90.0])

        nbin, _, bin_idx = restools.get_resolution_array(
            uc=cell, hf1=self.__volumeobj.grid
        )
        f1 = np.fft.fftshift(np.fft.fftn(self.__volumeobj.grid))
        f2 = np.fft.fftshift(np.fft.fftn(simulated_map.grid))

        bin_fsc = fsc.anytwomaps_fsc_covariance(
            f1=f1,
            f2=f2,
            bin_idx=bin_idx,
            nbin=nbin,
        )[0]

        fsc_avg = np.mean(bin_fsc)
        if scores_dict:
            scores_dict.update({"FSC_avg": fsc_avg})

        return fsc_avg

    def __smoc(
        self,
        simulated_map,
        protein_structure,
        sliding_window=11,
    ):
        fs = FastSMOC(
            protein_structure.to_TEMPy(),
            self.__volumeobj.TEMPy_map,
            self.__volumeobj.resolution,
            sim_map=simulated_map.TEMPy_map,
        )

        chains = set(chain.name for chain in protein_structure.chains)

        chain_scores = {}
        for chain in chains:
            smoc_scores_chain = fs.score_chain_contig(chain, sliding_window)

            chain_scores[chain] = smoc_scores_chain

        return chain_scores

    def __snd_classifier_scores(
        self,
        *args,
    ) -> None:
        (volume, structure) = args[0], args[1]
        scores_dict: Dict[str, Union[float, int]] = {}
        self.overlap(volume, scores_dict=scores_dict)

        if scores_dict["Overlap_Model"] < 0.02:
            logger.debug(
                "The Model doesn't overlap with the map. "
                "No more scores will be calculated."
            )
            self.__scores = {structure.filename: None}

        # Calculate the scores
        self.__cross_correlation_coeff(volume, scores_dict=scores_dict)
        self.__mutual_information(volume, scores_dict=scores_dict)
        self.__local_MI(volume, scores_dict=scores_dict)
        self.__fsc(volume, scores_dict=scores_dict)

        # smoc average over all chains

        smocdict = self.__smoc(volume, structure)
        smoc = _chain_scores_average(smocdict)

        # Adding the last score and resolution
        scores_dict = {**scores_dict, **{"Smoc_avg": smoc}}

        self.__scores = {structure.filename: scores_dict}

    def run(self):
        self.__snd_classifier_scores(self.__simulated_map, self.__model)

    def to_Dataframe(self) -> Union[DataFrame, None]:
        if self.__scores is None:
            return None

        scores_df = DataFrame.from_dict(self.__scores).T
        scores_df = scores_df[VARIABLE_ORDER]
        return scores_df

    def to_csv(self, filename) -> None:
        if self.__scores is None:
            return None

        scores_df = DataFrame.from_dict(self.__scores).T
        scores_df = scores_df[VARIABLE_ORDER]
        if not Path(filename).exists():
            mode = "w"
        else:
            mode = "a"

        scores_df.to_csv(
            filename,
            mode=mode,
            header=True,
            index=True,
        )


def _chain_scores_average(chain_score_dict: dict, chain_label: str = "") -> float:
    if chain_label:
        chain_score_dict = {chain_label: chain_score_dict[chain_label]}

    return np.mean(
        [score for (_, scores) in chain_score_dict.items() for score in scores.values()]
    )


def pad_to_even_dimensions(array):
    nx, ny, nz = array.shape
    if nx % 2 != 0:
        array = np.pad(array, ((0, 1), (0, 0), (0, 0)), mode="constant")
    if ny % 2 != 0:
        array = np.pad(array, ((0, 0), (0, 1), (0, 0)), mode="constant")
    if nz % 2 != 0:
        array = np.pad(array, ((0, 0), (0, 0), (0, 1)), mode="constant")
    return array


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Fitting scores preperation for the SnD Classifier",
        prefix_chars="-",
    )

    core = parser.add_argument_group("Required")
    core.add_argument("-mapin", type=str, help="Path to the input map file")
    core.add_argument("-xyzin", type=str, help="Path to the input pdb file")
    core.add_argument(
        "-resolution", type=float, help="Resolution of the input map file"
    )
    optional = parser.add_argument_group("Optional")
    optional.add_argument(
        "-outfile", type=str, default="scores.csv", help="Path to the output csv file"
    )
    optional.add_argument(
        "-threshold", type=float, default=-1.0, help="Threshold for the input map file"
    )

    args = parser.parse_args()

    fs = Fittingscores(
        args.mapin,
        args.resolution,
        args.threshold,
    )

    fs.model = Path(args.xyzin)

    try:
        # Run the fitting scores
        fs.run()
        fs.to_csv(args.outfile)
    except KeyboardInterrupt:
        sys.stderr.write("Interrupted by keyboard!")
        exit(1)
