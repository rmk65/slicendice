from numpy import array
import os
import pandas as pd
from pathlib import Path
from pickle import load
from tabulate import tabulate
import warnings
from scipy.special import expit
import numpy as np


warnings.filterwarnings("ignore")


class Classifier(object):
    """A class used to classify dice scores using a pre-trained classifier.

    Args:
        scores_df (pandas.DataFrame): A pandas DataFrame containing dice scores.
        logger (optional) : A logger object for logging messages. Defaults to None, .
        dev (optional): A boolean indicating whether to run in development mode.
            Defaults to False.
        classifier_threshold (optional): A float representing the threshold for
            classifying the scores. Defaults to 0.5.

    Attributes:
        logger: A logger object for logging messages.
        scores_df: A pandas DataFrame containing dice scores.
        dev: A boolean indicating whether to run in development mode.
        __col_length: An integer representing the number of columns in the
            scores DataFrame.
        scaler: A pre-trained scaler object for scaling the scores.
        classifier: A pre-trained classifier object for classifying the scores.

    Methods:
        run_prediction_by_line: Runs the prediction on each line of the scores
            DataFrame.
    """

    def __init__(self, scores_df: pd.DataFrame, **kwargs):
        dice_path = os.path.dirname(os.path.realpath(__file__))
        self.logger = kwargs.get("logger", None)
        if self.logger is None:
            import logging

            self.logger = logging.getLogger(__name__)
        self.scores_df = scores_df.fillna(0)

        self.dev = kwargs.get("dev", False)

        self.__col_length = len(scores_df.columns)
        self.classifier_threshold = kwargs.get("classifier_threshold", 0.5)
        self.scaler = load(
            open(
                str(Path(dice_path).joinpath("classifier").joinpath("sca.pkl")),
                "rb",
            )
        )

        self.classifier = load(
            open(
                str(
                    str(Path(dice_path).joinpath("classifier").joinpath("cla.sav")),
                ),
                "rb",
            )
        )

        self.run_prediction_by_line()

    def predict_proba(self, X):
        decision_scores = self.classifier.decision_function(X)
        probabilities = expit(decision_scores)
        return np.vstack([1 - probabilities, probabilities]).T

    def run_prediction_by_line(self):
        """Runs the prediction on each line of the scores DataFrame."""

        self.scores_df["class"] = 0
        class_col_numer = self.scores_df.columns.get_loc("class")

        self.scores_df["proba"] = 0
        proba_col_numer = self.scores_df.columns.get_loc("proba")

        for i in range(len(self.scores_df)):
            structure_index = self.scores_df.index[i]
            self.logger.info(
                "\n Map Model Scores for structure: " + Path(structure_index).name
            )
            scores_table = []
            for j in range(self.__col_length):
                column_name = self.scores_df.columns[j]
                score = "{:.4f}".format(self.scores_df.iloc[i, j])
                scores_table.append([column_name, score])

            self.logger.info(
                tabulate(
                    scores_table, headers=["Column Name", "Score"], tablefmt="orgtbl"
                )
            )
            if (
                self.scores_df.iloc[i, : self.__col_length]["CCC"] == -1.0
                or self.scores_df.iloc[i, : self.__col_length]["Overlap_Map"] < 0.002
            ):
                self.scores_df.iloc[i, class_col_numer] = 0

                self.scores_df.iloc[i, proba_col_numer] = -1.0
                continue

            scores = array(self.scores_df.iloc[i, : self.__col_length]).reshape(1, -1)
            scaled_scores = self.scaler.transform(scores)
            probabilty = self.predict_proba(scaled_scores)
            self.scores_df.iloc[i, proba_col_numer] = probabilty[0][1]
            self.scores_df.iloc[i, class_col_numer] = (
                1 if probabilty[0][1] > self.classifier_threshold else 0
            )
