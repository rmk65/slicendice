__author__ = "Adam Simpkin"
__date__ = "02 Feb 2022"
__version__ = "1.0"

from .submit_jobs_EM import EmSubmit, CMD_PREFIX  # noqa: F401
from .submit_jobs_MX import MrSubmit, MrSubmitHybrid  # noqa: F401
