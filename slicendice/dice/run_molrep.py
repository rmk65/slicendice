__author__ = "Adam Simpkin"
__date__ = "16 Feb 2022"
__version__ = "0.1"

import inspect
import operator
import os
import numpy as np
from pathlib import Path
from pyjob import cexec
from pyjob.script import EXE_EXT
from simbad.mr.options import SGAlternatives
from simbad.mr import molrep_mr
from simbad.parsers import mtz_parser
from simbad.util import mtz_util
import shutil
from struvolpy import Structure, Volume

m_obj = molrep_mr.Molrep("", "", "", "", "", "", "", "", "")
ALL_ALT_SG = m_obj.all_alt_sg
ALL_SG_CODES = m_obj.all_sg_codes
ENANT_SG = m_obj.enant_sg
ENANT_SG_CODES = m_obj.enant_sg_codes


class Molrep(object):
    """Class to run Molrep using a fixed model"""

    def __init__(self, xyzin: str, **kwargs):
        self.xyzin = Path(xyzin)
        self.xyz_fixed = kwargs.get("xyz_fixed", None)
        self.nmol = kwargs.get("nmol", 1)
        self.molrep_tf = kwargs.get("molrep_tf", "PTF")
        self.working_dir = kwargs.get("working_dir", Path.cwd())
        self.ignore_existing = kwargs.get("ignore_existing", False)
        self.run_dir = self.make_run_dir()
        self.logfile = kwargs.get("logfile", self.run_dir.joinpath("molrep.log"))
        self.xyzout = kwargs.get("xyzout", self.run_dir.joinpath("molrep.pdb"))
        self.mapin = kwargs.get("mapin", None)
        self.hklin = kwargs.get("hklin", None)
        self.hklout = kwargs.get("hklout", None)
        self.sga = kwargs.get("sga", None)

        # Check to see if either .map or .hkl file is inputted
        assert (not self.hklin or not self.mapin) and (
            self.hklin or self.mapin
        ), "Must provide an MTZ or Map input"

        if self.hklin:
            self.f_in = Path(self.hklin)
        elif self.mapin:
            self.f_in = Path(self.mapin)

        assert self.f_in.exists()
        assert self.xyzin.exists()

        if self.xyz_fixed is not None:
            self.xyz_fixed = Path(self.xyz_fixed)
            assert self.xyz_fixed.exists()

    def make_run_dir(self):
        run_dir = Path(self.working_dir).joinpath("molrep")
        if run_dir.exists() and not self.ignore_existing:
            raise RuntimeError(
                f"There is an existing work directory: {run_dir}\n "
                f"Please delete/move it aside."
            )
        elif not run_dir.exists():
            run_dir.mkdir()
        return run_dir

    def shift_output_pdb(self, output_pdb, map_file):
        pdb_struc = Structure.from_file(str(output_pdb))
        map_struc = Volume.from_file(str(map_file))
        pdb_struc.translate(np.array(map_struc.origin))
        pdb_struc.to_file(f"{output_pdb.stem[:-7]}-molrep-origin-shift.pdb")

    def run(self):
        current_work_dir = Path.cwd()
        os.chdir(self.run_dir)

        # Copy f_in and xyzin to working_dir for efficient running of Molrep
        f_in = self.run_dir.joinpath(self.f_in.name)
        f_in.write_bytes(self.f_in.read_bytes())

        # Changes the name after copying the file over
        if self.hklin is None and self.f_in.suffix == ".mrc":
            os.rename(f_in, f_in.with_suffix(".map"))
            f_in = f_in.with_suffix(".map")

        xyzin = self.run_dir.joinpath(self.xyzin.name)
        xyzin.write_text(self.xyzin.read_text())

        ptf_d = {"PTF": "N", "SAPTF": "S"}
        if self.molrep_tf is not None:
            keys = [f"NMON {self.nmol}\n", f"PRF {ptf_d[self.molrep_tf]}\n"]
        else:
            keys = [f"NMON {self.nmol}\n"]

        if self.hklin:
            self.molrep_mx(xyzin, f_in, keys, self.logfile)
        else:
            self.molrep(xyzin, f_in, keys, self.logfile)

            # Move output xyz to specified place
            molrep_out = Path("molrep.pdb")
            if molrep_out.exists():
                molrep_out.rename(self.xyzout)
            else:
                raise RuntimeError("No molrep solutions found")

        if self.mapin is not None:
            self.shift_output_pdb(Path(self.xyzout), Path(self.f_in))

        # Return to original working dir
        os.chdir(str(current_work_dir))

        # Delete any files copied across
        for file in [f_in, xyzin]:
            if file.exists():
                file.unlink()

    def molrep_mx(self, xyzin, f_in, keys, logfile):
        sg = "".join(mtz_parser.MtzParser(self.hklin).spacegroup_symbol.split())
        hklin_sg_code = ALL_SG_CODES[sg]
        self.molrep(
            xyzin,
            f_in,
            keys,
            os.path.join(self.run_dir, f"molrep_out_{hklin_sg_code}.log"),
        )

        # Move output xyz to specified place
        molrep_out = Path("molrep.pdb")
        if molrep_out.exists():
            molrep_out.rename(
                os.path.join(self.run_dir, f"molrep_out_{hklin_sg_code}.pdb")
            )
        else:
            msg = "No molrep solutions found"
            raise RuntimeError(msg)

        if self.sga == "enant" and sg in ENANT_SG_CODES:
            enant_sg_code = ENANT_SG[hklin_sg_code]
            contrast = molrep_mr.check_contrast(
                os.path.join(self.run_dir, f"molrep_out_{hklin_sg_code}.log")
            )
            contrasts = {sg: contrast}
            keys = [f"NMON {self.nmol}\n", f"NOSG {enant_sg_code}\n"]
            logfile = os.path.join(self.run_dir, f"molrep_out_{enant_sg_code}.log")

            self.molrep(xyzin, f_in, keys, logfile)
            contrasts[enant_sg_code] = molrep_mr.check_contrast(logfile)

            molrep_out = Path("molrep.pdb")
            if molrep_out.exists():
                molrep_out.rename(
                    os.path.join(self.run_dir, f"molrep_out_{enant_sg_code}.pdb")
                )
            self.evaluate_results(contrasts)
            return

        elif self.sga == "all" and sg in ALL_SG_CODES:
            all_alt_sg_codes = ALL_ALT_SG[hklin_sg_code]
            contrast = molrep_mr.check_contrast(
                os.path.join(self.run_dir, f"molrep_out_{hklin_sg_code}.log")
            )
            contrasts = {sg: contrast}
            for alt_sg_code in all_alt_sg_codes:
                keys = [f"NMON {self.nmol}\n", f"NOSG {alt_sg_code}\n"]
                logfile = os.path.join(
                    self.run_dir, "molrep_out_{0}.log".format(alt_sg_code)
                )
                self.molrep(xyzin, f_in, keys, logfile)
                contrasts[alt_sg_code] = molrep_mr.check_contrast(logfile)
                molrep_out = Path("molrep.pdb")
                if molrep_out.exists():
                    molrep_out.rename(
                        os.path.join(self.run_dir, f"molrep_out_{alt_sg_code}.pdb")
                    )
            self.evaluate_results(contrasts)
            return

        # Move output pdb to specified name
        molrep_out = Path(f"molrep_out_{hklin_sg_code}.pdb")
        if molrep_out.exists():
            molrep_out.rename(self.xyzout)
        # Move output hkl to specified name
        if f_in.exists():
            shutil.copy(self.hklin, self.hklout)

        # Move log file to specified name
        molrep_log = Path(f"molrep_out_{hklin_sg_code}.log")
        if molrep_log.exists():
            molrep_log.rename(self.logfile)

    def molrep(self, xyzin, f_in, keys, logfile):
        keyword_file = self.run_dir.joinpath("molrep_keywords.txt")
        with open(keyword_file, "w") as f:
            for key in keys:
                f.write(key)

        cmd = [
            "molrep" + EXE_EXT,
            "-m",
            str(xyzin.relative_to(self.run_dir)),
            "-f",
            str(f_in.relative_to(self.run_dir)),
            "-k",
            str(keyword_file.relative_to(self.run_dir)),
        ]

        if self.xyz_fixed is not None:
            xyz_fixed = self.run_dir.joinpath(self.xyz_fixed.name)
            xyz_fixed.write_text(self.xyz_fixed.read_text())
            [cmd.append(x) for x in ["-mx", str(xyz_fixed.relative_to(self.run_dir))]]

        stdout = cexec(cmd)
        with open(logfile, "w") as f_out:
            f_out.write(stdout)
        return

    def evaluate_results(self, results):
        """Function to evaluate molrep results and move the result with the
        best contrast score to the output pdb"""
        top_sg_code = max(results.items(), key=operator.itemgetter(1))[0]
        if os.path.isfile(
            os.path.join(self.run_dir, "molrep_out_{0}.pdb".format(top_sg_code))
        ):
            shutil.move(
                os.path.join(self.run_dir, "molrep_out_{0}.pdb".format(top_sg_code)),
                self.xyzout,
            )
        if os.path.isfile(
            os.path.join(self.run_dir, "molrep_out_{0}.log".format(top_sg_code))
        ):
            shutil.move(
                os.path.join(self.run_dir, "molrep_out_{0}.log".format(top_sg_code)),
                self.logfile,
            )

        mtz_util.reindex(self.hklin, self.hklout, top_sg_code)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Runs MR using MOLREP", prefix_chars="-"
    )

    core = parser.add_argument_group("Required")
    core.add_argument("-hklin", type=str, help="Path to the input hkl file")
    core.add_argument("-mapin", type=str, help="Path to the input map file")

    optional = parser.add_argument_group("Optional")
    optional.add_argument("-logfile", type=str, help="Path to the ouput log file")
    optional.add_argument(
        "-nmol", type=int, help="The predicted number of molecules to build"
    )
    optional.add_argument(
        "-molrep_tf",
        type=str,
        default="PTF",
        choices=["PTF", "SAPTF"],
        help="Type of phased translation function to use",
    )
    optional.add_argument(
        "-working_dir", type=str, help="Path to the working directory"
    )
    optional.add_argument("-xyzin", type=str, help="Path to the input pdb file")
    optional.add_argument("-xyz_list", nargs="+", help="List of input xyz files")
    optional.add_argument("-xyz_fixed", type=str, help="Path to a fixed model")
    optional.add_argument("-xyzout", type=str, help="Path to the output pdb file")
    optional.add_argument("-hklout", type=str, help="Path the output hkl file")
    optional.add_argument(
        "-sga",
        choices=SGAlternatives.__members__.keys(),
        help="Try alternative space groups",
    )
    args = parser.parse_args()

    molrep_kwargs = {}
    for arg in args._get_kwargs():
        if arg[0] in inspect.getfullargspec(Molrep).args:  # Ignore required arguments
            continue
        if arg[1] is not None:
            molrep_kwargs[arg[0]] = arg[1]

    if args.xyzin:
        molrep = Molrep(args.xyzin, **molrep_kwargs)
        molrep.run()

    elif args.xyz_list:
        # Simple strategy to run molrep on a list of pdb files
        del molrep_kwargs["xyzout"]
        if "xyz_fixed" in molrep_kwargs:
            del molrep_kwargs["xyz_fixed"]
        for i, xyzin in enumerate(args.xyz_list):
            if len(args.xyz_list) == 1:
                xyzout = args.xyzout
                xyz_fixed = args.xyz_fixed
            elif i == 0:
                xyzout = os.path.join(args.working_dir, "molrep", "molrep_temp.pdb")
                xyz_fixed = args.xyz_fixed
            elif i == len(args.xyz_list) - 1:
                xyzout = args.xyzout
                xyz_fixed = os.path.join(args.working_dir, "molrep", "molrep_temp.pdb")
                if not os.path.exists(xyz_fixed):
                    xyz_fixed = args.xyz_fixed
            else:
                xyzout = os.path.join(args.working_dir, "molrep", "molrep_temp.pdb")
                xyz_fixed = os.path.join(args.working_dir, "molrep", "molrep_temp.pdb")
                if not os.path.exists(xyz_fixed):
                    xyz_fixed = args.xyz_fixed

            molrep = Molrep(
                xyzin,
                xyzout=xyzout,
                xyz_fixed=xyz_fixed,
                ignore_existing=True,
                **molrep_kwargs,
            )
            molrep.run()
    else:
        raise RuntimeError("No input pdb file or list of pdb files provided")
