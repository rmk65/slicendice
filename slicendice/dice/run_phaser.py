__author__ = "Adam Simpkin & Ronan Keegan"
__date__ = "01 Feb 2022"
__version__ = "0.1"

import inspect
import os
from pathlib import Path
from phaser import InputMR_DAT, runMR_DAT, InputMR_AUTO, runMR_AUTO
from simbad.mr.options import SGAlternatives
from simbad.parsers import mtz_parser
from simbad.util import mtz_util


class PhaserRunError(Exception):
    pass


class Phaser(object):
    """Class to run Phaser"""

    def __init__(self, hklin: str, seqin: str, xyz_list: list, **kwargs):
        self.hklin = Path(hklin)
        self.seqin = Path(seqin)
        self.xyz_list = [Path(xyz).absolute() for xyz in xyz_list]

        self.fixed_xyzin = kwargs.get("fixed_xyzin", None)
        self.nmol = kwargs.get("nmol", 1)
        self.nproc = kwargs.get("nproc", 1)
        self.pack_cutoff = kwargs.get("pack_cutoff", 50)
        self.peak_rot_cutoff = kwargs.get("peak_rot_cutoff", 75)
        self.sgalternative = kwargs.get("sga", "none")
        self.working_dir = kwargs.get("working_dir", Path.cwd())

        self.run_dir = self.make_run_dir()
        self.logfile = kwargs.get("logfile", self.run_dir.joinpath("phaserAUTO.log"))
        self.hklout = kwargs.get("hklout", self.run_dir.joinpath("phaserAUTO.1.mtz"))
        self.xyzout = kwargs.get("xyzout", self.run_dir.joinpath("phaserAUTO.1.pdb"))

        if self.fixed_xyzin:
            assert Path(self.fixed_xyzin).exists()

        assert self.hklin.exists()
        assert self.seqin.exists()
        for xyz in self.xyz_list:
            assert xyz.exists()

    def make_run_dir(self):
        run_dir = Path(self.working_dir).joinpath("phaser")
        if run_dir.exists():
            raise RuntimeError(
                f"There is an existing work directory: {run_dir}\n "
                f"Please delete/move it aside."
            )
        run_dir.mkdir()
        return run_dir

    def run(self):
        current_work_dir = Path.cwd()
        os.chdir(self.run_dir)

        # Copy hklin, seqin and xyzin to working_dir for efficient running of Phaser
        hklin = self.run_dir.joinpath(self.hklin.name)
        hklin.write_bytes(self.hklin.read_bytes())
        seqin = self.run_dir.joinpath(self.seqin.name)
        seqin.write_text(self.seqin.read_text())

        xyz_list = []
        for f in self.xyz_list:
            xyz = self.run_dir.joinpath(f.name)
            xyz.write_text(f.read_text())
            xyz_list.append(xyz)

        # Run Phaser
        i = InputMR_DAT()
        i.setHKLI(str(hklin))

        input_mtz_obj = mtz_parser.MtzParser(str(self.hklin))
        input_mtz_obj.parse()
        if input_mtz_obj.i and input_mtz_obj.sigi:
            i.setLABI_I_SIGI(input_mtz_obj.i, input_mtz_obj.sigi)
        elif input_mtz_obj.f and input_mtz_obj.sigf:
            i.setLABI_F_SIGF(input_mtz_obj.f, input_mtz_obj.sigf)
        else:
            msg = "No flags for intensities or amplitudes have been provided"
            raise PhaserRunError(msg)
        i.setSGAL_SELE(SGAlternatives[self.sgalternative].value)
        i.setMUTE(True)
        r = runMR_DAT(i)

        if r.Success():
            i = InputMR_AUTO()
            i.setJOBS(self.nproc)
            i.setREFL_DATA(r.getREFL_DATA())
            i.setROOT("phaser_mr_output")

            if self.fixed_xyzin:
                i.addENSE_PDB_RMS("Fixed_model", self.fixed_xyzin, 1.2)
                i.setENSE_DISA_CHEC("Fixed_model", True)
                i.addSOLU_ORIG_ENSE("Fixed_model")

            for xyz in xyz_list:
                i.addENSE_PDB_RMS(xyz.stem, str(xyz), 1.2)
                i.addSEAR_ENSE_NUM(xyz.stem, self.nmol)
            i.setCOMP_BY("ASU")
            i.addCOMP_PROT_SEQ_NUM(str(self.seqin), self.nmol)
            i.setSGAL_SELE(SGAlternatives[self.sgalternative].value)
            i.setPACK_CUTO(self.pack_cutoff)
            i.setPEAK_ROTA_CUTO(self.peak_rot_cutoff)
            i.setMUTE(True)
            del r
            r = runMR_AUTO(i)

            with open(self.logfile, "w") as f:
                f.write(r.summary())

            if r.Success():
                Path(r.getTopPdbFile()).rename(self.xyzout)
                self.write_results(r, input_mtz_obj)

        # Return to original working dir
        os.chdir(current_work_dir)

        # Delete any files copied across
        for file in [hklin, seqin] + xyz_list:
            if file.exists():
                file.unlink()

    def write_results(self, r, input_mtz_obj):
        output_mtz_obj = mtz_parser.MtzParser(r.getTopMtzFile())

        if input_mtz_obj.spacegroup_symbol != output_mtz_obj.spacegroup_symbol:
            mtz_util.reindex(
                str(self.hklin),
                self.hklout,
                "".join(output_mtz_obj.spacegroup_symbol.split()),
            )
        else:
            Path(self.hklout).write_bytes(self.hklin.read_bytes())


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Runs MR using PHASER", prefix_chars="-"
    )

    core = parser.add_argument_group("Required")
    core.add_argument("-hklin", type=str, help="Path the input hkl file")
    core.add_argument("-seqin", type=str, help="Path the input sequence file")
    core.add_argument("-xyz_list", nargs="+", help="List of input xyz files")

    optional = parser.add_argument_group("Optional")
    optional.add_argument(
        "-fixed_xyzin", type=str, help="Path to an input fixed xyz file"
    )
    optional.add_argument("-hklout", type=str, help="Path the output hkl file")
    optional.add_argument("-logfile", type=str, help="Path to the ouput log file")
    optional.add_argument(
        "-nmol", type=int, help="The predicted number of molecules to build"
    )
    optional.add_argument(
        "-nproc",
        type=int,
        help="The number of processors to use in parallel parts of the code",
    )
    optional.add_argument(
        "-pack_cutoff", type=int, help="Limit on total number (or percent) of clashes"
    )
    optional.add_argument(
        "-peak_rot_cutoff",
        type=int,
        help="Cutoff value for the rotation function peak selection criteria.",
    )
    optional.add_argument(
        "-sga",
        choices=SGAlternatives.__members__.keys(),
        help="Try alternative space groups",
    )
    optional.add_argument(
        "-working_dir", type=str, help="Path to the working directory"
    )
    optional.add_argument("-xyzout", type=str, help="Path to the output xyz file")
    args = parser.parse_args()

    phaser_kwargs = {}
    for arg in args._get_kwargs():
        if arg[0] in inspect.getfullargspec(Phaser).args:  # Ignore required arguments
            continue
        if arg[1] is not None:
            phaser_kwargs[arg[0]] = arg[1]

    phaser = Phaser(args.hklin, args.seqin, args.xyz_list, **phaser_kwargs)
    phaser.run()
