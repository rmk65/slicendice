__author__ = "Luc Elliott"
__date__ = "23 Jan 2023"
__version__ = "0.1"

import inspect
from pathlib import Path
import os

KEYWORDS = [
    "directory",
    "nproc",
    "num",
    "xyz_fixed",
    "gpu",
    "no_resampling",
    "no_trimming",
    "bfac",
    "core_weighted",
    "laplace",
    "resampling_rate",
    "angle",
    "trimming_cutoff",
    "return_instances",
    "return_files",
    "basename",
    "logging_file",
]


class Powerfit(object):
    """
    A class that represents a PowerFit run.

    Attributes:
        __target (Path): The path to the target file.
        __resolution (float): The resolution of the run.
        __structure (Path): The path to the structure file.
        outfiles (None): The output files of the run.
        __kwargs (dict): A dictionary of keyword arguments for the run.
    """

    def __init__(self, exec, target, resolution, structure):
        """Initializes the PowerFit class.

        Args:
            exec (str): The path to the PowerFit executable.
            target (str): The path to the target file.
            resolution (float): The resolution of the run.
            structure (str): The path to the structure file.
        """

        self.__exec = Path(exec)
        self.__target = Path(target)
        self.__resolution = resolution
        self.__structure = Path(structure)

        self.outfiles = None

        self.__kwargs = {
            "return_instances": False,
            "directory": ".",
        }

    def set_kwargs(self, **kwargs):
        """Sets the keyword arguments for the PowerFit algorithm.

        Args:
            **kwargs: Arbitrary keyword arguments.

        Raises:
            KeyError: If an invalid keyword argument is provided.
        """
        for k, v in kwargs.items():
            if k not in KEYWORDS:
                raise KeyError(f"{k} is not a valid keyword argument.")

            self.__kwargs[k] = v

    @property
    def input_files(self):
        return {"Mapin": self.__target, "XYZin": self.__structure}

    def make_run_dir(self):
        """Makes a directory for the run, if it does not already exist."""
        current_dir = Path(self.__kwargs.get("directory", "."))
        run_dir = Path(current_dir).joinpath("powerfit")
        if run_dir.exists():
            pass
        else:
            run_dir.mkdir()
        return run_dir

    def run(self):
        """Runs the PowerFit docking software."""
        self.__kwargs["directory"] = self.make_run_dir()

        cmd = [
            str(self.__exec),
            str(self.__target),
            str(self.__resolution),
            str(self.__structure),
        ]

        for k, v in self.__kwargs.items():
            if v is None:
                continue
            if isinstance(v, bool):
                if v:
                    cmd.append(f"--{k}")
            else:
                cmd.append(f"--{k}")
                cmd.append(str(v))

        os.system(" ".join(cmd))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Runs docking with PowerFit", prefix_chars="-"
    )

    core = parser.add_argument_group("Required")
    core.add_argument(
        "-exec", type=str, dest="exec", help="Path to the powerfit executable"
    )
    core.add_argument(
        "-mapin", type=str, dest="target", help="Path to the input map file"
    )
    core.add_argument(
        "-xyzin", type=str, dest="structure", help="Path to the input pdb file"
    )
    core.add_argument(
        "-reso", type=float, dest="resolution", help="Resolution of the input map file"
    )
    optional = parser.add_argument_group("Optional")
    optional.add_argument("--logging_file", type=str, help="Path to the ouput log file")
    optional.add_argument(
        "-nmol", type=int, dest="num", help="The amount of models to be outputted"
    )
    optional.add_argument(
        "-xyz_fixed",
        type=str,
        help="Path to a fixed model to prevent overlap on outputs",
    )

    optional.add_argument(
        "-working_dir",
        dest="directory",
        type=Path,
        help="Path to the working directory",
    )
    optional.add_argument(
        "-basename", dest="basename", type=str, help="Basename for output files"
    )
    optional.add_argument("-nproc", type=int, help="Number of processors to use")
    optional.add_argument("-gpu", action="store_true", help="use GPU")
    optional.add_argument(
        "-no_resampling", action="store_true", help="Do not resample the map"
    )
    optional.add_argument(
        "-no_trimming", action="store_true", help="Do not trim the map"
    )
    optional.add_argument("-bfac", type=float, help="B-factor input for fitting")
    optional.add_argument(
        "-core_weighted", action="store_true", help="Use core weighted"
    )
    optional.add_argument("-laplace", action="store_true", help="Use laplace")
    optional.add_argument("-resampling_rate", type=float, help="Resampling rate")
    optional.add_argument("-angle", type=float, help="Angle")
    optional.add_argument("-trimming_cutoff", type=float, help="Trimming cutoff")

    args = parser.parse_args()

    powerfit_kwargs = {}
    for arg in args._get_kwargs():
        if arg[0] in inspect.getfullargspec(Powerfit).args:  # Ignore required arguments
            continue
        if arg[1] is not None:
            powerfit_kwargs[arg[0]] = arg[1]

    pf = Powerfit(args.exec, args.target, args.resolution, args.structure)
    pf.set_kwargs(**powerfit_kwargs)

    try:
        pf.run()
    except KeyboardInterrupt:
        import sys

        sys.stderr.write("Interrupted by keyboard!")
        exit(1)
