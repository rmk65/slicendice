__author__ = "Adam Simpkin & Ronan Keegan"
__date__ = "01 Feb 2022"
__version__ = "0.1"

import inspect
import os
from pathlib import Path
from pyjob.script import EXE_EXT
from pyjob import cexec


class Refmac(object):
    """Class to run refmac"""

    def __init__(self, hklin, xyzin, **kwargs):
        self.hklin = Path(hklin)
        self.xyzin = Path(xyzin)

        self.working_dir = kwargs.get("working_dir", Path.cwd())
        self.run_dir = self.make_run_dir()
        name = self.xyzin.stem
        self.logfile = kwargs.get(
            "logfile", self.run_dir.joinpath(f"{name}_refmac.log")
        )
        self.hklout = kwargs.get("hklout", self.run_dir.joinpath(f"{name}_refmac.mtz"))
        self.xyzout = kwargs.get("xyzout", self.run_dir.joinpath(f"{name}_refmac.pdb"))

    @staticmethod
    def refmac(hklin, hklout, xyzin, xyzout, logfile, key):
        cmd = [
            "refmac5" + EXE_EXT,
            "hklin",
            hklin,
            "hklout",
            hklout,
            "xyzin",
            xyzin,
            "xyzout",
            xyzout,
        ]
        stdout = cexec(cmd, stdin=key)
        with open(logfile, "w") as f_out:
            f_out.write(stdout)

    def make_run_dir(self):
        run_dir = Path(self.working_dir).joinpath("refmac")
        if run_dir.exists():
            raise RuntimeError(
                f"There is an existing work directory: {run_dir}\n "
                f"Please delete/move it aside."
            )
        run_dir.mkdir()
        return run_dir

    def run(self, ncyc=10):
        current_work_dir = Path.cwd()
        os.chdir(self.run_dir)

        # Copy hklin and xyzin to working_dir for efficient running of Phaser
        hklin = self.run_dir.joinpath(self.hklin.name)
        hklin.write_bytes(self.hklin.read_bytes())
        xyzin = self.run_dir.joinpath(self.xyzin.name)
        xyzin.write_text(self.xyzin.read_text())

        key = ["RIDG DIST SIGM 0.02", f"NCYC {ncyc}", "MAKE HYDR N"]

        Refmac.refmac(
            str(hklin.relative_to(self.run_dir)),
            str(self.hklout),
            str(xyzin.relative_to(self.run_dir)),
            str(self.xyzout),
            str(self.logfile),
            "\n".join(key),
        )

        # Return to original working directory
        os.chdir(current_work_dir)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Runs refinement using REFMAC", prefix_chars="-"
    )

    core = parser.add_argument_group("Required")
    core.add_argument("-hklin", type=str, help="Path the input hkl file")
    core.add_argument("-xyzin", type=str, help="Path to the input pdb file")

    optional = parser.add_argument_group("Optional")
    optional.add_argument("-hklout", type=str, help="Path the output hkl file")
    optional.add_argument("-logfile", type=str, help="Path to the ouput log file")
    optional.add_argument(
        "-ncyc", type=int, default=10, help="Number of cycles of refinement to run"
    )
    optional.add_argument(
        "-working_dir", type=str, help="Path to the working directory"
    )
    optional.add_argument("-xyzout", type=str, help="Path to the output pdb file")
    args = parser.parse_args()

    refmac_kwargs = {}
    for arg in args._get_kwargs():
        if arg[0] in inspect.getfullargspec(Refmac).args:  # Ignore required arguments
            continue
        if arg[1] is not None:
            refmac_kwargs[arg[0]] = arg[1]

    refmac = Refmac(args.hklin, args.xyzin, **refmac_kwargs)
    refmac.run(args.ncyc)
