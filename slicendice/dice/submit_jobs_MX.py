__author__ = "Adam Simpkin"
__date__ = "02 Feb 2022"
__version__ = "1.0"

import json
import logging
import os
import pandas as pd
from pathlib import Path
from pyjob.script import ScriptCollector, Script
from simbad.parsers import molrep_parser
from simbad.parsers import phaser_parser
from simbad.parsers import refmac_parser
from simbad.util import pdb_util, source_ccp4, submit_chunk
from tabulate import tabulate
import uuid

logger = logging.getLogger(__name__)

EXPORT = "SET" if os.name == "nt" else "export"
CMD_PREFIX = "call" if os.name == "nt" else ""
MR_PROG_HANDLE = {"molrep": "Molrep", "phaser": "Phaser"}


class Score(object):
    """A molecular replacement scoring class"""

    __slots__ = (
        "split_id",
        "phaser_llg",
        "phaser_tfz",
        "phaser_log",
        "molrep_score",
        "molrep_tfscore",
        "molrep_log",
        "final_r_fact",
        "final_r_free",
        "refmac_log",
        "xyzout",
        "hklout",
    )

    def __init__(self, split_id):
        self.split_id = split_id
        self.phaser_tfz = None
        self.phaser_llg = None
        self.phaser_log = None
        self.molrep_score = None
        self.molrep_tfscore = None
        self.molrep_log = None
        self.final_r_fact = 1.0
        self.final_r_free = 1.0
        self.refmac_log = None
        self.xyzout = None
        self.hklout = None

    def __repr__(self):
        string = "{name}(split_id={split_id} final_r_fact={final_r_fact} "
        "final_r_free={final_r_free})"
        return string.format(
            name=self.__class__.__name__,
            **{k: getattr(self, k) for k in self.__slots__},
        )

    def _asdict(self):
        return {k: getattr(self, k) for k in self.__slots__}


class MrSubmit(object):
    """Class to submit MR jobs for Slice'N'Dice"""

    def __init__(self, hklin: str, seqin: str, **kwargs):
        self.hklin = hklin
        self.seqin = seqin
        self.working_dir = kwargs.get("working_dir", None)
        self.tmp_dir = kwargs.get("tmp_dir", None)

        self.mr_program = kwargs.get("mr_program", "phaser")
        self.nmol = kwargs.get("no_mols", None)
        self.pack_cutoff = kwargs.get("pack_cutoff", None)
        self.peak_rot_cutoff = kwargs.get("peak_rot_cutoff", None)
        self.sgalternative = kwargs.get("sgalternative", "none")
        self.ncyc = kwargs.get("ncyc_refmac", 10)
        self.xyz_fixed = kwargs.get("xyz_fixed", None)

        self.results = None

    @staticmethod
    def sort_xyz_list(xyz_list):
        """Sort a list of XYZ files based on size of number of residues"""
        slice_lengths = {}
        for xyz in xyz_list:
            struct = pdb_util.PdbStructure().from_file(xyz)
            slice_lengths[xyz] = struct.nres
        sorted_slice_lengths = sorted(
            slice_lengths.items(), key=lambda item: item[1], reverse=True
        )
        return [x[0] for x in sorted_slice_lengths]

    def submit_jobs(
        self, results, nproc=1, submit_qtype=None, submit_queue=False, monitor=None
    ):
        run_files = []
        collector = ScriptCollector(None)
        for xyz_list in results:
            script, run_file = self.generate_script(xyz_list)
            collector.add(script)
            run_files.append(run_file)
        logger.info(
            f"\nRunning {MR_PROG_HANDLE[self.mr_program]} Molecular Replacement "
            "and Refmac5 refinement"
        )

        input_arguments = [
            collector,
            self.working_dir,
            nproc,
            "slicendice_mr",
            submit_qtype,
            submit_queue,
            True,
            monitor,
            mr_succeeded_log,
        ]
        submit_chunk(*input_arguments)

        mr_results = []
        (
            mr_xyzouts,
            mr_logfiles,
            refmac_xyzouts,
            refmac_hklouts,
            refmac_logfiles,
        ) = zip(*run_files)
        for (
            xyz_list,
            mr_xyzout,
            mr_logfile,
            refmac_xyzout,
            refmac_hklout,
            refmac_logfile,
        ) in zip(
            results,
            mr_xyzouts,
            mr_logfiles,
            refmac_xyzouts,
            refmac_hklouts,
            refmac_logfiles,
        ):
            if not Path(mr_logfile).exists():
                logger.debug(f"Cannot find phaser MR log file: {mr_logfile}")
                continue
            elif not Path(refmac_logfile).exists():
                logger.debug(f"Cannot find refmac5 refine log file: {refmac_logfile}")
                continue
            elif not Path(mr_xyzout).exists():
                logger.debug(f"Cannot find phaser output file: {mr_xyzout}")
                continue
            elif not Path(refmac_xyzout).exists():
                logger.debug(f"Cannot find refmac output file: {refmac_xyzout}")
                continue
            elif not Path(refmac_hklout).exists():
                logger.debug(f"Cannot find refmac output file: {refmac_hklout}")
                continue

            split = len(xyz_list)
            score = Score(split_id=f"split_{split}")
            if self.mr_program == "phaser":
                pp = phaser_parser.PhaserParser(mr_logfile)
                score.phaser_tfz = pp.tfz
                score.phaser_llg = pp.llg
                score.phaser_log = str(mr_logfile)
            elif self.mr_program == "molrep":
                mp = molrep_parser.MolrepParser(mr_logfile)
                score.molrep_score = mp.score
                score.molrep_tfscore = mp.tfscore
                score.molrep_log = str(mr_logfile)
            rp = refmac_parser.RefmacParser(refmac_logfile)
            score.final_r_free = rp.final_r_free
            score.final_r_fact = rp.final_r_fact
            score.refmac_log = str(refmac_logfile)
            score.xyzout = str(refmac_xyzout)
            score.hklout = str(refmac_hklout)

            mr_results += [score]

        self.results = sorted(
            mr_results, key=lambda x: float(x.final_r_free), reverse=False
        )

    def generate_script(self, xyz_list):
        working_dir = Path(xyz_list[0]).parent.resolve()
        split = len(xyz_list)

        if self.mr_program == "phaser":
            mr_logfile = working_dir.joinpath("phaser", f"split_{split}_phaser.log")
            mr_hklout = working_dir.joinpath("phaser", f"split_{split}_phaser.mtz")
            mr_xyzout = working_dir.joinpath("phaser", f"split_{split}_phaser.pdb")

            # Sort XYZ list to improve chances of success
            xyz_list = self.sort_xyz_list(xyz_list)

            mr_cmd = [
                CMD_PREFIX,
                "ccp4-python",
                "-m",
                "slicendice.dice.run_phaser",
                "-hklin",
                self.hklin,
                "-hklout",
                mr_hklout,
                "-seqin",
                self.seqin,
                "-xyz_list",
                " ".join(xyz_list),
                "-xyzout",
                mr_xyzout,
                "-logfile",
                mr_logfile,
                "-working_dir",
                working_dir,
                "-nmol",
                self.nmol,
                "-pack_cutoff",
                self.pack_cutoff,
                "-peak_rot_cutoff",
                self.peak_rot_cutoff,
                "-sga",
                self.sgalternative,
            ]

        elif self.mr_program == "molrep":
            mr_logfile = working_dir.joinpath("molrep", f"split_{split}_molrep.log")
            mr_hklout = working_dir.joinpath("molrep", f"split_{split}_molrep.mtz")
            mr_xyzout = working_dir.joinpath("molrep", f"split_{split}_molrep.pdb")

            mr_cmd = [
                CMD_PREFIX,
                "ccp4-python",
                "-m",
                "slicendice.dice.run_molrep",
                "-hklin",
                self.hklin,
                "-hklout",
                mr_hklout,
                "-xyz_list",
                " ".join(xyz_list),
                "-xyzout",
                mr_xyzout,
                "-logfile",
                mr_logfile,
                "-working_dir",
                working_dir,
                "-nmol",
                self.nmol,
                "-sga",
                self.sgalternative,
            ]

        run_files = [mr_xyzout, mr_logfile]

        if self.xyz_fixed:
            mr_cmd += [
                "-fixed_xyzin",
                self.xyz_fixed,
            ]

        refmac_logfile = working_dir.joinpath("refmac", f"split_{split}_refmac.log")
        refmac_hklout = working_dir.joinpath("refmac", f"split_{split}_refmac.mtz")
        refmac_xyzout = working_dir.joinpath("refmac", f"split_{split}_refmac.pdb")

        ref_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_refmac",
            "-xyzin",
            mr_xyzout,
            "-xyzout",
            refmac_xyzout,
            "-hklin",
            mr_hklout,
            "-hklout",
            refmac_hklout,
            "-logfile",
            refmac_logfile,
            "-working_dir",
            working_dir,
            "-ncyc",
            self.ncyc,
        ]

        source = source_ccp4()
        ccp4_scr = os.environ["CCP4_SCR"]
        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            mr_cmd + ["\n"],
            ref_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + ccp4_scr],
        ]
        run_script = Script(directory=working_dir, prefix="mr_", stem=str(split))

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files += [
            refmac_xyzout,
            refmac_hklout,
            refmac_logfile,
        ]

        return run_script, run_files

    def report_results(self, json_file=None):
        """Function to display results as a pretty table"""

        if self.mr_program == "phaser":
            columns = [
                "split_id",
                "phaser_llg",
                "phaser_tfz",
                "final_r_fact",
                "final_r_free",
            ]
        elif self.mr_program == "molrep":
            columns = [
                "split_id",
                "molrep_score",
                "molrep_tfscore",
                "final_r_fact",
                "final_r_free",
            ]
        df = pd.DataFrame([r._asdict() for r in self.results])

        if df.empty:
            logger.info("No results found")
        else:
            if json_file:
                logger.debug(f"Storing results in file: {json_file}")
                dice_data = {}
                for _, row in df.iterrows():
                    dice_data[row["split_id"]] = {
                        "phaser_llg": row["phaser_llg"],
                        "phaser_tfz": row["phaser_tfz"],
                        "phaser_logfile": row["phaser_log"],
                        "molrep_score": row["molrep_score"],
                        "molrep_tfscore": row["molrep_tfscore"],
                        "final_r_fact": row["final_r_fact"],
                        "final_r_free": row["final_r_free"],
                        "refmac_logfile": row["refmac_log"],
                        "xyzout": row["xyzout"],
                        "hklout": row["hklout"],
                    }

                if Path(json_file).exists():
                    with open(json_file, "r+") as f:
                        data = json.load(f)
                        data["dice"] = dice_data
                        f.seek(0)
                        json.dump(data, f, indent=4)
                else:
                    with open(json_file, "w") as f:
                        json.dump(dice_data, f, indent=4)

            df = df[columns]
            df.set_index("split_id", inplace=True)
            logger.info(tabulate(df, headers="keys", tablefmt="psql"))


class MrSubmitHybrid(object):
    """Class to submit MR jobs for Slice'N'Dice in hybrid mode"""

    def __init__(self, hklin: str, seqin: str, **kwargs):
        self.hklin = hklin
        self.seqin = seqin
        self.working_dir = Path(kwargs.get("working_dir", Path.cwd()))
        self.tmp_dir = kwargs.get("tmp_dir", None)

        self.llg_cutoff = kwargs.get("llg_cutoff", 45)
        self.nmol = kwargs.get("no_mols", None)
        self.molrep_tf = kwargs.get("molrep_tf", "PTF")
        self.pack_cutoff = kwargs.get("pack_cutoff", None)
        self.peak_rot_cutoff = kwargs.get("peak_rot_cutoff", None)
        self.sgalternative = kwargs.get("sga", "none")
        self.tfz_cutoff = kwargs.get("tfz_cutoff", 7)
        self.ncyc = kwargs.get("ncyc", 10)
        self.xyz_fixed = kwargs.get("xyz_fixed", None)

        self.results = None

    def submit_jobs(  # noqa: C901 # Reason: Function too complex
        self, results, nproc=1, submit_qtype=None, submit_queue=False, monitor=None
    ):
        mr_results = []
        for xyz_list in results:
            # Sort slices based on length - might replace this with eLLG in future
            slice_lengths = {}
            for xyz in xyz_list:
                struct = pdb_util.PdbStructure().from_file(xyz)
                slice_lengths[xyz] = struct.nres
            sorted_slice_lengths = dict(
                sorted(slice_lengths.items(), key=lambda item: item[1], reverse=True)
            )

            run_files = []
            collector = ScriptCollector(None)
            # Run each XYZ file in phaser individually
            for xyz in sorted_slice_lengths.keys():
                xyz_file = Path(xyz)
                working_dir = xyz_file.parent.resolve().joinpath(
                    f"phaser_{xyz_file.stem}"
                )
                if not working_dir.exists():
                    if self.xyz_fixed:
                        script, run_file = self.generate_phaser_scripts(
                            [str(xyz_file)], working_dir, fixed_xyzin=self.xyz_fixed
                        )
                    else:
                        script, run_file = self.generate_phaser_scripts(
                            [str(xyz_file)], working_dir
                        )
                    collector.add(script)
                    run_files.append(run_file)

            if len(collector) == 0:
                continue

            logger.info("\nRunning Phaser Molecular Replacement")

            input_arguments = [
                collector,
                str(self.working_dir),
                nproc,
                "slicendice_mr",
                submit_qtype,
                submit_queue,
                True,
                monitor,
                None,
            ]
            submit_chunk(*input_arguments)

            # Find individual models that succeeded
            solutions = []
            solution_run_files = []
            phaser_xyzouts, phaser_logfiles = zip(*run_files)
            for xyz, phaser_xyzout, phaser_logfile in zip(
                sorted_slice_lengths.keys(), phaser_xyzouts, phaser_logfiles
            ):
                if not Path(phaser_logfile).exists():
                    logger.debug(f"Cannot find phaser MR log file: {phaser_logfile}")
                    continue
                elif not Path(phaser_xyzout).exists():
                    logger.debug(f"Cannot find phaser output file: {phaser_xyzout}")
                    continue
                pp = phaser_parser.PhaserParser(phaser_logfile)
                if _phaser_mr_job_succeded(
                    pp.llg, pp.tfz, self.llg_cutoff, self.tfz_cutoff
                ):
                    solutions.append(xyz)
                    solution_run_files.append([phaser_xyzout, phaser_logfile])

            if len(solutions) == 0:
                logger.debug("No solutions found by Phaser")
                continue
            elif len(solutions) == 1:
                logger.info("1 slice placed by Phaser")
                phaser_xyzout, phaser_logfile = solution_run_files[0]
            else:
                logger.info(f"{len(solutions)} slices placed by Phaser")

                ref_xyz = Path(xyz_list[0])
                working_dir = ref_xyz.parent.resolve().joinpath("combined_phaser")
                if working_dir.exists():
                    logger.debug(f"{working_dir} exists, skipping")
                    phaser_logfile = working_dir.joinpath(
                        "phaser", f"{ref_xyz.stem}_phaser.log"
                    )
                    phaser_xyzout = working_dir.joinpath(
                        "phaser", f"{ref_xyz.stem}_phaser.pdb"
                    )
                else:
                    collector = ScriptCollector(None)
                    script, run_file = self.generate_phaser_scripts(
                        solutions[1:],
                        working_dir,
                        nproc=nproc,
                        fixed_xyzin=solutions[0],
                    )
                    collector.add(script)
                    logger.info(
                        "\nRunning second round of Phaser Molecular Replacement"
                    )
                    input_arguments = [
                        collector,
                        str(self.working_dir),
                        nproc,
                        "slicendice_mr",
                        submit_qtype,
                        submit_queue,
                        True,
                        monitor,
                        None,
                    ]
                    submit_chunk(*input_arguments)
                    phaser_xyzout, phaser_logfile = run_file

                if not Path(phaser_logfile).exists():
                    logger.debug(f"Cannot find phaser MR log file: {phaser_logfile}")
                    continue
                elif not Path(phaser_xyzout).exists():
                    logger.debug(f"Cannot find phaser output file: {phaser_xyzout}")
                    continue

            pp = phaser_parser.PhaserParser(phaser_logfile)
            final_phaser_tfz = pp.tfz
            final_phaser_llg = pp.llg

            best_molrep_score, best_molrep_tfscore, molrep_logs = None, None, []
            failures = [
                xyz for xyz in sorted_slice_lengths.keys() if xyz not in solutions
            ]
            if len(failures) != 0:
                # Run Refmac5 to get the starting R-fact/R-free values
                logger.info("\nRunning initial Refmac5 Refinement")
                (
                    refmac_xyzout,
                    refmac_logfile,
                    starting_r_free,
                    starting_r_fact,
                ) = self.submit_refmac_job(
                    phaser_xyzout, self.ncyc, nproc, submit_qtype, submit_queue, monitor
                )

                logger.info(f"Initial R-fact: {starting_r_fact}")
                logger.info(f"Initial R-free: {starting_r_free}")

                fixed_model, best_molrep_score, best_molrep_tfscore = (
                    refmac_xyzout,
                    None,
                    None,
                )

                for failure in failures:
                    (
                        xyz_out,
                        molrep_log,
                        score,
                        tfscore,
                        r_free,
                        r_fact,
                    ) = self.submit_molrep_job(
                        failure, fixed_model, nproc, submit_qtype, submit_queue, monitor
                    )
                    molrep_logs.append(str(molrep_log))
                    if r_free < starting_r_free and r_fact < starting_r_fact:
                        logger.info("R-scores improved to:")
                        logger.info(f"R-fact: {r_fact}")
                        logger.info(f"R-free: {r_free}")
                        starting_r_fact = r_fact
                        starting_r_free = r_free
                        fixed_model = xyz_out
                        best_molrep_score = score
                        best_molrep_tfscore = tfscore
                    else:
                        logger.info(
                            "Molrep unable to improve current solution, trying "
                            "next unplaced slice"
                        )
            else:
                fixed_model = phaser_xyzout

            # Run Refmac5 to get the final R-fact/R-free values
            logger.info("\nRunning final Refmac5 Refinement")
            (
                refmac_xyzout,
                refmac_logfile,
                final_r_free,
                final_r_fact,
            ) = self.submit_refmac_job(
                fixed_model, self.ncyc, nproc, submit_qtype, submit_queue, monitor
            )

            split = len(xyz_list)
            score = Score(split_id=f"split_{split}")
            score.phaser_tfz = final_phaser_tfz
            score.phaser_llg = final_phaser_llg
            score.phaser_log = str(phaser_logfile)
            score.molrep_score = best_molrep_score
            score.molrep_tfscore = best_molrep_tfscore
            score.molrep_log = molrep_logs
            score.final_r_free = final_r_free
            score.final_r_fact = final_r_fact
            score.refmac_log = str(refmac_logfile)
            score.xyzout = str(fixed_model)
            score.hklout = str(fixed_model).replace(".pdb", ".mtz")

            mr_results += [score]

        self.results = sorted(
            mr_results, key=lambda x: float(x.final_r_free), reverse=False
        )

    def submit_molrep_job(
        self, xyzin, fixed_model, nproc, submit_qtype, submit_queue, monitor
    ):
        name = Path(xyzin).stem
        working_dir = Path(xyzin).parent.resolve().joinpath(f"molrep_{name}")
        collector = ScriptCollector(None)
        script, run_file = self.generate_molrep_scripts(xyzin, fixed_model, working_dir)
        collector.add(script)

        logger.info(f"\nRunning Molrep {self.molrep_tf} search on {name}")

        input_arguments = [
            collector,
            str(self.working_dir),
            nproc,
            "slicendice_mr",
            submit_qtype,
            submit_queue,
            True,
            monitor,
            None,
        ]
        submit_chunk(*input_arguments)

        (
            molrep_xyzout,
            molrep_logfile,
            refmac_xyzout,
            refmac_hklout,
            refmac_logfile,
        ) = run_file
        if not Path(molrep_logfile).exists():
            logger.debug(f"Cannot find molrep MR log file: {molrep_logfile}")
            return None, 0, 0, 1, 1
        elif not Path(refmac_logfile).exists():
            logger.debug(f"Cannot find refmac5 refine log file: {refmac_logfile}")
            return None, 0, 0, 1, 1
        elif not Path(molrep_xyzout).exists():
            logger.debug(f"Cannot find molrep output file: {molrep_xyzout}")
            return None, 0, 0, 1, 1
        elif not Path(refmac_xyzout).exists():
            logger.debug(f"Cannot find refmac output file: {refmac_xyzout}")
            return None, 0, 0, 1, 1
        elif not Path(refmac_hklout).exists():
            logger.debug(f"Cannot find refmac output file: {refmac_hklout}")
            return None, 0, 0, 1, 1

        mp = molrep_parser.MolrepParser(molrep_logfile)
        molrep_score = mp.score
        molrep_tfscore = mp.tfscore
        rp = refmac_parser.RefmacParser(refmac_logfile)
        final_r_free = rp.final_r_free
        final_r_fact = rp.final_r_fact

        return (
            refmac_xyzout,
            molrep_logfile,
            molrep_score,
            molrep_tfscore,
            final_r_free,
            final_r_fact,
        )

    def submit_refmac_job(
        self, xyzin, ncyc, nproc, submit_qtype, submit_queue, monitor
    ):
        working_dir = self.working_dir.joinpath(f"refmac_{uuid.uuid4()}")
        collector = ScriptCollector(None)
        script, run_file = self.generate_refmac_scripts(
            xyzin, ncyc=ncyc, working_dir=working_dir
        )
        collector.add(script)

        input_arguments = [
            collector,
            str(self.working_dir),
            nproc,
            "slicendice_mr",
            submit_qtype,
            submit_queue,
            True,
            monitor,
            None,
        ]
        submit_chunk(*input_arguments)

        refmac_xyzout, refmac_hklout, refmac_logfile = run_file
        if not Path(refmac_logfile).exists():
            logger.debug(f"Cannot find refmac5 refine log file: {refmac_logfile}")
            return None, None, 1, 1
        elif not Path(refmac_xyzout).exists():
            logger.debug(f"Cannot find refmac output file: {refmac_xyzout}")
            return None, None, 1, 1
        elif not Path(refmac_hklout).exists():
            logger.debug(f"Cannot find refmac output file: {refmac_hklout}")
            return None, None, 1, 1

        rp = refmac_parser.RefmacParser(refmac_logfile)
        final_r_free = rp.final_r_free
        final_r_fact = rp.final_r_fact

        return refmac_xyzout, refmac_logfile, final_r_free, final_r_fact

    def generate_phaser_scripts(self, xyz_list, working_dir, nproc=1, fixed_xyzin=None):
        name = Path(xyz_list[0]).stem
        working_dir.mkdir()
        phaser_logfile = working_dir.joinpath("phaser", f"{name}_phaser.log")
        phaser_hklout = working_dir.joinpath("phaser", f"{name}_phaser.mtz")
        phaser_xyzout = working_dir.joinpath("phaser", f"{name}_phaser.pdb")

        mr_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_phaser",
            "-hklin",
            self.hklin,
            "-hklout",
            phaser_hklout,
            "-seqin",
            self.seqin,
            "-xyz_list",
            " ".join(xyz_list),
            "-xyzout",
            phaser_xyzout,
            "-logfile",
            phaser_logfile,
            "-working_dir",
            working_dir,
            "-nmol",
            self.nmol,
            "-nproc",
            nproc,
            "-pack_cutoff",
            self.pack_cutoff,
            "-peak_rot_cutoff",
            self.peak_rot_cutoff,
            "-sga",
            self.sgalternative,
        ]

        if fixed_xyzin:
            mr_cmd += ["-fixed_xyzin", fixed_xyzin]

        source = source_ccp4()
        ccp4_scr = os.environ["CCP4_SCR"]
        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            mr_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + ccp4_scr],
        ]
        run_script = Script(directory=working_dir, prefix="mr_", stem=name)

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = (phaser_xyzout, phaser_logfile)
        return run_script, run_files

    def generate_molrep_scripts(self, xyzin, xyz_fixed, working_dir):
        name = Path(xyzin).stem
        working_dir.mkdir()
        molrep_logfile = working_dir.joinpath("molrep", f"{name}_molrep.log")
        molrep_xyzout = working_dir.joinpath("molrep", f"{name}_molrep.pdb")

        refmac_logfile = working_dir.joinpath("refmac", f"{name}_refmac.log")
        refmac_hklout = working_dir.joinpath("refmac", f"{name}_refmac.mtz")
        refmac_xyzout = working_dir.joinpath("refmac", f"{name}_refmac.pdb")

        mr_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_molrep",
            "-hklin",
            self.hklin,
            "-xyz_list",
            " ".join([xyzin]),
            "-xyz_fixed",
            xyz_fixed,
            "-xyzout",
            molrep_xyzout,
            "-logfile",
            molrep_logfile,
            "-working_dir",
            working_dir,
            "-nmol",
            self.nmol,
            "-molrep_tf",
            self.molrep_tf,
        ]

        ref_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_refmac",
            "-xyzin",
            molrep_xyzout,
            "-xyzout",
            refmac_xyzout,
            "-hklin",
            self.hklin,
            "-hklout",
            refmac_hklout,
            "-logfile",
            refmac_logfile,
            "-working_dir",
            working_dir,
            "-ncyc",
            20,
        ]

        source = source_ccp4()
        ccp4_scr = os.environ["CCP4_SCR"]
        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            mr_cmd + ["\n"],
            ref_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + ccp4_scr],
        ]
        run_script = Script(directory=working_dir, prefix="mr_", stem=name)

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = (
            molrep_xyzout,
            molrep_logfile,
            refmac_xyzout,
            refmac_hklout,
            refmac_logfile,
        )
        return run_script, run_files

    def generate_refmac_scripts(self, xyzin, ncyc, working_dir):
        name = Path(xyzin).stem
        working_dir.mkdir()
        refmac_logfile = working_dir.joinpath("refmac", f"{name}_refmac.log")
        refmac_hklout = working_dir.joinpath("refmac", f"{name}_refmac.mtz")
        refmac_xyzout = working_dir.joinpath("refmac", f"{name}_refmac.pdb")

        ref_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_refmac",
            "-xyzin",
            xyzin,
            "-xyzout",
            refmac_xyzout,
            "-hklin",
            self.hklin,
            "-hklout",
            refmac_hklout,
            "-logfile",
            refmac_logfile,
            "-working_dir",
            working_dir,
            "-ncyc",
            ncyc,
        ]

        source = source_ccp4()
        ccp4_scr = os.environ["CCP4_SCR"]
        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            ref_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + ccp4_scr],
        ]
        run_script = Script(directory=working_dir, prefix="mr_", stem=name)

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = (refmac_xyzout, refmac_hklout, refmac_logfile)
        return run_script, run_files

    def report_results(self, json_file=None):
        """Function to display results as a pretty table"""
        columns = [
            "split_id",
            "phaser_llg",
            "phaser_tfz",
            "molrep_score",
            "molrep_tfscore",
            "final_r_fact",
            "final_r_free",
        ]

        df = pd.DataFrame([r._asdict() for r in self.results])

        if df.empty:
            logger.info("No results found")
        else:
            if json_file:
                logger.debug(f"Storing results in file: {json_file}")
                dice_data = {}
                for _, row in df.iterrows():
                    dice_data[row["split_id"]] = {
                        "phaser_llg": row["phaser_llg"],
                        "phaser_tfz": row["phaser_tfz"],
                        "molrep_score": row["molrep_score"],
                        "molrep_tfscore": row["molrep_tfscore"],
                        "final_r_fact": row["final_r_fact"],
                        "final_r_free": row["final_r_free"],
                        "xyzout": row["xyzout"],
                        "hklout": row["hklout"],
                    }

                if Path(json_file).exists():
                    with open(json_file, "r+") as f:
                        data = json.load(f)
                        data["dice"] = dice_data
                        f.seek(0)
                        json.dump(data, f, indent=4)
                else:
                    with open(json_file, "w") as f:
                        json.dump(dice_data, f, indent=4)

            df = df[columns]
            df.set_index("split_id", inplace=True)
            logger.info(tabulate(df, headers="keys", tablefmt="psql"))


def mr_succeeded_log(log):
    """Check a Molecular Replacement job for success"""
    log = Path(log)
    split = log.stem.split("_")[1]
    refmac_log = log.parent.resolve().joinpath("refmac", f"split_{split}_refmac.log")

    if refmac_log.exists():
        rp = refmac_parser.RefmacParser(str(refmac_log))
        return _mr_job_succeeded(rp.final_r_fact, rp.final_r_free)
    return False


def _mr_job_succeeded(r_fact, r_free):
    """Check values for job success"""
    return r_fact < 0.45 and r_free < 0.45


def _phaser_mr_job_succeded(llg, tfz, llg_cutoff=45, tfz_cutoff=7):
    """Check values for job success"""
    return llg > llg_cutoff and tfz > tfz_cutoff
