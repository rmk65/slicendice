__author__ = "Luc Elliott and Adam Simpkin"
__date__ = "04 May 2023"
__version__ = "1.0"

import logging
import os
from pathlib import Path
from pyjob.script import ScriptCollector, Script
from simbad.util import source_ccp4, submit_chunk
from slicendice.util import (
    check_powerfit_installation,
    check_powerfit_gpu_functionality,
    get_emplacement,
)
from slicendice.util.file_utils import FITTING_SCORES_NAME


EXPORT = "SET" if os.name == "nt" else "export"
CMD_PREFIX = "call" if os.name == "nt" else ""


class EmSubmit(object):
    """Class for submitting jobs for electron microscopy (EM) processing.

    Args:
        mapin (str): Path to input map file.
        parent_dir (str, optional): Parent directory for output files. Defaults to ".".
        tmp_dir (str, optional): Temporary directory for intermediate files. Defaults
            to None.
        xyz_fixed (str, optional): Path to fixed XYZ file. Defaults to None.
        no_mols (int, optional): Number of molecules in the input map. Defaults to 1.
        gpu (bool, optional): Whether to use GPU acceleration. Defaults to False.
        contour (float, optional): Contour level for the input map. Defaults to None.
            If None, the contour level will be calculated automatically.
        resolution (float): Resolution of the input map.
        nproc (int, optional): Number of processors to use. Defaults to 1.
        models (list, optional): List of models to use. Defaults to None.
        source (str, optional): Source of the input map. Defaults to source_ccp4().
        export_source (str, optional): Export source for intermediate files.
            Defaults to os.environ["CCP4_SCR"].
        prefix (str, optional): Prefix for output files. Defaults to "em_".
        name (str, optional): Name of the EM job. Defaults to "slicendice_em".
        dev (bool, optional): Whether to run in development mode. Defaults to False.
        logger (logging.Logger, optional): Logger. Defaults to None.
        fitting_scores_python_excecutable (str, optional): Path to python
            executable for fitting scores. Defaults to None.
        fitting_scores_site_packages (str, optional): Path to python
            site-packages for fitting scores. Defaults to None.

    """

    def __init__(self, **kwargs):
        self.mapin = kwargs.get("mapin")
        self.half_map_1 = kwargs.get("half_map_1", None)
        self.half_map_2 = kwargs.get("half_map_2", None)
        self.point_group_symmetry = kwargs.get("point_group_symmetry", None)
        self.sequence_composition = kwargs.get("seqin", None)
        self.starting_model_vrms = kwargs.get("starting_model_vrms", None)

        self._parent_dir = kwargs.get(
            "parent_dir", "."
        )  # runs in current directory by default
        self.make_dir(self._parent_dir)
        self.tmp_dir = kwargs.get("tmp_dir", None)
        self.xyz_fixed = kwargs.get("xyz_fixed", None)
        self.nmol = kwargs.get("no_mols", 1)
        self.gpu = kwargs.get("gpu", False)
        self.contour = kwargs.get("contour", None)
        self.resolution = float(kwargs.get("resolution"))
        self.nproc = kwargs.get("nproc", 1)
        self.models = kwargs.get("models", None)
        self.source = kwargs.get("source", source_ccp4())
        self.export_source = kwargs.get("export_source", os.environ["CCP4_SCR"])
        self.prefix = kwargs.get("prefix", "em_")
        self.name = kwargs.get("name", "slicendice_em")
        self.dev = kwargs.get("dev", False)
        self.logger = kwargs.get("logger", None)
        self.classifier_threshold = kwargs.get("classifier_threshold", 0.5)
        self.external_python = kwargs.get("fitting_scores_python_excecutable", None)
        self.external_python_path = kwargs.get("fitting_scores_site_packages", None)
        if self.logger is None:
            self.logger = logging.getLogger(__name__)
        self.collector = ScriptCollector(None)
        self.scoring_models = []

    @property
    def working_dir(self):
        """Return the working directory"""
        return self._working_dir

    @working_dir.setter
    def working_dir(self, working_dir):
        """Set the working directory, creating it if it doesn't exist"""

        self._working_dir = self.make_dir(working_dir)

    def generate_powerfit_scripts_em(self, working_dir, xyzin):
        """Generate the powerfit scripts for EM processing"""
        # For Linux only currently, windows support to be added later
        self.logger.debug(f"\nGenerating powerfit script for {xyzin.stem}")
        powerfit_exe = check_powerfit_installation()

        if os.name == "nt":
            msg = "Windows is not supported yet"
            raise RuntimeError(msg)

        xyzin = Path(xyzin)
        powerfit_logfile = working_dir.joinpath(
            "powerfit", f"{xyzin.stem}_powerfit.log"
        )
        basename = xyzin.stem

        pf_cmd = [
            "ccp4-python",
            "-m",
            "slicendice.dice.run_powerfit",
            "-exec",
            powerfit_exe,
            "-mapin",
            self.mapin,
            "-xyzin",
            str(xyzin),
            "-reso",
            self.resolution,
            "--logging_file",
            powerfit_logfile,
            "-working_dir",
            working_dir,
            "-nmol",
            10 if self.nmol <= 10 else (self.nmol + (self.nmol // 2)),
            "-nproc",
            self.nproc,
            "-basename",
            basename,
        ]

        if self.xyz_fixed is not None:
            [pf_cmd.append(x) for x in ["-xyz_fixed", str(self.xyz_fixed)]]

        if self.gpu:
            check_powerfit_gpu_functionality()
            pf_cmd.append("-gpu")

        cmd = [
            pf_cmd + ["\n"],
        ]

        run_script = Script(directory=working_dir, prefix=self.prefix, stem=xyzin.stem)

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = powerfit_logfile

        return run_script, run_files

    def generate_molrep_scripts_em(self, working_dir, xyzin):
        """Generate the molrep scripts for EM processing"""
        self.logger.debug(f"\nGenerating molrep script for {xyzin.stem}")
        molrep_logfile = working_dir.joinpath("molrep", f"{xyzin.stem}_molrep.log")
        molrep_xyzout = working_dir.joinpath("molrep", f"{xyzin.stem}_molrep.pdb")
        molrep_oxyzout = working_dir.joinpath(
            "molrep", f"{xyzin.stem}-molrep-origin-shift.pdb"
        )

        mr_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_molrep",
            "-mapin",
            self.mapin,
            "-xyzin",
            str(xyzin),
            "-xyzout",
            molrep_xyzout,
            "-logfile",
            molrep_logfile,
            "-working_dir",
            working_dir,
            "-nmol",
            self.nmol,
        ]

        if self.xyz_fixed is not None:
            [mr_cmd.append(x) for x in ["-xyz_fixed", self.xyz_fixed]]

        # Generate scores in the same way
        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [self.source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            mr_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + self.export_source],
        ]

        run_script = Script(
            directory=working_dir, prefix=self.prefix, stem=Path(xyzin).stem
        )

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = (molrep_xyzout, molrep_oxyzout, molrep_logfile)
        return run_script, run_files

    def generate_fitting_scores_scripts_em(self, xyzin: Path):
        """Generate the fitting scores scripts for EM processing"""
        self.logger.debug(f"\nGenerating fitting scores for {xyzin.stem}")
        self.make_dir(self._working_dir.joinpath("scores"))
        outfile = self._working_dir.joinpath(
            "scores", f"{xyzin.stem}_{FITTING_SCORES_NAME}.csv"
        )

        fs_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.scoresclassifier.fittingscores",
            "-mapin",
            self.mapin,
            "-xyzin",
            str(xyzin),
            "-resolution",
            self.resolution,
            "-outfile",
            outfile,
        ]
        if self.contour is not None:
            fs_cmd.extend(["-threshold", self.contour])

        if self.tmp_dir:  # use tempfile to store intermediate files
            # turn off when dev is true
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(self._working_dir)

        cmd = [
            [self.source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            fs_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + self.export_source],
        ]

        run_script = Script(
            directory=self._working_dir, prefix=self.prefix, stem=Path(xyzin).stem
        )

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = outfile
        return run_script, run_files

    def generate_fitting_scores_scripts_em_external(
        self, xyzin: Path, external_python_exe: str, external_site_packages: str
    ):
        """Generate the fitting scores scripts for EM processing
        Note: this is for external use, i.e. not using the CCP4 source
        This is for taking an input of a different python executable
        """
        self.logger.debug(f"\nGenerating fitting scores for {xyzin.stem}")
        self.make_dir(self._working_dir.joinpath("scores"))
        outfile = self._working_dir.joinpath(
            "scores", f"{xyzin.stem}_{FITTING_SCORES_NAME}.csv"
        )
        pathway = Path(os.path.dirname(os.path.realpath(__file__)))
        fs_cmd = [
            CMD_PREFIX,
            external_python_exe,
            str(pathway.joinpath("scoresclassifier", "fittingscores.py")),
            "-mapin",
            str(self.mapin),
            "-xyzin",
            str(xyzin),
            "-resolution",
            self.resolution,
            "-outfile",
            str(outfile),
        ]
        if self.contour is not None:
            fs_cmd.extend(["-threshold", self.contour])

        cmd = [
            [CMD_PREFIX],
            ["export", f'"PATH={external_python_exe}:$PATH"'],
            [
                "export",
                f'"PYTHONPATH={external_site_packages}:$PYTHONPATH"',
            ],
            fs_cmd + ["\n"],
        ]

        run_script = Script(
            directory=self._working_dir, prefix=self.prefix, stem=Path(xyzin).stem
        )

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        run_files = outfile
        return run_script, run_files

    def generate_em_placement_scripts_em(self, working_dir, xyzin):
        """Generate the em_placement scripts for EM processing"""
        self.logger.debug(f"\nGenerating em_placement script for {xyzin.stem}")

        # Currently I'm on ccp4 9.003 where emplacement is in the libexec folder
        # This may need to be updated in the future
        emplacement_exe = get_emplacement()
        phil_file = working_dir.joinpath(f"{xyzin.stem}.phil")
        emp_cmd = [
            CMD_PREFIX,
            "ccp4-python",
            "-m",
            "slicendice.dice.run_em_placement",
            "--emplacement_exec",
            emplacement_exe,
            "--working_dir",
            working_dir,
            "--half_map_1",
            self.half_map_1,
            "--half_map_2",
            self.half_map_2,
            "--best_resolution",
            self.resolution,
            "--point_group_symmetry",
            self.point_group_symmetry,
            "--sequence_composition",
            self.sequence_composition,
            "--molecule_name",
            xyzin.stem,
            "--map_or_model_file",
            str(xyzin),
            "--starting_model_vrms",
            self.starting_model_vrms,
            "--phil_file",
            phil_file,
        ]

        if self.tmp_dir:
            tmp_dir = self.tmp_dir
        else:
            tmp_dir = str(working_dir)

        cmd = [
            [self.source],
            [EXPORT, "CCP4_SCR=" + tmp_dir],
            emp_cmd + ["\n"],
            [EXPORT, "CCP4_SCR=" + self.export_source],
        ]

        run_script = Script(
            directory=self._working_dir, prefix=self.prefix, stem=Path(xyzin).stem
        )

        for c in cmd:
            run_script.append(" ".join(map(str, c)))

        return run_script, None

    def submit_fitting_scores_job(self, submit_qtype, submit_queue):
        """Submit the fitting scores job"""
        self.logger.debug("\nSubmitting fitting scores job")
        run_files = []
        if len(self.scoring_models) == 0:
            self.logger.debug("\nNo models to process")
            return None

        for xyzin in self.scoring_models:
            xyzin = Path(xyzin)
            if (
                self.external_python is not None
                and self.external_python_path is not None
            ):
                script, run_file = self.generate_fitting_scores_scripts_em_external(
                    xyzin, self.external_python, self.external_python_path
                )

            else:
                script, run_file = self.generate_fitting_scores_scripts_em(xyzin)

            self.collector.add(script)
            run_files.append(run_file)

        input_arguments = self.input_argument_generator(
            self.nproc,
            submit_qtype,
            submit_queue,
        )

        submit_chunk(*input_arguments)
        csv_pathway = self._working_dir.joinpath("scores", "fittingscores.csv")
        self.reset_collector()  # Reset the collector
        self.combine_csvs(run_files, csv_pathway)

        return str(csv_pathway.resolve())

    def submit_powerfit_job(self, submit_qtype, submit_queue):
        """Submit the powerfit job"""
        self.logger.debug("\nSubmitting powerfit job")
        # run_files = []
        if len(self.models) == 0:
            msg = "No models to process"
            raise ValueError(msg)
        if not len(self.models) == 1:
            msg = "Currently only one model is supported"
            raise ValueError(msg)

        xyzin = Path(self.models[0])

        dir_name = (
            f"powerfit_{xyzin.stem}"
            if self.xyz_fixed is None
            else f"powerfit_{xyzin.stem}_"
            f"{Path(self.xyz_fixed).stem.replace('fixed_file_', '')}"
        )

        job_directory = self.working_dir.resolve().joinpath(dir_name)
        self.make_dir(job_directory)

        script, run_file = self.generate_powerfit_scripts_em(job_directory, xyzin)
        self.collector.add(script)
        # run_files.append(run_file)

        input_arguments = self.input_argument_generator(
            1,
            submit_qtype,
            submit_queue,
        )

        submit_chunk(*input_arguments)
        self.reset_collector()  # Reset the collector

    def submit_molrep_job_em(self, submit_qtype, submit_queue, monitor=None):
        """Submit the molrep job"""
        self.logger.debug("\nSubmitting molrep job")
        if len(self.models) == 0:
            msg = "No models to process"
            raise ValueError(msg)

        for xyzin in self.models:
            xyzin = Path(xyzin)

            dir_name = (
                f"molrep_{xyzin.stem}"
                if self.xyz_fixed is None
                else f"molrep_{xyzin.stem}_"
                f"{Path(self.xyz_fixed).stem.replace('fixed', '')}"
            )

            working_dir = self._working_dir.resolve().joinpath(dir_name)
            self.make_dir(working_dir)
            script, _ = self.generate_molrep_scripts_em(working_dir, xyzin)
            self.collector.add(script)

        input_arguments = self.input_argument_generator(
            self.nproc,
            submit_qtype,
            submit_queue,
        )

        submit_chunk(*input_arguments)
        self.reset_collector()  # Reset the collector

    def submit_em_placement_job(self, submit_qtype, submit_queue):
        """Submit the EM placement job"""
        self.logger.debug("\nSubmitting EM placement job")
        run_files = []
        em_placement_exe = get_emplacement()
        if not em_placement_exe:
            msg = (
                "em_placement is not installed or is not in your PATH. Please install "
                "the latest version of ccp4 via https://www.ccp4.ac.uk/download/  "
                "or directly from Phenix via https://phenix-online.org/download/"
            )
            raise RuntimeError(msg)

        if len(self.models) == 0:
            msg = "No models to process"
            raise ValueError(msg)

        for xyzin in self.models:
            xyzin = Path(xyzin)

            dir_name = (
                f"emplacement_{xyzin.stem}"
                if self.xyz_fixed is None
                else f"emplacement_{xyzin.stem}_"
                f"{Path(self.xyz_fixed).stem.replace('fixed_file_', '')}"
            )

            working_dir = self._working_dir.resolve().joinpath(dir_name)
            self.make_dir(working_dir)
            script, run_file = self.generate_em_placement_scripts_em(working_dir, xyzin)
            self.collector.add(script)
            run_files.append(run_file)

        input_arguments = self.input_argument_generator(
            self.nproc,
            submit_qtype,
            submit_queue,
        )

        submit_chunk(*input_arguments)
        self.reset_collector()

    def input_argument_generator(self, nproc, submit_qtype, submit_queue, monitor=None):
        """Generate the input arguments for submitting jobs"""
        return [
            self.collector,
            str(self._working_dir),
            nproc,
            self.name,
            submit_qtype,
            submit_queue,
            True,
            monitor,
            None,
        ]

    def get_scoring_inputs(self, glob_expression: str) -> None:
        """Get the scoring inputs from the working directory"""
        self.scoring_models = [x for x in self._working_dir.rglob(glob_expression)]

    @staticmethod
    def make_dir(new_dir) -> Path:
        """Make a directory if it doesn't exist"""
        new_dir = Path(new_dir)

        if new_dir.exists():
            pass
        else:
            new_dir.mkdir()

        return new_dir

    def reset_collector(self):
        """Reset the collector so that it can be reused"""
        self.collector = ScriptCollector(None)

    def combine_csvs(self, csv_files, outfile):
        """Combine the csv files into a single csv file,
        Note: should maybe be moved into helper class than EM submit"""
        from pandas import read_csv, concat

        if len([f for f in csv_files if f.exists()]) == 0:
            self.logger.debug("\nNo csv files to combine, returning empty csv")
            Path(outfile).touch()
            return

        """Combine the csv files"""
        df = concat((read_csv(f, header=0) for f in csv_files if f.exists()))
        df.to_csv(outfile, index=False)
