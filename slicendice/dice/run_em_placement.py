from pathlib import Path
import subprocess
from typing import Union
import os


class EmPlacement:
    def __init__(
        self,
        **kwargs,
    ):
        self.emplacement_exec = kwargs.get("emplacement_exec")
        self.phil_file = kwargs.get("phil_file")
        self.working_dir = kwargs.get("working_dir")
        self.name = kwargs.get("molecule_name", "")
        self.ignore_existing = kwargs.get("ignore_existing", True)

        kwargs.pop("emplacement_exec")
        kwargs.pop("phil_file")
        kwargs.pop("working_dir")
        self.run_dir = self.make_run_dir()

        self.make_phil_file(**kwargs, phil_file=self.phil_file)

        if not Path(self.phil_file).exists():
            msg = f"phil file {self.phil_file} not found"
            raise FileNotFoundError(msg)

    def run(self):
        os.chdir(self.run_dir)
        cmd = [self.emplacement_exec, self.phil_file]

        run = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        if run.returncode != 0:
            msg = f"em_placement failed with the following error: \
{run.stderr.decode('utf-8')}"
            raise RuntimeError(msg)

        output_pdbs = [f for f in self.run_dir.rglob("docked_model*.pdb")]
        csv_out = [f for f in self.run_dir.rglob("output_table_docking.csv")][0]

        for pdb in output_pdbs:
            pdb_num = pdb.stem[-1]
            new_name = pdb.with_name(f"docked_model_{self.name}_{pdb_num}.pdb")
            pdb.rename(new_name)

        print(run.stdout.decode("utf-8"))
        self.post_process_csv(csv_out)

    def make_run_dir(self):
        run_dir = Path(self.working_dir).joinpath("em_placement")
        if run_dir.exists() and not self.ignore_existing:
            raise RuntimeError(
                f"There is an existing work directory: {run_dir}\n "
                f"Please delete/move it aside."
            )
        elif not run_dir.exists():
            run_dir.mkdir()
        return run_dir

    def post_process_csv(self, csv_file):
        """
        Post-process the CSV file to update the index and column names.


        Args:
            csv_file (pathlib.Path): The path to the CSV file.

        Returns:
            pandas.DataFrame: The DataFrame with updated index.
        """
        import pandas as pd

        df = pd.read_csv(csv_file, index_col=0, header=0)
        csv_path = Path(csv_file).parent
        model_name = Path(df["Model"].values[0]).stem

        def update_index(index):
            if isinstance(index, str):
                num = index.split("-")[-1].split(".")[0]
                return str(csv_path / f"docked_model_{model_name}_{num}.pdb")
            return index

        df.index = df.index.map(update_index)
        df.rename(columns={"Model": model_name}, inplace=True)
        df.to_csv(csv_file)

    def make_phil_file(
        self,
        half_map_1: Union[str, Path],
        half_map_2: Union[str, Path],
        best_resolution: Union[int, float],
        point_group_symmetry: str,
        sequence_composition: str,
        molecule_name: str,
        map_or_model_file,
        starting_model_vrms: Union[int, float],
        phil_file: Union[str, Path],
    ):

        phil_text = f"""
        voyager
            {{
            remove_phasertng_folder = True
            map_model
            {{
                half_map = {str(half_map_1)}
                half_map = {str(half_map_2)}
                best_resolution = {str(best_resolution)}
                point_group_symmetry = {str(point_group_symmetry)}
                sequence_composition = {str(sequence_composition)}
            }}

            biological_unit {{
            molecule
            {{
                molecule_name = {str(molecule_name)}
                map_or_model_file = {str(map_or_model_file)}
                starting_model_vrms = {str(starting_model_vrms)}
            }}
            }}
        }}
        """
        with open(phil_file, "w") as phil:
            phil.write(phil_text)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Run em_placement")
    parser.add_argument(
        "--emplacement_exec",
        type=str,
        help="The executable for emplacement",
        default="em_placement",
    )
    parser.add_argument(
        "--working_dir",
        type=str,
        help="The working directory",
        default=".",
    )
    parser.add_argument(
        "--half_map_1",
        type=str,
        help="The first half map",
        required=True,
    )
    parser.add_argument(
        "--half_map_2",
        type=str,
        help="The second half map",
        required=True,
    )
    parser.add_argument(
        "--best_resolution",
        type=float,
        help="The best resolution",
        required=True,
    )
    parser.add_argument(
        "--point_group_symmetry",
        type=str,
        help="The point group symmetry",
        required=True,
    )
    parser.add_argument(
        "--sequence_composition",
        type=str,
        help="The sequence composition",
        required=True,
    )
    parser.add_argument(
        "--molecule_name",
        type=str,
        help="The molecule name",
        required=True,
    )
    parser.add_argument(
        "--map_or_model_file",
        type=str,
        help="The map or model file",
        required=True,
    )
    parser.add_argument(
        "--starting_model_vrms",
        type=float,
        help="The starting model vrms",
        required=True,
    )
    parser.add_argument(
        "--phil_file",
        type=str,
        help="The phil file path",
        default="emplacement.phil",
    )

    args = parser.parse_args()

    emplacement = EmPlacement(
        **vars(args),
    )
    emplacement.run()
