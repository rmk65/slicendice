#!/usr/bin/env ccp4-python
import logging
from struvolpy import Structure, Volume

logging.basicConfig(level=logging.DEBUG)


def test_struvolpy_structure(test_data):
    try:
        Structure.from_file(test_data.test_6iwd_pdb)
    except Exception as e:
        assert False, f"Structure.from_file failed with error: {e}"


def test_struvolpy_volume(test_data):
    try:
        Volume.from_file(test_data.test_3407_segment_mrc)
    except Exception as e:
        assert False, f"Volume.from_file failed with error: {e}"
