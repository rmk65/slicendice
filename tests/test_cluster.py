#!/usr/bin/env ccp4-python
import importlib
import logging
import pytest
from slicendice.slice import clustering_tasks
from slicendice.slice.cluster import (
    clusterfactory,
    CLUSTERING_METHODS,
    ClusteringMethodNotFoundError,
)

logging.basicConfig(level=logging.DEBUG)


def test_clusterfactory_1():
    method = "NotAMethod"
    if method not in CLUSTERING_METHODS:
        with pytest.raises(ClusteringMethodNotFoundError):
            clusterfactory(method, None)


def test_clusterfactor_2():
    method = "agglomerativeclustering"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceAgglometative)


def test_clusterfactor_3():
    method = "birch"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceBirch)


def test_clusterfactor_4():
    method = "dbscan"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceDBscan)


def test_clusterfactor_5():
    method = "kmeans"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceKMeans)


def test_clusterfactor_6():
    method = "meanshift"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceMeanShift)


def test_clusterfactor_7():
    method = "optics"
    with clusterfactory(method, None, 1) as cluster:
        assert isinstance(cluster, clustering_tasks.SliceOptics)


def test_clusterfactor_8():
    for _, v in CLUSTERING_METHODS.items():
        module, class_ = v
        assert getattr(importlib.import_module(module), class_)


def test_clusterfactor_9(test_data):
    method = "pae_networkx"
    with clusterfactory(
        method, None, pae_file=test_data.test_Q5VSL9_AF_model_json, nclust=1
    ) as cluster:
        assert isinstance(cluster, clustering_tasks.SlicePaeNetworkx)


def test_clusterfactor_10(test_data):
    method = "pae_igraph"
    with clusterfactory(
        method, None, pae_file=test_data.test_Q5VSL9_AF_model_json, nclust=1
    ) as cluster:
        assert isinstance(cluster, clustering_tasks.SlicePaeIGraph)


if __name__ == "__main__":
    import sys

    pytest.main([__file__] + sys.argv[1:])
