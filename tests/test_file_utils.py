#!/usr/bin/env ccp4-python

import json
import logging
import pandas as pd
from pathlib import Path
import pytest
from slicendice.util.file_utils import (
    FileTracker,
    ReportOutput,
    NON_PASSED,
    RESULTS_DIR,
    OUTPUT_DIR,
    GROUPED_DIR,
)
import tempfile


@pytest.fixture
def file_tracker():
    # use Tempfile to create a temporary directory
    tmpdir = tempfile.TemporaryDirectory()
    # create a FileTracker object
    tmp_split_dir = Path(tmpdir.name).joinpath("split_2")
    tmp_split_dir.mkdir()
    file_tracker = FileTracker(tmp_split_dir)

    yield file_tracker


@pytest.fixture
def report_output():
    logger = logging.getLogger(__name__)
    return ReportOutput(logger=logger)


@pytest.fixture
def tmp_proba_jsons():
    tmpdir = tempfile.TemporaryDirectory()
    tmp_split_dir = Path(tmpdir.name).joinpath("split_2")
    tmp_split_dir.mkdir()
    tmp_results_dir = tmp_split_dir.joinpath(RESULTS_DIR)
    tmp_results_dir.mkdir()
    tmp_proba_jsons = iter(
        [
            {"pdb_8GTD_cluster_0_1": 0.9},
            {"pdb_8GTD_cluster_1_1": 0.8},
            {"pdb_8GTD_cluster_2_1": 0.7},
        ]
    )
    json_paths = []
    for dir_ in [OUTPUT_DIR, GROUPED_DIR, NON_PASSED]:
        dir_ = tmp_results_dir.joinpath(dir_)
        dir_.mkdir()
        with open(dir_.joinpath("model_proba.json"), "w") as f:
            json.dump(next(tmp_proba_jsons), f)
            json_paths.append(dir_.joinpath("model_proba.json"))
    yield json_paths


def test_file_tracker_initialization(file_tracker):
    assert file_tracker.parent_directory == file_tracker.results_directory.parent
    assert file_tracker.json_dict["parent_directory"] == str(
        file_tracker.parent_directory
    )
    assert file_tracker.json_dict["results_directory"] == str(
        file_tracker.results_directory
    )
    assert file_tracker.json_dict["fixed_xyz"] is None
    assert file_tracker.json_dict["cycle_1"] == []
    assert file_tracker.json_dict["extra_1"] == []


def test_file_tracker_update_fixed_file(file_tracker, tmp_path):
    fixed_file_path = tmp_path / "fixed.xyz"
    file_tracker.update_fixed_file(fixed_file_path)
    assert file_tracker.json_dict["fixed_xyz"] == str(fixed_file_path)


def test_file_tracker_copy_output_file(file_tracker, tmp_path):
    output_file_path = tmp_path / "output.txt"
    output_file_path.touch()
    file_tracker.copy_output_file(output_file_path)
    assert file_tracker.results_directory.joinpath(output_file_path.name).exists()


def test_file_tracker_return_all_file_pathways(file_tracker, tmp_path):
    output_file_path = tmp_path / "output.txt"
    output_file_path.touch()
    file_tracker.copy_output_file(output_file_path)
    file_pathways = file_tracker.return_all_file_pathways()
    assert (
        str(file_tracker.results_directory.joinpath(output_file_path.name))
        in file_pathways
    )


def test_top_n_non_passed(file_tracker):
    # Create some test files
    test_files = [
        "prefixa_1_1.pdb",
        "prefixa_1_2.pdb",
        "prefixa_2_1.pdb",
        "prefixb_1_1.pdb",
        "prefixb_1_2.pdb",
        "prefixb_2_1.pdb",
        "prefixa_2_3.pdb",
    ]

    input_files_test = [
        "prefixa_1.pdb",
        "prefixb_1.pdb",
    ]
    file_tracker.parent_directory.joinpath("molrep").mkdir()
    file_tracker.parent_directory = file_tracker.parent_directory.joinpath("molrep")

    for file in test_files:
        file_path = file_tracker.parent_directory / file
        file_path.touch()
        if file != "prefixa_2_3.pdb" and file != "prefixb_2_4.pdb":
            file_tracker.copy_output_file(file_path)

    for file in input_files_test:
        file_path = file_tracker.parent_directory / "one1"
        file_path.mkdir(exist_ok=True)
        file_path = file_path / file
        file_path.touch()

    values = [str(file_tracker.parent_directory.joinpath("prefixa_2_3.pdb")), "0.9"]
    df = pd.DataFrame([values], columns=["name", "proba"])
    df.set_index("name", inplace=True)

    df.to_csv(
        file_tracker.parent_directory / "prefixa_2_1fitting_scores.csv",
        index=True,
    )

    file_tracker.parent_directory = file_tracker.parent_directory.parent

    file_tracker.top_n_non_passed(2)

    # Check if the files are moved to the NON_PASSED directory
    non_passed_directory = file_tracker.results_directory / NON_PASSED

    file_path = non_passed_directory / test_files[-1]

    assert file_path.exists()

    for file in test_files:
        file_path = file_tracker.parent_directory / "molrep" / file
        assert file_path.exists()


def test_common_and_non_common_prefixes(file_tracker):
    paths = [
        "file1_cluster_1.txt",
        "file2_cluster_2.txt",
        "file3_cluster_3.txt",
        "File1_cluster_1.txt",
    ]
    common_prefixes = file_tracker.find_common_prefixes(paths)

    assert len(common_prefixes) == 2
    assert common_prefixes[0] == "file"
    assert common_prefixes[1] == "File1_"


def test_report_output_groupings(caplog, report_output):
    caplog.set_level(logging.DEBUG)
    groups = {1: ["8GTD_cluster_0_1.pdb", "8GTD_cluster_1_1.pdb"]}
    report_output.report_groupings(groups)

    assert "+----------------+" in caplog.text
    assert "| Grouped models |" in caplog.text
    assert "+----------------+" in caplog.text
    assert "| Slice name   |   Group |   Cluster |   Model number |" in caplog.text
    assert "|:-------------|--------:|----------:|---------------:|" in caplog.text
    assert "| 8GTD         |       1 |         0 |              1 |" in caplog.text
    assert "| 8GTD         |       1 |         1 |              1 |" in caplog.text


def test_report_output_probabilities(caplog, report_output, tmp_proba_jsons):
    caplog.set_level(logging.DEBUG)
    # print out the logging messages

    report_output.report_probabilities(tmp_proba_jsons)

    print(caplog.text)

    assert "+---------------------+" in caplog.text
    assert "| Model probabilities |" in caplog.text
    assert "+---------------------+" in caplog.text
    assert "| Slice name   |   Cluster |   Model number |   Score |" in caplog.text
    assert "|:-------------|----------:|---------------:|--------:|" in caplog.text
    assert "| pdb_8GTD     |         0 |              1 |     0.9 |" in caplog.text
    assert "| pdb_8GTD     |         1 |              1 |     0.8 |" in caplog.text


def test_report_output_non_passed(caplog, report_output, tmp_proba_jsons):
    caplog.set_level(logging.DEBUG)
    report_output.report_non_passed_models(tmp_proba_jsons)
    assert "+-------------------+" in caplog.text
    assert "| Non-passed models |" in caplog.text
    assert "+-------------------+" in caplog.text
    assert "| Slice name   |   Cluster |   Model number |   Score |" in caplog.text
    assert "|:-------------|----------:|---------------:|--------:|" in caplog.text
    assert "| pdb_8GTD     |         2 |              1 |     0.7 |" in caplog.text


def test_report_no_mols_passed(caplog, report_output, tmp_proba_jsons):
    caplog.set_level(logging.DEBUG)
    report_output.report_no_mols_passed(tmp_proba_jsons, 2)

    assert "+-----------------------------+" in caplog.text
    assert "| Number of models passed / 2 |" in caplog.text
    assert "+-----------------------------+" in caplog.text
    assert "|   Cluster | Number of models   |" in caplog.text
    assert "|----------:|:-------------------|" in caplog.text
    assert "|         0 | 1 / 2              |" in caplog.text
    assert "|         1 | 1 / 2              |" in caplog.text
    assert "|         2 | 0 / 2              |" in caplog.text
