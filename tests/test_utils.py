#!/usr/bin/env ccp4-python

import pandas as pd
from slicendice.util import find_best_non_overlapping_models
from slicendice.util import check_powerfit_installation


def test_find_best_non_overlapping_models(test_data):
    model_scores = pd.DataFrame(
        {
            "Model": [
                test_data.test_8GTD_1_true_pdb,
                test_data.test_8GTD_2_true_pdb,
                test_data.test_8GTD_cluster_0_pdb,
                test_data.test_8GTD_cluster_1_pdb,
                test_data.test_8GTD_cluster_0_pdb,
            ],
            "Score": [0.9, 0.8, 0.7, 0.6, 0.91],
        }
    )

    model_scores.set_index("Model", inplace=True)
    best_models = find_best_non_overlapping_models(model_scores, "Score")
    assert best_models == [
        "../test_data/8GTD_cluster_0.pdb",
        "../test_data/8GTD_1_true.pdb",
        "../test_data/8GTD_2_true.pdb",
        "../test_data/8GTD_cluster_1.pdb",
    ]


def test_powerfit_installation():
    try:
        powerfitexe = check_powerfit_installation()
    except FileNotFoundError:
        assert True

    except Exception:
        assert False
    else:
        assert isinstance(powerfitexe, str)
