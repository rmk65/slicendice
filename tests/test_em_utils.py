import pytest
from slicendice.util.em_util import EmSubmit_lite, em_check
from pyjob.script import Script


@pytest.fixture
def em_submit_lite():
    return EmSubmit_lite()


def test_fitting_scores_em_external_help(em_submit_lite):
    script, _ = em_submit_lite.fitting_scores_em_external_help()
    assert isinstance(script, Script)


def test_submit_fitting_scores_job(em_submit_lite):
    try:
        em_submit_lite.submit_fitting_scores_job("qtype", "queue")
    except Exception:
        pass
    else:
        raise AssertionError


def test_em_check():
    assert isinstance(em_check(), bool)
