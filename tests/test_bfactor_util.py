#!/usr/bin/env ccp4-python
import gemmi
import logging
import numpy as np
import pytest
from slicendice.util import bfactor_util

logging.basicConfig(level=logging.DEBUG)


def test_convert_plddt_to_bfactor_1():
    plddt = 100
    bfactor = bfactor_util._convert_plddt_to_bfactor(plddt)
    actual = np.round(bfactor, decimals=2)
    expected = 9.47

    assert actual == expected


def test_convert_plddt_to_bfactor_2():
    plddt = 80
    bfactor = bfactor_util._convert_plddt_to_bfactor(plddt)
    actual = np.round(bfactor, decimals=2)
    expected = 36.14

    assert actual == expected


def test_convert_plddt_to_bfactor_3():
    plddt = 51
    bfactor = bfactor_util._convert_plddt_to_bfactor(plddt)
    actual = np.round(bfactor, decimals=2)
    expected = 538.46

    assert actual == expected


def test_convert_plddt_to_bfactor_4():
    plddt = 10
    actual = bfactor_util._convert_plddt_to_bfactor(plddt)
    expected = 657.97

    assert actual == expected


def test_convert_rms_to_bfactor_1():
    rms = 0.5
    occ, bfactor = bfactor_util._convert_rms_to_bfactor(rms)
    actual = np.round(bfactor, decimals=2)
    expected = 6.58

    assert actual == expected


def test_convert_rms_to_bfactor_2():
    rms = 1
    occ, bfactor = bfactor_util._convert_rms_to_bfactor(rms)
    actual = np.round(bfactor, decimals=2)
    expected = 26.32

    assert actual == expected


def test_convert_rms_to_bfactor_3():
    rms = 4
    occ, bfactor = bfactor_util._convert_rms_to_bfactor(rms)
    actual = np.round(bfactor, decimals=2)
    expected = 421.1

    assert actual == expected


def test_convert_rms_to_bfactor_4():
    rms = 10
    occ, actual = bfactor_util._convert_rms_to_bfactor(rms)
    expected = 999.0

    assert actual == expected


def test_remove_residues_below_threshold_1(test_data):
    threshold = 70
    struct = gemmi.read_structure(test_data.test_T1049_AF_model_pdb)
    struct.setup_entities()
    struct = bfactor_util.remove_residues_below_threshold(struct, threshold)
    actual = 0
    for chain in struct[0]:
        for _ in chain:
            actual += 1
    expected = 126
    assert actual == expected


def test_remove_residues_below_threshold_2(test_data):
    threshold = 50
    struct = gemmi.read_structure(test_data.test_T1049_AF_model_pdb)
    struct.setup_entities()
    struct = bfactor_util.remove_residues_below_threshold(struct, threshold)
    actual = 0
    for chain in struct[0]:
        for _ in chain:
            actual += 1
    expected = 137
    assert actual == expected


def test_remove_residues_below_threshold_3(test_data):
    threshold = 0
    struct = gemmi.read_structure(test_data.test_T1049_AF_model_pdb)
    struct.setup_entities()
    struct = bfactor_util.remove_residues_below_threshold(struct, threshold)
    actual = 0
    for chain in struct[0]:
        for _ in chain:
            actual += 1
    expected = 141
    assert actual == expected


def test_remove_residues_below_threshold_4(test_data):
    threshold = 100
    struct = gemmi.read_structure(test_data.test_T1049_AF_model_pdb)
    struct.setup_entities()
    struct = bfactor_util.remove_residues_below_threshold(struct, threshold)
    actual = 0
    for chain in struct[0]:
        for _ in chain:
            actual += 1
    expected = 0
    assert actual == expected


if __name__ == "__main__":
    import sys

    pytest.main([__file__] + sys.argv[1:])
