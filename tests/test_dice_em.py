import logging
import tempfile
from pathlib import Path
from unittest.mock import MagicMock
import pytest
from slicendice.command_line.dice_em import DiceEm, JobSubmissionHelper
from struvolpy.Structure import Structure


@pytest.fixture
def dice_em():
    logger = logging.getLogger(__name__)
    with tempfile.TemporaryDirectory() as temp_dir:
        parent_dir = Path(temp_dir)
        output_json = parent_dir / "output.json"
        yield DiceEm(logger, parent_dir, output_json)


@pytest.fixture
def job_submission_helper():
    logger = logging.getLogger(__name__)
    yield JobSubmissionHelper(logger)


def test_generate_kwargs(dice_em):
    args = MagicMock()
    args._get_kwargs.return_value = [("no_mols", 3), ("other_arg", "value")]

    kwargs = dice_em.generate_kwargs(args)

    assert kwargs == {"no_mols": 3}


def test_pre_procesing_errors(dice_em, test_data):
    kwargs = {"powerfit": True, "molrep": True, "mapin": "mapin.mrc"}
    with pytest.raises(FileNotFoundError):
        dice_em.pre_procesing_errors(kwargs)

    kwargs = {
        "powerfit": True,
        "molrep": True,
        "mapin": test_data.test_3407_segment_centered_mrc,
    }
    with pytest.raises(ValueError):
        dice_em.pre_procesing_errors(kwargs)
    kwargs = {
        "resolution": None,
        "powerfit": False,
        "molrep": True,
        "mapin": test_data.test_3407_segment_centered_mrc,
    }
    with pytest.raises(ValueError):
        dice_em.pre_procesing_errors(kwargs)


def test_pre_processing_warnings(dice_em):
    kwargs = {"contour": None}
    # won't raise an error
    try:
        dice_em.pre_processing_warnings(kwargs)
    except Exception:
        assert False
    else:
        assert True


def test_xyz_fixed_check(dice_em):
    tmp_dir = Path(dice_em.parent_dir).resolve()
    tmp_dir.joinpath("split_1").mkdir()

    pdb = tmp_dir.joinpath("fixed_model.pdb")
    pdb.touch()
    kwargs = {"xyz_fixed": pdb}
    file_tracker = MagicMock()
    file_tracker.results_directory = tmp_dir
    run_track = {}

    run_track = dice_em.xyz_fixed_check(kwargs, file_tracker, run_track)

    assert run_track["XYZ_fixed_track"] == file_tracker.results_directory.joinpath(
        "xyz_fixed.pdb"
    )
    assert run_track["XYZ_fixed_track"].exists()


def test_run_dice_em(dice_em, test_data):
    args = MagicMock()
    args._get_kwargs.return_value = [
        ("no_mols", 3),
        ("resolution", 1),
        ("queue", "local"),
        ("qtype", None),
        ("mapin", test_data.test_3407_segment_centered_mrc),
    ]
    results = []
    dice_em.run_dice_em(args, results)

    assert "pre_procesing_errors" in dir(dice_em)
    assert "pre_processing_warnings" in dir(dice_em)
    assert "xyz_fixed_check" in dir(dice_em)
    assert "post_processing" in dir(dice_em)


def test_get_fixed_structure(job_submission_helper, dice_em, test_data):
    run_track = dice_em.initialise_run_track(
        [
            test_data.test_8GTD_1_true_pdb,
            test_data.test_8GTD_2_true_pdb,
        ]
    )
    file_tracker = MagicMock()
    file_tracker.return_all_file_pathways.return_value = [
        test_data.test_8GTD_1_true_pdb,
        test_data.test_8GTD_2_true_pdb,
    ]
    file_tracker.results_directory = dice_em.parent_dir.joinpath("results")
    file_tracker.results_directory.mkdir()
    combine_structures = MagicMock(
        return_value=file_tracker.results_directory.joinpath("xyz_fixed.pdb")
    )

    job_submission_helper.get_fixed_structure(run_track, file_tracker)

    assert run_track["XYZ_fixed_track"] == str(
        file_tracker.results_directory.joinpath("xyz_fixed.pdb")
    )
    assert Path(run_track["XYZ_fixed_track"]).exists()
    assert combine_structures.called_with(
        [test_data.test_8GTD_1_true_pdb, test_data.test_8GTD_2_true_pdb],
        file_tracker.results_directory,
    )
    assert file_tracker.copy_output_file.called_with(
        str(file_tracker.results_directory.joinpath("xyz_fixed.pdb")), key="cycle"
    )


def test_copy_files_to_results(job_submission_helper, test_data):
    file_tracker = MagicMock()
    models = [
        test_data.test_8GTD_1_true_pdb,
        test_data.test_8GTD_2_true_pdb,
    ]

    job_submission_helper.copy_files_to_results(file_tracker, models)

    assert file_tracker.copy_output_file.call_count == 2
    assert file_tracker.copy_output_file.call_args_list == [
        ((test_data.test_8GTD_1_true_pdb,), {"key": "cycle"}),
        ((test_data.test_8GTD_2_true_pdb,), {"key": "cycle"}),
    ]


def test_split_model_into_chains(job_submission_helper, dice_em, test_data):
    model = Structure.from_file(test_data.test_8GTD_1_true_pdb)
    model2 = Structure.from_file(test_data.test_8GTD_2_true_pdb)
    model3 = model.combine(model2)
    model3.to_file(str(dice_em.parent_dir.joinpath("model3.pdb")))
    job_submission_helper.split_model_into_chains(
        dice_em.parent_dir.joinpath("model3.pdb")
    )

    assert len(list(dice_em.parent_dir.glob("model3_*"))) == 2


def test_generate_scores_and_run_classifier(job_submission_helper, test_data, dice_em):
    ems = MagicMock()
    ems.scoring_models = [
        test_data.test_8GTD_1_true_pdb,
        test_data.test_8GTD_2_true_pdb,
    ]
    ems.working_dir = dice_em.parent_dir
    ems.submit_fitting_scores_job.return_value = test_data.test_test_csv
    ems.classifier_threshold = 0.5
    Classifier = MagicMock()
    classifier_instance = MagicMock()
    Classifier.return_value = classifier_instance
    classifier_instance.scores_df = MagicMock()

    successful_scores_df = job_submission_helper.generate_scores_and_run_classifier(
        ems, "prefix"
    )

    assert successful_scores_df.columns.__contains__("class")
    assert successful_scores_df.columns.__contains__("proba")


def test_no_mol_limiter(job_submission_helper):
    non_overlapping_models = [1, 2, 3, 4, 5]
    no_mols = 3

    limited_models = job_submission_helper.no_mol_limiter(
        non_overlapping_models, no_mols
    )

    assert limited_models == [1, 2, 3]


def test_powerfit_submitter():
    pass


def test_molrep_submitter():
    pass
