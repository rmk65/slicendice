#!/usr/bin/env ccp4-python
import logging
import multiprocessing
import os
import pandas as pd
import re
from pathlib import Path
import pytest
from slicendice.dice.scoresclassifier import Classifier
from slicendice.dice.submit_jobs_EM import EmSubmit
from slicendice.util import check_powerfit_installation
from slicendice.util.em_util import em_check, em_fitting_scores
import tempfile

logging.basicConfig(level=logging.DEBUG)

try:
    GPU_SKIP = check_powerfit_installation()
    GPU_SKIP = not GPU_SKIP
except Exception:
    GPU_SKIP = True

try:
    FITTING_SCORES_SKIP = em_check()
    if em_fitting_scores() is None:
        FITTING_SCORES_SKIP = False
    FITTING_SCORES_SKIP = not FITTING_SCORES_SKIP
except Exception:
    FITTING_SCORES_SKIP = True


@pytest.fixture()
def temporary_directory():
    with tempfile.TemporaryDirectory() as temp_dir:
        yield Path(temp_dir)


@pytest.fixture()
def em_submit(test_data, temporary_directory):
    cores = int(multiprocessing.cpu_count() // 1.33) if not 0 else 1  # 3/4 cores
    ems = EmSubmit(
        mapin=str(Path(test_data.test_3407_segment_centered_mrc).resolve()),
        resolution=3.0,
        parent_dir=str(temporary_directory),
        xyz_fixed=None,
        nmol=1,
        models=[str(Path(test_data.test_1ss8chaina_pdb).resolve())],
        gpu=False,
        nproc=cores,
        contour=0.0131,
    )
    ems.working_dir = str(temporary_directory)

    return ems


@pytest.fixture()
def em_submit_emp(test_data, temporary_directory):
    cores = int(multiprocessing.cpu_count() // 1.33) if not 0 else 1  # 3/4 cores
    ems = EmSubmit(
        half_map_1=str(
            Path(test_data.test_3488_run_half2_class001_unfil_mrc).resolve()
        ),
        half_map_2=str(
            Path(test_data.test_3488_run_half1_class001_unfil_mrc).resolve()
        ),
        seqin=str(Path(test_data.test_3488_5me2_fasta).resolve()),
        point_group_symmetry="C1",
        starting_model_vrms=1.0,
        resolution=3.0,
        parent_dir=str(temporary_directory),
        xyz_fixed=None,
        nmol=1,
        models=[str(Path(test_data.test_5me2_a_to_move_pdb).resolve())],
        gpu=False,
        nproc=cores,
        contour=0.0131,
    )
    ems.working_dir = str(temporary_directory)

    return ems


@pytest.fixture()
@pytest.mark.skipif(
    "THIS_IS_TRAVIS" in os.environ or GPU_SKIP, reason="Not implemented in TRAVIS CI"
)
def test_running_powerfit(em_submit):
    em_submit.submit_powerfit_job("local", None)
    assert em_submit.working_dir.joinpath("powerfit_1ss8chaina").exists()
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("em_1ss8chaina.sh")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("em_1ss8chaina.log")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .joinpath("1ss8chaina_powerfit.log")
        .exists()
    )

    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .joinpath("1ss8chaina_1.pdb")
        .exists()
    )


@pytest.mark.skipif(
    "THIS_IS_TRAVIS" in os.environ or GPU_SKIP, reason="Not implemented in TRAVIS CI"
)
def test_running_powerfit_gpu(em_submit):
    em_submit.gpu = True
    em_submit.submit_powerfit_job("local", None)

    assert em_submit.working_dir.joinpath("powerfit_1ss8chaina").exists()
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("em_1ss8chaina.sh")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("em_1ss8chaina.log")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .joinpath("1ss8chaina_powerfit.log")
        .exists()
    )

    assert (
        em_submit.working_dir.joinpath("powerfit_1ss8chaina")
        .joinpath("powerfit")
        .joinpath("1ss8chaina_1.pdb")
        .exists()
    )


@pytest.mark.skipif(
    "THIS_IS_TRAVIS" in os.environ, reason="Not implemented in TRAVIS CI"
)
def test_molrep_submit(em_submit):
    em_submit.submit_molrep_job_em("local", None)
    assert em_submit.working_dir.joinpath("molrep_1ss8chaina").exists()
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina").joinpath("molrep").exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("em_1ss8chaina.sh")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("1ss8chaina_molrep.pdb")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("1ss8chaina-molrep-origin-shift.pdb")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep_rf.tab")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep.xml")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep.crd")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep.btc")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep_fobs_dns.ps")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("1ss8chaina.crd")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep_keywords.txt")
        .exists()
    )
    assert (
        em_submit.working_dir.joinpath("molrep_1ss8chaina")
        .joinpath("molrep")
        .joinpath("molrep.doc")
        .exists()
    )


@pytest.mark.skipif(
    "THIS_IS_TRAVIS" in os.environ or FITTING_SCORES_SKIP,
    reason="Not implemented in TRAVIS CI",
)  # split into 2
def test_fitting_scores_classifier(test_data, em_submit):
    em_submit.scoring_models = [test_data.test_1ss8chaina_pdb]
    exe, site_packages = em_fitting_scores()
    em_submit.external_python = exe
    em_submit.external_python_path = site_packages
    em_submit.submit_fitting_scores_job("local", None)

    df = pd.read_csv(
        em_submit.working_dir.joinpath("scores").joinpath("fittingscores.csv"),
        index_col=0,
        header=0,
    )
    cl = Classifier(df)

    assert em_submit.working_dir.joinpath("scores").exists()
    assert (
        em_submit.working_dir.joinpath("scores").joinpath("fittingscores.csv").exists()
    )

    assert cl.scores_df is not None
    assert cl.scores_df["class"].all() == 1


pytest.mark.skipif(
    "THIS_IS_TRAVIS" in os.environ or GPU_SKIP, reason="Not implemented in TRAVIS CI"
)


def test_running_em_placement(em_submit_emp):
    em_submit_emp.submit_em_placement_job("local", None)

    print(open(em_submit_emp.working_dir.joinpath("em_5me2_a_to_move.log")).read())

    emplaced_files_dir = None
    for dir_ in em_submit_emp.working_dir.joinpath(
        "emplacement_5me2_a_to_move", "em_placement"
    ).iterdir():
        if re.match(r"emplaced_files_\d+", dir_.name):
            emplaced_files_dir = dir_
            break

    assert emplaced_files_dir is not None, "Emplaced files directory not found"

    # Assert the existence of the files
    assert em_submit_emp.working_dir.joinpath("em_5me2_a_to_move.sh").exists()
    assert em_submit_emp.working_dir.joinpath("em_5me2_a_to_move.log").exists()
    assert em_submit_emp.working_dir.joinpath(
        "emplacement_5me2_a_to_move", "5me2_a_to_move.phil"
    ).exists()
    assert emplaced_files_dir.joinpath("output_table_docking.csv").exists()
    assert emplaced_files_dir.joinpath("docked_model2.map").exists()
    assert emplaced_files_dir.joinpath("docked_model_5me2_a_to_move_2.pdb").exists()
    assert emplaced_files_dir.joinpath("docked_model1.map").exists()
    assert emplaced_files_dir.joinpath("docked_model_5me2_a_to_move_1.pdb").exists()

    df = pd.read_csv(
        emplaced_files_dir.joinpath("output_table_docking.csv"), header=0, index_col=0
    )
    assert df is not None
    assert df.index[0] == str(
        emplaced_files_dir.joinpath("docked_model_5me2_a_to_move_1.pdb")
    )
    assert df.index[1] == str(
        emplaced_files_dir.joinpath("docked_model_5me2_a_to_move_2.pdb")
    )
