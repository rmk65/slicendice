#!/usr/bin/env ccp4-python
from pathlib import Path


def test_post_processing(test_data):
    from slicendice.post_processing import PostProcessGrouping

    structures = [test_data.test_8GTD_cluster_0_pdb, test_data.test_8GTD_cluster_1_pdb]
    ppg = PostProcessGrouping(
        *structures,
        input_json=test_data.test_ppg_json,
        split_key="8GTD_chain_A_split_2",
        no_mols=1
    )

    answer = {1: [Path(x).name for x in list(ppg.groups.values())[0]]}

    assert answer == {1: ['8GTD_cluster_0.pdb', '8GTD_cluster_1.pdb']}
