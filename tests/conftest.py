from collections import namedtuple
from pathlib import Path
import pytest


@pytest.fixture(scope="session")
def test_data():
    """Return a namedtuple object with the paths to all the data files we require.
    Returns
    -------
    :obj:`namedtuple <collections.namedtuple>`
        object with paths to required data files
    """
    data_dir = Path('../test_data')
    d = {}
    for i in data_dir.glob("*"):

        stem, suffix = i.stem, i.suffix[1:]
        d[f"test_{stem}_{suffix}"] = str(i)

    nt = namedtuple('TestData', d)
    return nt(**d)
